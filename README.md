# selling_furniture_ui
Đồ án "Xây dựng ứng dụng bán hàng nội thất online tích hợp công nghệ AR" trên nền tảng Android. Nhóm đã sử dụng ngôn ngữ Dart của Flutter để xây dựng ứng dụng bán hàng nội thất và dùng Unity3D để xây dựng trải nghiệm AR cho người dùng.

## 1. Chức năng:

* Ứng dụng cung cấp cho người dùng môi trường trao đổi hàng nội thất.
* Hỗ trợ người dùng mua hàng nhanh chóng chỉ với vài thao tác đơn giản.
* Người dùng có thể yêu thích sản phẩm để lưu lại vào danh sách yêu thích.
* Hiện thị thông tin sản phẩm đầy đủ, chân thật.
* Cho phép người dùng trải nghiệm sản phẩm bằng công nghệ AR: 
* Di chuyển sản phẩm đến những nơi muốn trải nghiệm.
* Chọn sản phẩm để xoay.

## 2. Yêu cầu thiết bị:

* Hệ điều hành: Android 9 trở lên.
* API version: nhỏ nhất là 28.
* Có hỗ trợ Google Play Service For AR.

## 3. Thư viện & công nghệ:

* Ứng dụng được lập trình bằng công nghệ Flutter sử dụng ngôn ngữ Dart.
* Chức năng AR của dụng dụng được xây dựng bằng Unity và được liên kết với ứng dùng bằng Flutter-unity-widget.
* Ứng dụng ARFoundation trên Unity để xây dựng chức năng AR.

## 4. Back-end & database:

* Đồ án nhóm sử dụng Firebase như một server cũng như để lưu trữ data.
* Quản lý đồ án với gitlab.

## Hướng dẫn cài đặt:

**Lưu ý nhánh sử dụng để có thể chạy AR là nhánh new-nghia**

- Bước 1: Mở AR app -> trên thanh tool bar của Unity -> chọn Flutter -> chọn Export Android.
- Bước 2: Mở flutter app, trong folder "Android" tạo 1 folder "unity-classes".
- Bước 3: Vào "unityLibrary/libs" sao chép file unity-classes.jar và dán vào folder unity-classes đã tạo ở trên.

# DEMO
Xem demo sản phẩm của nhóm [tại đây](https://www.youtube.com/watch?v=oneZMlY0_48)
