import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/widget/ProductItem.dart';

class ManageProductList extends StatefulWidget {
  String userId;
  ManageProductList({this.userId});
  @override
  _ManageProductListState createState() => _ManageProductListState();
}

class _ManageProductListState extends State<ManageProductList> {
  List<Product> getAllProd = [];
  TextEditingController _searchName = TextEditingController();
  List<Product> searchList = [];

  callback() {
    setState(() {
      loadInitData();
    });
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    getAllProd = await ProductService().prodList();
    searchList = await ProductService().prodList();
    print(getAllProd.length);
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: Container(
              padding: EdgeInsets.only(left: 10),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.search,
                    color: Colors.black.withOpacity(0.5),
                    size: 30,
                  ),
                  Container(
                    width: 300,
                    child: TextField(
                      onChanged: (value) {
                        setState(() {
                          getAllProd =
                              ProductService().searchProd(value, searchList);
                        });
                      },
                      controller: _searchName,
                      style: TextStyle(fontSize: 18, color: Colors.black),
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Nhập tên sản phẩm",
                          hintStyle: TextStyle(
                              fontSize: 18,
                              color: Colors.black.withOpacity(0.5))),
                    ),
                  )
                ],
              ),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black.withOpacity(0.5)),
                borderRadius: BorderRadius.circular(10),
              )),
          //decoration: BoxDecoration(color: Colors.blue),
        ),
        Padding(padding: EdgeInsets.only(bottom: 16)),
        Container(
          height: 630,
          child: GridView.builder(
            //scrollDirection: Axis.vertical,
            //shrinkWrap: true,
            itemCount: getAllProd.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 0.75),
            itemBuilder: (context, index) {
              return Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: ProductItem(
                    prod: getAllProd[index],
                    userId: widget.userId,
                    callbackFunction: callback,
                  ));
              //child: Text("I"),);
            },
          ),
        ),
      ],
    );
  }
}
