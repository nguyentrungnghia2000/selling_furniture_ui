import 'package:DecoHome/Views/models/order.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/screen/OrderDetailPage.dart';
import 'package:DecoHome/Views/services/order.service.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListOrder extends StatefulWidget {
  //final int deliveryState;
  final Order order;
  ListOrder({this.order,this.callbackFunction});
  final callbackFunction;
  @override
  _ListOrderState createState() => _ListOrderState();
}

class _ListOrderState extends State<ListOrder> {
  Image noImage = Image.asset("assets/Ghe_1.png");
  List<OrderInfo> getAllOrderInfo = [];
  List<Product> listPro = [];
  int deliveryType;
  Users users;
  int total=0;
  callback(){
    widget.callbackFunction();
  }
  @override
  void initState() {
    super.initState();
    setState(() {
    });
    loadInitData();
  }

  loadInitData() async {
    getAllOrderInfo = await OrderService().listOrderInfo(widget.order.orderId);
    listPro = await ProductService().prodList();
    users= await UserService().userInfo(widget.order.userId);
    for(int i=0;i<getAllOrderInfo.length;i++){
      Product product=ProductService().getPro(listPro, getAllOrderInfo[i].proId);
      total=total+product.price*getAllOrderInfo[i].quantityPro;
    }
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return InkWell(
      onTap:(){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>OrderDetailPage(order: widget.order,list: getAllOrderInfo,user: users,total: total,listPro: listPro,callbackFunction: callback,)));
      },
      child: Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Column(
          children: <Widget>[
            Container(
              height: 120 * getAllOrderInfo.length.toDouble(),
              child: ListView.builder(
                physics: ScrollPhysics(parent: null),
                  itemCount: getAllOrderInfo.length,
                  itemBuilder: (context, index) {
                    Product product=ProductService().getPro(listPro,getAllOrderInfo[index].proId);
                    return Container(
                      child: Row(
                        children: [
                          Container(
                            height: 120,
                            width: 120,
                            child: Image.network(
                              product.pic,
                              loadingBuilder: (context, child, loadingProgress) =>
                                  (loadingProgress == null)
                                      ? child
                                      : CircularProgressIndicator(),
                              errorBuilder: (context, error, stackTrace) =>
                                  noImage,
                              //fit: BoxFit.fitWidth,
                            ),
                          ),
                          Container(
                            child: Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        product.name,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                          " X ",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        Text(
                                          getAllOrderInfo[index].quantityPro.toString(),
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600),
                                        ),
                                    ],
                                  ),
                                  //Padding(padding: EdgeInsets.only(bottom: 2)),
                                  Text(users.name),
                                  Padding(padding: EdgeInsets.only(bottom: 4)),
                                  Container(
                                    //margin: EdgeInsets.only(top: 10),
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          product.price.toString()+"đ",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        
                                      ],
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.only(bottom: 4)),
                                  Text(
                                    widget.order.timeOrder,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black.withOpacity(0.5)),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }),
            ),
            (widget.order.deliveryState==2)?Container():Container(
              padding: EdgeInsets.only(bottom:12),
              child: Row(
                children: [
                  (widget.order.deliveryState!=3)?
                  InkWell(
                    onTap: () {
                      showDialog(
                                context: context,
                                builder: (context) => Dialog(
                                      child: Container(
                                          height: 190,
                                          width: 100,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Container(
                                                  margin:
                                                      EdgeInsets.only(bottom: 16),
                                                  child: Text(
                                                    "Hủy đơn hàng!",
                                                    style:
                                                        TextStyle(fontSize: 24),
                                                  )),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Column(
                                                  children: [
                                                    Text(
                                                      "Bạn có muốn tạm hủy đơn hàng",
                                                      style: TextStyle(fontSize: 16),
                                                    ),
                                                    Text(
                                                      "này không?",
                                                      style: TextStyle(fontSize: 16),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    InkWell(
                                                      onTap: () {
                                                        //exitDialog();
                                                        Navigator.pop(context);
                                                      },
                                                      child: Container(
                                                        width: 96,
                                                        height: 40,
                                                        //padding: EdgeInsets.only(top:0),
                                                        child: Center(
                                                          child: Text("Hủy",
                                                              style: TextStyle(
                                                                  color: Colors.red,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                        ),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Colors.red,
                                                                width: 1.5),
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(5)),
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: (){
                                                        Navigator.pop(context);
                                                        OrderService().cancelOrder(widget.order.orderId);
                                                        widget.callbackFunction();
                                                        showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) =>
                                                                      Dialog(
                                                                        child: Container(
                                                                            height: 160,
                                                                            width: 100,
                                                                            child: Column(
                                                                              mainAxisAlignment:
                                                                                  MainAxisAlignment.end,
                                                                              children: [
                                                                                Container(
                                                                                    margin: EdgeInsets.only(bottom: 16),
                                                                                    child: Text(
                                                                                      "Hủy đơn hàng!",
                                                                                      style: TextStyle(fontSize: 24),
                                                                                    )),
                                                                                Container(
                                                                                  margin: EdgeInsets.only(bottom: 16),
                                                                                  child: Text(
                                                                                    "Hủy đơn hàng thành công!",
                                                                                    style: TextStyle(fontSize: 16),
                                                                                  ),
                                                                                ),
                                                                                InkWell(
                                                                                  onTap: () {
                                                                                    Navigator.pop(context);
                                                                                  },
                                                                                  child: Container(
                                                                                    margin: EdgeInsets.only(bottom: 16),
                                                                                    width: 120,
                                                                                    height: 40,
                                                                                    //padding: EdgeInsets.only(top:0),
                                                                                    child: Center(
                                                                                      child: Text("Hoàn thành", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),
                                                                                    ),
                                                                                    decoration: BoxDecoration(border: Border.all(color: Theme.of(context).accentColor, width: 1.5), color: Theme.of(context).accentColor, borderRadius: BorderRadius.circular(5)),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            )),
                                                                      ));
                                                      },
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            left: 16),
                                                        width: 96,
                                                        height: 40,
                                                        //padding: EdgeInsets.only(top:0),
                                                        child: Center(
                                                          child: Text("Xác nhận",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                        ),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Theme.of(
                                                                        context)
                                                                    .accentColor,
                                                                width: 1.5),
                                                            color:
                                                                Theme.of(context)
                                                                    .accentColor,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(5)),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          )),
                                    ));
                    },
                    child: Container(
                      width: 170,
                      height: 42,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Hủy",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red)),
                        ],
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.red, width: 2)),
                    ),
                  ):Container(
                    width: 170,
                    height: 42,
                  ),
                  Padding(padding: EdgeInsets.only(left: 16)),
                  InkWell(
                    onTap: (){
                      showDialog(
                                context: context,
                                builder: (context) => Dialog(
                                      child: Container(
                                          height: 190,
                                          width: 140,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Container(
                                                  margin:
                                                      EdgeInsets.only(bottom: 16),
                                                  child: Text(
                                                    textStatus(widget.order.deliveryState)+" đơn hàng!",
                                                    style:
                                                        TextStyle(fontSize: 24),
                                                  )),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Column(
                                                  children: [
                                                    Text(
                                                      "Bạn có muốn "+titleStatus(widget.order.deliveryState),
                                                      style: TextStyle(fontSize: 16),
                                                    ),
                                                    Text(
                                                      " đơn hàng này?",
                                                      style: TextStyle(fontSize: 16),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    InkWell(
                                                      onTap: () {
                                                        //exitDialog();
                                                        Navigator.pop(context);
                                                      },
                                                      child: Container(
                                                        width: 96,
                                                        height: 40,
                                                        //padding: EdgeInsets.only(top:0),
                                                        child: Center(
                                                          child: Text("Hủy",
                                                              style: TextStyle(
                                                                  color: Colors.red,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                        ),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Colors.red,
                                                                width: 1.5),
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(5)),
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: (){
                                                        Navigator.pop(context);
                                                        OrderService().updateStatusOrder(widget.order.orderId,widget.order.deliveryState);
                                                        widget.callbackFunction();
                                                        showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) =>
                                                                      Dialog(
                                                                        child: Container(
                                                                            height: 160,
                                                                            width: 100,
                                                                            child: Column(
                                                                              mainAxisAlignment:
                                                                                  MainAxisAlignment.end,
                                                                              children: [
                                                                                Container(
                                                                                    margin: EdgeInsets.only(bottom: 16),
                                                                                    child: Text(
                                                                                      textStatus(widget.order.deliveryState)+" đơn hàng!",
                                                                                      style: TextStyle(fontSize: 24),
                                                                                    )),
                                                                                Container(
                                                                                  margin: EdgeInsets.only(bottom: 16),
                                                                                  child: Text(
                                                                                    textStatus(widget.order.deliveryState)+" đơn hàng thành công!",
                                                                                    style: TextStyle(fontSize: 16),
                                                                                  ),
                                                                                ),
                                                                                InkWell(
                                                                                  onTap: () {
                                                                                    Navigator.pop(context);
                                                                                  },
                                                                                  child: Container(
                                                                                    margin: EdgeInsets.only(bottom: 16),
                                                                                    width: 120,
                                                                                    height: 40,
                                                                                    child: Center(
                                                                                      child: Text("Hoàn thành", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),
                                                                                    ),
                                                                                    decoration: BoxDecoration(border: Border.all(color: Theme.of(context).accentColor, width: 1.5), color: Theme.of(context).accentColor, borderRadius: BorderRadius.circular(5)),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            )),
                                                                      ));
                                                      },
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            left: 16),
                                                        width: 96,
                                                        height: 40,
                                                        child: Center(
                                                          child: Text(buttonStatus(widget.order.deliveryState),
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                        ),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Theme.of(
                                                                        context)
                                                                    .accentColor,
                                                                width: 1.5),
                                                            color:
                                                                Theme.of(context)
                                                                    .accentColor,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(5)),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          )),
                                    ));
                    },
                    child: Container(
                      width: 170,
                      height: 42,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(buttonStatus(widget.order.deliveryState),
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ],
                      ),
                      decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              color: Theme.of(context).accentColor, width: 2)),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Theme.of(context).accentColor, width: 1.5)),
      ),
    );
  }
  String buttonStatus(int a) {
    if (a == 0) {
      return "Xác nhận";
    } else if (a == 1) {
      return "Thành công";
    } else if (a == 2) {
      return "";
    } else {
      return "Vận chuyển";
    }
  }
  String textStatus(int a){
    if(a==0){
      return "Xác nhận";
    }
    else if(a==1){
      return "Vận chuyển";
    }
    else if(a==2){
      return "";
    }
    else {
      return "Vận chuyển";
    }
  }
  String titleStatus(int a){
    if(a==0){
      return "xác nhận";
    }
    else if(a==1){
      return "vận chuyển";
    }
    else if(a==2){
      return "";
    }
    else {
      return "vận chuyển";
    }
  }
}
