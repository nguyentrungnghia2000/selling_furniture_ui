import 'package:DecoHome/Views/widget/NewspaperItem.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/models/paper.model.dart';
import 'package:DecoHome/Views/services/paper.service.dart';

class NewspaperList extends StatefulWidget {
  @override
  _NewspaperListState createState() => _NewspaperListState();
}

class _NewspaperListState extends State<NewspaperList> {
  List<Paper> getAllPaper = [];
  callback() {
    setState(() {
      loadInitData(); 
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {});
    loadInitData();
  }

  loadInitData() async {
    getAllPaper = await PaperService().paperList();
    print(getAllPaper.length);
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return ListView.builder(
      physics: ScrollPhysics(parent: null),
      itemCount: getAllPaper.length,
      itemBuilder: (context, index) {
        return Container(
          padding: EdgeInsets.only(top: 6, bottom: 6),
          child: NewspaperItem(paper: getAllPaper[index],),
        );
      },
    );
  }
}
