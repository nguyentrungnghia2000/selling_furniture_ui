import 'package:DecoHome/Views/models/order.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/models/order.model.dart';
import 'package:DecoHome/Views/services/order.service.dart';
import 'package:DecoHome/Views/widget/PurchasesItem.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PurchaseList extends StatefulWidget {
  final String userId;
  final int deliveryState;
  PurchaseList({this.deliveryState, this.userId});
  @override
  _PurchaseListState createState() => _PurchaseListState();
}

class _PurchaseListState extends State<PurchaseList> {
  List<OrderInfo> getAllPurchase = [];
  List<Product> listPro = [];
  List<Order> getAllOrder = [];
  int deliveryType;
  int count = 0;
  callback() {
    setState(() {
      loadInitData();
      //print("Come here");
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      deliveryType = widget.deliveryState;
    });
    loadInitData();
  }

  loadInitData() async {
    count=0;
    //print(deliveryType);
    getAllPurchase = await OrderService().listOrderInfoUser(widget.userId);
    listPro = await ProductService().prodList();
    getAllOrder = await OrderService().listOrderUser(widget.userId);
    for (int i = 0; i < getAllOrder.length; i++) {
      if (getAllOrder[i].deliveryState == deliveryType) {
        count++;
      }
    }
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: getAllPurchase.length,
            itemBuilder: (context, index) {
              Product product =
                  ProductService().getPro(listPro, getAllPurchase[index].proId);
              Order order = OrderService()
                  .getOrder(getAllOrder, getAllPurchase[index].orderId);
              if (order.deliveryState == widget.deliveryState) {
                return Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    height: 120,
                    width: 340,
                    child: ItemPurchase(
                        orderInfo: getAllPurchase[index],
                        prod: product,
                        order: order,
                        callbackFunction: callback));
              } else {
                return Container();
              }
            },
          );
  }
}

class EmptyPurchase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 160),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            children: [
              SvgPicture.asset('assets/EmtyCard.svg'),
              Padding(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Text("Your purchase is empty!",
                    style: TextStyle(
                        fontSize: 24, color: Theme.of(context).accentColor)),
              ),
              Text(
                "Go a head, order some items",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.black.withOpacity(0.5)),
              ),
              Text(
                "from the menu.",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.black.withOpacity(0.5)),
              ),
              Padding(
                padding: EdgeInsets.only(top: 50),
                child: Container(
                  padding:
                      EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.add_outlined,
                        size: 30,
                        color: Colors.white,
                      ),
                      Text(
                        "Add item",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: Colors.white),
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Theme.of(context).accentColor),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
