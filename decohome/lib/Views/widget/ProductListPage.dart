import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:DecoHome/Views/widget/ItemCard.dart';
import 'package:flutter/material.dart';

class ProductList extends StatefulWidget {
  final String userId;
  Function callbackFunction;
  ProductList({this.userId, this.callbackFunction});
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  List<Product> getAllProd = [];
  callback() {
    setState(() {
      widget.callbackFunction();
      loadInitData();
    });
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    String element = await UserService().getElement(widget.userId);
    if (element != "") {
      //print(ProductService().getElementByColor("Xanh lá cây"));
      getAllProd = await ProductService().prodListElement(element);
      if (getAllProd.length < 2) {
        getAllProd = await ProductService().prodList();
      }
      getAllProd.shuffle();
    } else {
      getAllProd = await ProductService().prodList();
      getAllProd.shuffle();
    }
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      shrinkWrap: true,
      itemCount: getAllProd.length > 3 ? 3 : getAllProd.length,
      itemBuilder: (context, index) {
        return Container(
            margin: EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: ItemCard(
              prod: getAllProd[index],
              userId: widget.userId,
              callbackFunction: callback,
            ));
      },
    );
  }
}
