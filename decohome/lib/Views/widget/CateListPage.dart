import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/screen/AddBirthday.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:DecoHome/Views/widget/CateItemCard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CateList extends StatefulWidget {
  final String userId;
  CateList({@required this.userId});
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<CateList> {
  Users user;
  List<Product> getAllProd = [];
  List<Product> searchList = [];
  int itemType = 0;
  int currentIndex = 0;
  List categories = [
    'Tất cả',
    'Bàn Ghế',
    'Tủ Kệ',
    'Sofa',
    'Giường',
    'Trang trí',
    'Khác'
  ];
  int type = 0;
  TextEditingController _searchName = TextEditingController();
  bool isFilter = false;
  @override
  void initState() {
    super.initState();
    setState(() {});
    loadInitData(type);
  }

  loadInitData(int type) async {
          user = await UserService().userInfo(widget.userId);
    if (isFilter != true) {
      getAllProd = await ProductService().prodCateList(type);
      getAllProd.shuffle();
      searchList = await ProductService().prodList();
    } else {
      getAllProd =
          await ProductService().prodCateElementList(user.element, type);
      getAllProd.shuffle();
      searchList = await ProductService().prodListElement(user.element);
    }
    //print(searchList);
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              margin: EdgeInsets.only(top: 20),
              //padding: EdgeInsets.only(left: 10, right: 10),
              child: Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.search,
                        color: Colors.black.withOpacity(0.5),
                        size: 30,
                      ),
                      Container(
                        width: 280,
                        child: TextField(
                          onChanged: (value) {
                            setState(() {
                              getAllProd = ProductService()
                                  .searchProd(value, searchList);
                            });
                          },
                          controller: _searchName,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Nhập tên sản phẩm",
                              hintStyle: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black.withOpacity(0.5))),
                        ),
                      )
                    ],
                  ),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black.withOpacity(0.5)),
                    borderRadius: BorderRadius.circular(10),
                  )),
              //decoration: BoxDecoration(color: Colors.blue),
            ),
            Expanded(
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: InkWell(
                        onTap: () {
                          setState(() {
                            if (isFilter == true) {
                              isFilter = false;
                            } else {
                               if (user.element != "") {
                                 isFilter = true;
                               }
                              else{
                                Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddBirthday(
                                          userid: user.id,
                                        )));
                              }
                            }
                            loadInitData(type);
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(8),
                          child: Row(
                            children: <Widget>[
                              SvgPicture.asset(
                                (isFilter == true)
                                    ? 'assets/AmDuong.svg'
                                    : 'assets/AmDuong2.svg',
                                height: 30,
                                width: 30,
                              ),
                            ],
                          ),
                          decoration: BoxDecoration(
                              color: (isFilter == true)
                                  ? Theme.of(context).accentColor
                                  : Colors.grey,
                              borderRadius: BorderRadius.circular(8)),
                        )),
                  )
                ],
              ),
            ),
          ],
        ),
        Container(
            margin: EdgeInsets.only(top: 20),
            height: 30,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: categories.length,
                itemBuilder: (context, index) => GestureDetector(
                      child: Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(right: 20),
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          categories[index],
                          style: TextStyle(
                              color: index == currentIndex
                                  ? Colors.white
                                  : Theme.of(context).hintColor,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                        decoration: BoxDecoration(
                            color: index == currentIndex
                                ? Theme.of(context).accentColor
                                : Colors.transparent,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                                color: Theme.of(context).hintColor, width: 2)),
                      ),
                      onTap: () {
                        setState(() {
                          currentIndex = index;
                          type = index;
                        });
                      },
                    ))),
        Container(
          margin: EdgeInsets.only(top: 10),
          width: 450,
          height: 590,
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: getAllProd.length,
            itemBuilder: (context, index) {
              if (type == 0) {
                return Container(
                    margin: EdgeInsets.symmetric(
                      vertical: 20,
                    ),
                    child: ItemShow(
                      prod: getAllProd[index],
                      userId: widget.userId,
                    ));
              } else if (getAllProd[index].prodCategory == type.toString()) {
                return Container(
                    margin: EdgeInsets.symmetric(
                      vertical: 20,
                    ),
                    child: ItemShow(
                      prod: getAllProd[index],
                      userId: widget.userId,
                    ));
              } else {
                return Container();
              }
            },
          ),
        ),
      ],
    );
  }
}
