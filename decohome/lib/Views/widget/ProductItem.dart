import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/cart.service.dart';
import 'package:DecoHome/Views/services/favorite.service.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/screen/ManageProductInfoPage.dart';
import 'package:flutter/material.dart';

class ProductItem extends StatefulWidget {
  final Product prod;
  final String userId;
  final Function callbackFunction;
  //Function callbackFunction;
  ProductItem({this.prod, this.userId,this.callbackFunction});
  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  Image noImage = Image.asset("assets/Ghe_1.png");
  callback()  {
    setState(()  {
      widget.callbackFunction();
      loadInitData();
    });
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ManageProductInfoPage(
                  prod: widget.prod,
                  userId: widget.userId,
                  callbackFunction: callback,
                ),
              ));
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 200,
              width: 170,
              //child: Text("data"),
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: <Widget>[
                  Container(
                    width: 170,
                    height: 200,
                    child: Image.network(
                      widget.prod.pic,
                      loadingBuilder: (context, child, loadingProgress) =>
                          (loadingProgress == null)
                              ? child
                              : CircularProgressIndicator(),
                      errorBuilder: (context, error, stackTrace) => noImage,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(15)),
            ),
            Container(
              padding: EdgeInsets.only(top: 4,bottom: 2),
              child: Text(
                widget.prod.name,
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
            ),
            Text(widget.prod.price.toString()+"đ"),
          ],
        ));
  }
}
