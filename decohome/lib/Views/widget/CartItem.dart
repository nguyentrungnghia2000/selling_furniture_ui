import 'package:DecoHome/Views/models/cart.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/cart.service.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:flutter/material.dart';

class CartItem extends StatefulWidget {
  final Cart cartId;
  final String userId;
  final Product prod;
  final Function callbackFunction;
  CartItem({this.cartId, this.userId, this.prod, this.callbackFunction}) {
    //print(prod.prodid);
  }
  @override
  _CartItemState createState() => _CartItemState();
}

class _CartItemState extends State<CartItem> {
  Image noImage = Image.asset("assets/Ghe_1.png");
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Container(
        height: 120,
        child: Row(
          children: <Widget>[
            Container(
              width: 150,
              height: 150,
              child: Image.network(
                widget.prod.pic,
                loadingBuilder: (context, child, loadingProgress) =>
                    (loadingProgress == null)
                        ? child
                        : CircularProgressIndicator(),
                errorBuilder: (context, error, stackTrace) => noImage,
              ),
            ),
            Expanded(
                child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 15),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.prod.name,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                      Text(widget.prod.color),
                      Padding(padding: EdgeInsets.only(top: 10)),
                      Row(
                        children: <Widget>[
                          Text(
                            widget.prod.price.toString(),
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                          Text(
                            "đ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                      Row(children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                              right: 5, left: 3, top: 2, bottom: 2),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.star_rate_rounded,
                                size: 20,
                                color: Colors.white,
                              ),
                              Text("0.0",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white))
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Theme.of(context).accentColor),
                        ),
                        Padding(padding: EdgeInsets.only(right: 50)),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              (widget.cartId.quantityPro==1)?Container(width: 24):InkWell(
                                  onTap: () {
                                    setState(() {
                                      widget.cartId.quantityPro =
                                          widget.cartId.quantityPro - 1;
                                      CartService().subtractQuantityPro(
                                          widget.cartId.cartId,
                                          widget.cartId.quantityPro);
                                      widget.callbackFunction();
                                    });
                                  },
                                  child: Icon(Icons.horizontal_rule_rounded)),
                              Container(
                                width: 40,
                                margin: EdgeInsets.only(left: 5, right: 5),
                                //padding: EdgeInsets.only(left: 5, right: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                        widget.cartId.quantityPro.toString(),
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600)),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(
                                        color: Theme.of(context).accentColor)),
                              ),
                              (widget.cartId.quantityPro==widget.prod.amount)?Container():InkWell(
                                  onTap: () {
                                    setState(() {
                                      widget.cartId.quantityPro =
                                          widget.cartId.quantityPro + 1;
                                      CartService().addQuantityPro(
                                          widget.cartId.cartId,
                                          widget.cartId.quantityPro);
                                      widget.callbackFunction();
                                    });
                                  },
                                  child: Icon(Icons.add)),
                            ]),
                      ])
                    ],
                  ),
                ),
                Positioned(
                    right: 10,
                    top: 10,
                    child: InkWell(
                        onTap: () {
                          setState(() {
                            showDialog(
                                context: context,
                                builder: (context) => Dialog(
                                      child: Container(
                                          height: 190,
                                          width: 100,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            //crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 16),
                                                  child: Text(
                                                    "Xóa sản phẩm!",
                                                    style:
                                                        TextStyle(fontSize: 24),
                                                  )),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Column(
                                                  children: [
                                                    Text(
                                                      "Bạn có muốn xóa sản phẩm",
                                                      style:
                                                          TextStyle(fontSize: 16),
                                                    ),
                                                    Text(
                                                      "khỏi giỏ hàng cá nhân.",
                                                      style:
                                                          TextStyle(fontSize: 16),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    InkWell(
                                                      onTap: () {
                                                        //exitDialog();
                                                        Navigator.pop(context);
                                                      },
                                                      child: Container(
                                                        width: 96,
                                                        height: 40,
                                                        //padding: EdgeInsets.only(top:0),
                                                        child: Center(
                                                          child: Text("Hủy",
                                                              style: TextStyle(
                                                                  color: Colors.red,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                        ),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Colors.red,
                                                                width: 1.5),
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: () {
                                                        Navigator.pop(context);
                                                        CartService().deleteCartItem(widget.userId, widget.prod.prodid);
                                                        showDialog(
                                                            context: context,
                                                            builder:
                                                                (context) =>
                                                                    Dialog(
                                                                      child: Container(
                                                                          height: 160,
                                                                          width: 100,
                                                                          child: Column(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.end,
                                                                            children: [
                                                                              Container(
                                                                                  margin: EdgeInsets.only(bottom: 16),
                                                                                  child: Text(
                                                                                    "Xóa sản phẩm!",
                                                                                    style: TextStyle(fontSize: 24),
                                                                                  )),
                                                                              Container(
                                                                                margin: EdgeInsets.only(bottom: 16),
                                                                                child: Text(
                                                                                  "Xóa sản phẩm thành công",
                                                                                  style: TextStyle(fontSize: 16),
                                                                                ),
                                                                              ),
                                                                              InkWell(
                                                                                onTap: () {
                                                                                  Navigator.pop(context);
                                                                                  widget.callbackFunction();
                                                                                },
                                                                                child: Container(
                                                                                  margin: EdgeInsets.only(bottom: 16),
                                                                                  width: 120,
                                                                                  height: 40,
                                                                                  //padding: EdgeInsets.only(top:0),
                                                                                  child: Center(
                                                                                    child: Text("Hoàn thành", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),
                                                                                  ),
                                                                                  decoration: BoxDecoration(border: Border.all(color: Theme.of(context).accentColor, width: 1.5), color: Theme.of(context).accentColor, borderRadius: BorderRadius.circular(5)),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          )),
                                                                    ));
                                                        //widget.callbackFunction();
                                                      },
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            left: 16),
                                                        width: 96,
                                                        height: 40,
                                                        //padding: EdgeInsets.only(top:0),
                                                        child: Center(
                                                          child: Text("Xóa",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                        ),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Theme.of(
                                                                        context)
                                                                    .accentColor,
                                                                width: 1.5),
                                                            color: Theme.of(
                                                                    context)
                                                                .accentColor,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          )),
                                    ));
                          });
                        },
                        child: Icon(Icons.cancel)))
              ],
            ))
          ],
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border:
                Border.all(color: Theme.of(context).accentColor, width: 1.5)),
      ),
    );
  }
}
