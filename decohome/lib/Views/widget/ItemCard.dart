import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/cart.service.dart';
import 'package:DecoHome/Views/services/favorite.service.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/screen/ProductInfoPage.dart';
import 'package:flutter/material.dart';

class ItemCard extends StatefulWidget {
  final Product prod;
  final String userId;
  Function callbackFunction;
  ItemCard({this.prod, this.userId, this.callbackFunction});
  @override
  _ItemCardState createState() => _ItemCardState();
}

class _ItemCardState extends State<ItemCard> {
  Image noImage = Image.asset("assets/Ghe_1.png");
  bool isFavo;
  bool isInCart;
  callback() {
    setState(() {
      widget.callbackFunction();
      loadInitData();
    });
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    isFavo = await FavoriteService().isFavo(widget.userId, widget.prod.prodid);
    isInCart = await CartService().isInCart(widget.userId, widget.prod.prodid);
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProductInfoPage(
                  prod: widget.prod,
                  userId: widget.userId,
                  callbackFunction: callback,
                ),
              ));
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 200,
              width: 170,
              //child: Text("data"),
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: <Widget>[
                  Container(
                    width: 170,
                    height: 200,
                    child: Image.network(
                      widget.prod.pic,
                      loadingBuilder: (context, child, loadingProgress) =>
                          (loadingProgress == null)
                              ? child
                              : CircularProgressIndicator(),
                      errorBuilder: (context, error, stackTrace) => noImage,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  Positioned(
                      top: 7.5,
                      left: 7.5,
                      child: Container(
                          padding: EdgeInsets.all(5),
                          child: InkWell(
                            onTap: () {
                          setState(() {
                            if (isFavo == true) {
                              isFavo = false;
                              FavoriteService().deleteFavoItem(
                                  widget.userId, widget.prod.prodid);
                              showDialog(
                                barrierDismissible: false,
                                  context: context,
                                  builder: (context) => Dialog(
                                        child: Container(
                                            height: 190,
                                            width: 100,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 16),
                                                    child: Text(
                                                      "Hủy yêu thích!",
                                                      style:
                                                          TextStyle(fontSize: 24),
                                                    )),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(bottom: 16),
                                                  child: Column(
                                                    children: [
                                                      Text(
                                                        "Xóa sản phẩn khỏi danh sách",
                                                        style:
                                                            TextStyle(fontSize: 16),
                                                      ),
                                                      Text(
                                                        "yêu thích thành công!",
                                                        style:
                                                            TextStyle(fontSize: 16),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                    if (widget.callbackFunction !=
                                                        null) {
                                                      widget.callbackFunction();
                                                    }
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 16),
                                                    width: 120,
                                                    height: 40,
                                                    //padding: EdgeInsets.only(top:0),
                                                    child: Center(
                                                      child: Text("Hoàn thành",
                                                          style: TextStyle(
                                                              color: Colors.white,
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                    ),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color:
                                                                Theme.of(context)
                                                                    .accentColor,
                                                            width: 1.5),
                                                        color: Theme.of(context)
                                                            .accentColor,
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                5)),
                                                  ),
                                                ),
                                              ],
                                            )),
                                      ));
                            } else {
                              isFavo = true;
                              FavoriteService().createFavoItem(
                                  widget.userId, widget.prod.prodid);
                              if (widget.callbackFunction != null) {
                                widget.callbackFunction();
                              }
                            }
                          });
                        },
                            child: Container(
                                //padding: EdgeInsets.all(7.5),
                                child: Icon(
                                  isFavo == true
                                      ? Icons.favorite
                                      : Icons.favorite_border,
                                  size: 25,
                                  color: Theme.of(context).accentColor,
                                ),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30))),
                          ),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(30))))
                ],
              ),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(15)),
            ),
            Container(
              margin: EdgeInsets.only(top:4,bottom: 2),
              child: Text(
                widget.prod.name,
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
            ),
            Row(
              children: [
                Text(widget.prod.price.toString()),
                Text('đ')
              ],
            ),
          ],
        ));
  }
}
