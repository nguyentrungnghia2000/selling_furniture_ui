import 'package:flutter/material.dart';
import 'package:DecoHome/Views/models/paper.model.dart';
import 'package:DecoHome/Views/services/paper.service.dart';
import 'package:url_launcher/url_launcher.dart';

class NewspaperItem extends StatefulWidget {
  final Paper paper;
  NewspaperItem({this.paper});
  @override
  _NewspaperItemState createState() => _NewspaperItemState();
}

class _NewspaperItemState extends State<NewspaperItem> {
  Image noImage = Image.asset("assets/Ghe_1.png");
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _openUrl(widget.paper.Link);
      },
      child: Container(
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(8),
                height: 120,
                width: 120,
                child: Container(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                      widget.paper.Pic,
                      loadingBuilder: (context, child, loadingProgress) =>
                          (loadingProgress == null)
                              ? child
                              : CircularProgressIndicator(),
                      errorBuilder: (context, error, stackTrace) => noImage,
                      //fit: BoxFit.fitWidth,
                    ),
                  ),
                  decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border:
                  Border.all(color: Theme.of(context).accentColor, width: 1.5)),
                ),
                
              ),
              Container(
                width: 260,
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                  Text(
                      widget.paper.Header,
                      style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      ),
                  Text(widget.paper.Title,maxLines: 3,overflow: TextOverflow.ellipsis,textAlign: TextAlign.justify)
                ]
                    ),
              )
            ],
          ),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border:
                  Border.all(color: Theme.of(context).accentColor, width: 1.5)),
        ),
      ),
    );
  }
    void _openUrl(String url) async {
    final _uri = Uri.parse(url);
    if (await canLaunchUrl(_uri)) {
      await launchUrl(_uri);
    } else {
      // can't launch url, there is some error
      final scaffold = ScaffoldMessenger.of(context);
      scaffold.showSnackBar(
        SnackBar(
          content: Text('Could not open Shopee link'),
          action: SnackBarAction(
              label: 'Close', onPressed: scaffold.hideCurrentSnackBar),
        ),
      );
      throw "Could not launch $url";
    }
  }
}
