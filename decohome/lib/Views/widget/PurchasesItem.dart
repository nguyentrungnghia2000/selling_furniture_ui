import 'package:DecoHome/Views/models/order.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/order.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
class ItemPurchase extends StatefulWidget {
  final OrderInfo orderInfo;
  final Product prod;
  final Order order;
  final Function callbackFunction;
  ItemPurchase({this.orderInfo, this.prod, this.order, this.callbackFunction}) {

  }
  @override
  _ItemPurchaseState createState() => _ItemPurchaseState();
}

class _ItemPurchaseState extends State<ItemPurchase> {
  Image noImage = Image.asset("assets/Ghe_1.png");
  @override
  Widget build(BuildContext context) {
    //print(widget.orderInfo.orderId);
    return InkWell(
      onTap: (){
      },
          child: Container(
        margin: EdgeInsets.only(top: 10),
        child: Container(
          child: Row(
            children: <Widget>[
              Container(
                height: 120,
                width: 120,
                child: Image.network(
                  widget.prod.pic,
                  loadingBuilder: (context, child, loadingProgress) =>
                      (loadingProgress == null)
                          ? child
                          : CircularProgressIndicator(),
                  errorBuilder: (context, error, stackTrace) => noImage,
                  //fit: BoxFit.fitWidth,
                ),
              ),
              Expanded(
                  child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.prod.name,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                        Text(widget.prod.color),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Row(
                            children: <Widget>[
                              Text(
                                widget.prod.price.toString(),
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                              Text(
                                "đ",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                        ),
                        Text(
                          widget.order.timeOrder,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Colors.black.withOpacity(0.5)),
                        )
                      ],
                    ),
                  ),
                  Positioned(
                      top: 10,
                      right: 10,
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            //OrderService().deleteOrder(widget.order, widget.orderInfo);
                            showDialog(
                                context: context,
                                builder: (context) => Dialog(
                                      child: Container(
                                          height: 190,
                                          width: 100,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            //crossAxisAlignment: CrossAxisAlignment.end,
                                            children: [
                                              Container(
                                                  margin:
                                                      EdgeInsets.only(bottom: 16),
                                                  child: Text(
                                                    "Xóa sản phẩm!",
                                                    style:
                                                        TextStyle(fontSize: 24),
                                                  )),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Column(
                                                  children: [
                                                    Text(
                                                      "Bạn muốn xóa sản phẩm",
                                                      style: TextStyle(fontSize: 16),
                                                    ),
                                                    Text(
                                                      "khỏi đơn hàng này?",
                                                      style: TextStyle(fontSize: 16),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    InkWell(
                                                      onTap: () {
                                                        //exitDialog();
                                                        Navigator.pop(context);
                                                      },
                                                      child: Container(
                                                        width: 96,
                                                        height: 40,
                                                        //padding: EdgeInsets.only(top:0),
                                                        child: Center(
                                                          child: Text("Hủy",
                                                              style: TextStyle(
                                                                  color: Colors.red,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                        ),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Colors.red,
                                                                width: 1.5),
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(5)),
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: (){
                                                        Navigator.pop(context);
                                                        OrderService().deleteOrder(widget.order, widget.orderInfo);
                                                        //widget.callbackFunction();
                                                        showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) =>
                                                                      Dialog(
                                                                        child: Container(
                                                                            height: 190,
                                                                            width: 100,
                                                                            child: Column(
                                                                              mainAxisAlignment:
                                                                                  MainAxisAlignment.end,
                                                                              children: [
                                                                                Container(
                                                                                    margin: EdgeInsets.only(bottom: 16),
                                                                                    child: Text(
                                                                                      "Xóa sản phẩm!",
                                                                                      style: TextStyle(fontSize: 24),
                                                                                    )),
                                                                                Container(
                                                                                  margin: EdgeInsets.only(bottom: 16),
                                                                                  child: Column(
                                                                                    children: [
                                                                                      Text(
                                                                                        "Xóa sản phẩm khỏi đơn hàng",
                                                                                        style: TextStyle(fontSize: 16),
                                                                                      ),
                                                                                      Text(
                                                                                        "thành công!",
                                                                                        style: TextStyle(fontSize: 16),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                InkWell(
                                                                                  onTap: () {
                                                                                    Navigator.pop(context);
                                                                                    //OrderService().deleteOrder(widget.order, widget.orderInfo);
                                                                                    widget.callbackFunction();
                                                                                  },
                                                                                  child: Container(
                                                                                    margin: EdgeInsets.only(bottom: 16),
                                                                                    width: 120,
                                                                                    height: 40,
                                                                                    //padding: EdgeInsets.only(top:0),
                                                                                    child: Center(
                                                                                      child: Text("Hoàn thành", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),
                                                                                    ),
                                                                                    decoration: BoxDecoration(border: Border.all(color: Theme.of(context).accentColor, width: 1.5), color: Theme.of(context).accentColor, borderRadius: BorderRadius.circular(5)),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            )),
                                                                      ));
                                                          //widget.callbackFunction();
                                                      },
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            left: 16),
                                                        width: 96,
                                                        height: 40,
                                                        //padding: EdgeInsets.only(top:0),
                                                        child: Center(
                                                          child: Text("Xóa",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                        ),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Theme.of(
                                                                        context)
                                                                    .accentColor,
                                                                width: 1.5),
                                                            color:
                                                                Theme.of(context)
                                                                    .accentColor,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(5)),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          )),
                                    ));
                          });
                        },
                        child: Icon(
                            widget.order.deliveryState < 1 ? Icons.cancel : null),
                      ))
                ],
              ))
            ],
          ),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border:
                  Border.all(color: Theme.of(context).accentColor, width: 1.5)),
        ),
      ),
    );
  }
}
