import 'package:DecoHome/Views/models/order.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/order.service.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/widget/ListOrder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ManagePurchaseList extends StatefulWidget {
  final int deliveryState;
  ManagePurchaseList({this.deliveryState});
  @override
  _ManagePurchaseListState createState() => _ManagePurchaseListState();
}

class _ManagePurchaseListState extends State<ManagePurchaseList> {
  List<Product> listPro = [];
  List<Order> getAllOrder = [];
  int deliveryType;
  callback(){
    setState(() {
      loadInitData();
    });
  }
  @override
  void initState() {
    super.initState();
    setState(() {
      deliveryType = widget.deliveryState;
    });
    loadInitData();
  }

  loadInitData() async {
    listPro = await ProductService().prodList();
    getAllOrder = await OrderService().listOrder(4);
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return ListView.builder(
      physics: ScrollPhysics(parent: null),
      itemCount: getAllOrder.length,
      itemBuilder: (context, index) {
        if (getAllOrder[index].deliveryState == widget.deliveryState) {
          return Container(
            padding: EdgeInsets.only(top: 6, bottom: 6),
            child: ListOrder(
              order: getAllOrder[index],callbackFunction: callback,
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
