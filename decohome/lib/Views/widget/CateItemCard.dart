import 'package:DecoHome/Views/models/favorite.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/screen/ProductInfoPage.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:DecoHome/Views/services/favorite.service.dart';

class ItemShow extends StatefulWidget {
  final Product prod;
  final String userId;
  ItemShow({this.prod, this.userId});
  @override
  _ItemShowState createState() => _ItemShowState();
}

class _ItemShowState extends State<ItemShow> {
  Image noImage = Image.asset("assets/Ghe_1.png");
  bool isFavo = false;
  callback() {
    setState(() {
      loadInitData();
    });
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    isFavo = await FavoriteService().isFavo(widget.userId, widget.prod.prodid);
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    String elementColor=iconMenh(ProductService()
                            .getElementByColor(widget.prod.color));
    String elementMaterial=iconMenh(ProductService().getElementByMaterail(widget.prod.material));
    return InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProductInfoPage(
                  prod: widget.prod,
                  userId: widget.userId,
                  callbackFunction: callback,
                ),
              ));
        },
        child: Container(
          margin: EdgeInsets.only(bottom: 10),
          //width: 100,
          padding: EdgeInsets.only(left: 5, right: 10),
          child: Stack(
            alignment: AlignmentDirectional.center,
            children: <Widget>[
              Container(
                height: 250,
                width: 500,
                padding: EdgeInsets.only(left: 50, right: 50),
                child: Image.network(
                  widget.prod.pic,
                  loadingBuilder: (context, child, loadingProgress) =>
                      (loadingProgress == null)
                          ? child
                          : CircularProgressIndicator(),
                  errorBuilder: (context, error, stackTrace) => noImage,
                  //fit: BoxFit.fitWidth,
                ),
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(10)),
              ),
              Positioned(
                  top: 15,
                  left: 15,
                  child: Column(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          setState(() {
                            if (isFavo == true) {
                              isFavo = false;
                              FavoriteService().deleteFavoItem(
                                  widget.userId, widget.prod.prodid);
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (context) => Dialog(
                                        child: Container(
                                            height: 190,
                                            width: 100,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 16),
                                                    child: Text(
                                                      "Hủy yêu thích!",
                                                      style: TextStyle(
                                                          fontSize: 24),
                                                    )),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 16),
                                                  child: Column(
                                                    children: [
                                                      Text(
                                                        "Xóa sản phẩn khỏi danh sách",
                                                        style: TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                      Text(
                                                        "yêu thích thành công!",
                                                        style: TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 16),
                                                    width: 120,
                                                    height: 40,
                                                    //padding: EdgeInsets.only(top:0),
                                                    child: Center(
                                                      child: Text("Hoàn thành",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                    ),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .accentColor,
                                                            width: 1.5),
                                                        color: Theme.of(context)
                                                            .accentColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5)),
                                                  ),
                                                ),
                                              ],
                                            )),
                                      ));
                            } else {
                              isFavo = true;
                              FavoriteService().createFavoItem(
                                  widget.userId, widget.prod.prodid);
                            }
                          });
                        },
                        child: Container(
                            padding: EdgeInsets.all(7.5),
                            child: Icon(
                              isFavo == true
                                  ? Icons.favorite
                                  : Icons.favorite_border,
                              size: 30,
                              color: Theme.of(context).accentColor,
                            ),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30))),
                      ),
                      (elementColor=="")?Container():Padding(padding: EdgeInsets.only(top: 10),
                      child: SvgPicture.asset(
                        elementColor,
                        height: 45,
                        width: 45,
                      ),),
                      (elementMaterial==""||elementMaterial==elementColor)?Container():Padding(padding: EdgeInsets.only(top: 10),
                      child: SvgPicture.asset(
                        elementMaterial,
                        height: 45,
                        width: 45,
                      ),),
                    ],
                  )),
              Positioned(
                  bottom: -25,
                  child: Container(
                    width: 250,
                    padding:
                        EdgeInsets.only(top: 5, bottom: 5, left: 20, right: 20),
                    child: Column(
                      children: <Widget>[
                        Text(
                          widget.prod.name,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600,),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(widget.prod.price.toString()),
                            Text("đ")
                          ],
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30)),
                  ))
            ],
            overflow: Overflow.visible,
          ),
        ));
  }

  String iconMenh(String menh) {
    if (menh == "Hỏa") {
      return 'assets/Hoa.svg';
    } else if (menh == "Kim") {
      return 'assets/Kim.svg';
    } else if (menh == "Mộc") {
      return 'assets/Moc.svg';
    } else if (menh == "Thổ") {
      return 'assets/Tho.svg';
    } else if (menh == "Thủy") {
      return 'assets/Thuy.svg';
    } else {
      return '';
    }
  }
}
