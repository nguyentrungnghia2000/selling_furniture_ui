import 'package:DecoHome/Views/models/cart.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/services/cart.service.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:DecoHome/Views/widget/CartItem.dart';
import 'package:DecoHome/Views/screen/DeliveryStatusPage.dart';
import 'package:flutter/material.dart';

class CartList extends StatefulWidget {
  String userId;
  Function callbackFunction;
  CartList({this.userId,this.callbackFunction});
  @override
  _CartListState createState() => _CartListState();
}

class _CartListState extends State<CartList> {
  List<Cart> getAllCart = [];
  List<Product> listPro;
  int totalPrice = 0;
  Users users;
  //String noname = "Noname";
  callback() {
    setState(() {
      //print("Come here");
      widget.callbackFunction();
      loadInitData();
    });
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    getAllCart = await CartService().cartList(widget.userId);
    //print(getAllCart);
    listPro = await ProductService().prodList();
    totalPrice = await CartService().totalPrice(widget.userId);
    users = await UserService().userInfo(widget.userId);
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 700,
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: getAllCart.length,
            itemBuilder: (context, index) {
              Product product =
                  ProductService().getPro(listPro, getAllCart[index].proId);
              return Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 10,
                  ),
                  child: CartItem(
                      cartId: getAllCart[index],
                      userId: widget.userId,
                      prod: product,
                      callbackFunction: callback));
            },
          ),
        ),
        Column(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
          Container(
            height: 2,
            width: 400,
            child: Text(""),
            decoration: BoxDecoration(color: Theme.of(context).accentColor),
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Row(children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Tổng",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            totalPrice.toString(),
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600),
                          ),
                          Text(
                            "đ",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DeliveryStatusPage(
                                  users: users,
                                  listCart: getAllCart,
                                )));
                  },
                  child: Container(
                    padding: EdgeInsets.only(
                        top: 10, bottom: 10, left: 30, right: 30),
                    child: Text("Đặt hàng",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600)),
                    decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.circular(10)),
                  ))
            ]),
          )
        ]),
      ],
    );
  }
}
