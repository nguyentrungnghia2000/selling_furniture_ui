import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/widget/ItemCard.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FavotListPro extends StatefulWidget {
  String userId;
  FavotListPro({this.userId});
  @override
  _FavoListProState createState() => _FavoListProState();
}

class _FavoListProState extends State<FavotListPro> {
  List<Product> getAllProd = [];
  callback(){
    setState(() {
      //print("Come here");
      loadInitData();
    });
  }
  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    getAllProd = await ProductService().favoListProd(widget.userId);
    if (this.mounted) {
      setState(() {});
    }
  }
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: GridView.builder(
            itemCount: getAllProd.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 0.75
            ),
            itemBuilder: (context, index) {
              return Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: ItemCard(prod: getAllProd[index],userId: widget.userId,callbackFunction: callback,));
            },
          ),
        ),
      ],
    );
  }
}

