//import 'dart:html';
import 'dart:io';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/services/sign.service.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:firebase_storage/firebase_storage.dart';

class EditProductInfoPage extends StatefulWidget {
  final Product product;
  final Function callbackFunction;
  EditProductInfoPage({this.product, this.callbackFunction}) {}
  @override
  _EditProductInfoState createState() =>
      _EditProductInfoState(product: this.product);
}

class _EditProductInfoState extends State<EditProductInfoPage> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  TextEditingController _amountController = TextEditingController();

  _EditProductInfoState({this.product});
  String _typeChoose = "Bàn Ghế";
  String _colorChoose = "Đỏ";
  String _materialChoose = "Gỗ";
  List _listType = ["Bàn Ghế", "Tủ Kệ", "Sofa", "Giường", "Trang trí", "Khác"];
  List _listColor = [
    "Đỏ",
    "Cam",
    "Vàng",
    "Xanh lá cây",
    "Xanh nước biển",
    "Tím",
    "Nâu",
    "Trắng",
    "Đen",
    "Hồng",
    "Xám",
    "Khác"
  ];
  List _listMaterial = [
    "Gỗ",
    "Nhựa",
    "Tự nhiên",
    "Kim loại",
    "Thủy tinh",
    "Gốm sứ",
    "Da",
    "Khác"
  ];

  final SignService _auth = new SignService();
  Image noImage = Image.asset("assets/Ghe_1.png");
  String tempLink = "";
  Product product;
  File _image;

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    this.product = await ProductService().getProbyID(product.prodid);
    if (this.mounted) {
      setState(() {
        tempLink = widget.product.pic;
        _typeChoose = categoryName(widget.product.prodCategory);
        _colorChoose = widget.product.color;
        _materialChoose = widget.product.material;
      });
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 10, top: 10, right: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios_rounded,
                      size: 30,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 5)),
                  Text(
                    "Chỉnh sửa thông tin sản phẩm",
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ]),
                Padding(padding: EdgeInsets.only(top: 16)),
                Container(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            _pickImage();
                            //ProductService().updatePic(widget.product.prodid, tempLink);
                            //getImage();
                          },
                          child: Container(
                            padding: EdgeInsets.all(7.5),
                            child: Icon(
                              Icons.image,
                              size: 30,
                              color: Theme.of(context).accentColor,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: Theme.of(context).hintColor),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(left: 5)),
                        Container(
                          width: 150,
                          height: 150,
                          child: CircleAvatar(
                            radius: 150,
                            child: tempLink == ""
                                ? Icon(
                                    Icons.add_a_photo,
                                    size: 30,
                                    color: Colors.white,
                                  )
                                : null,
                            backgroundImage:
                                tempLink != "" ? NetworkImage(tempLink) : null,
                            backgroundColor: Theme.of(context).accentColor,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Theme.of(context).accentColor,
                              width: 5,
                            ),
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(75),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(left: 5)),
                      ]),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Tên sản phẩm",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 300,
                            //padding: EdgeInsets.only(left:16,right:16),
                            child: TextField(
                              controller: _nameController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: product.name,
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Loại sản phẩm",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                              width: 300,
                              //padding: EdgeInsets.only(left:16,right:16),
                              child: DropdownButton(
                                isExpanded: true,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                                value: _typeChoose,
                                onChanged: (value) {
                                  setState(() {
                                    //print(value);
                                    _typeChoose = value;
                                  });
                                },
                                items: _listType.map((value) {
                                  return DropdownMenuItem(
                                      value: value, child: Text(value));
                                }).toList(),
                              )),
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Giá thành",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 300,
                            //padding: EdgeInsets.only(left:16,right:16),
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(
                                decimal: true,
                                signed: true,
                              ),
                              controller: _priceController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: product.price.toString(),
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Mô tả",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 300,
                            //padding: EdgeInsets.only(left:16,right:16),
                            child: TextField(
                              controller: _descriptionController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: product.prodInfo,
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Số lượng",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 300,
                            //padding: EdgeInsets.only(left:16,right:16),
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(
                                decimal: true,
                                signed: true,
                              ),
                              controller: _amountController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: product.amount.toString(),
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Màu sắc chủ đạo",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                              width: 300,
                              //padding: EdgeInsets.only(left:16,right:16),
                              child: DropdownButton(
                                isExpanded: true,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                                value: _colorChoose,
                                onChanged: (value) {
                                  setState(() {
                                    //print(value);
                                    _colorChoose = value;
                                  });
                                },
                                items: _listColor.map((value) {
                                  return DropdownMenuItem(
                                      value: value, child: Text(value));
                                }).toList(),
                              )),
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Nguyên liệu chính",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                              width: 300,
                              //padding: EdgeInsets.only(left:16,right:16),
                              child: DropdownButton(
                                isExpanded: true,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                                value: _materialChoose,
                                onChanged: (value) {
                                  setState(() {
                                    //print(value);
                                    _materialChoose = value;
                                  });
                                },
                                items: _listMaterial.map((value) {
                                  return DropdownMenuItem(
                                      value: value, child: Text(value));
                                }).toList(),
                              )),
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(left: 10, right: 10, bottom: 16),
        child: InkWell(
          onTap: () {
            ProductService().updateProductInfo(
                product.prodid,
                _nameController.text,
                categoryID(_typeChoose),
                _priceController.text,
                _descriptionController.text,
                _amountController.text,
                tempLink,
                _colorChoose,
                _materialChoose);
            showDialog(
                context: context,
                builder: (context) => Dialog(
                      child: Container(
                          height: 190,
                          width: 100,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                  margin: EdgeInsets.only(bottom: 16),
                                  child: Text(
                                    "Cập nhật thông tin!",
                                    style: TextStyle(fontSize: 24),
                                  )),
                              Container(
                                margin: EdgeInsets.only(bottom: 16),
                                child: Column(
                                  children: [
                                    Text(
                                      "Cập nhật thông tin sản phẩm",
                                      style: TextStyle(fontSize: 16),
                                    ),
                                    Text(
                                      "thành công!",
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                  if (widget.callbackFunction != null) {
                                    widget.callbackFunction();
                                  }
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 16),
                                  width: 120,
                                  height: 40,
                                  //padding: EdgeInsets.only(top:0),
                                  child: Center(
                                    child: Text("Hoàn thành",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Theme.of(context).accentColor,
                                          width: 1.5),
                                      color: Theme.of(context).accentColor,
                                      borderRadius: BorderRadius.circular(5)),
                                ),
                              ),
                            ],
                          )),
                    ));
          },
          child: Container(
            padding: EdgeInsets.only(top: 16, bottom: 16),
            margin: EdgeInsets.only(top: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Cập nhật thông tin",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
              ],
            ),
            decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.circular(10)),
          ),
        ),
      ),
    );
  }

  String categoryName(String type) {
    if (type == '1')
      return "Bàn Ghế";
    else if (type == '2')
      return "Tủ Kệ";
    else if (type == '3')
      return "Sofa";
    else if (type == '4')
      return "Giường";
    else if (type == '5')
      return "Trang trí";
    else if (type == '6') {
      return "Khác";
    }
    return '';
  }

  String categoryID(String type) {
    if (type == 'Bàn Ghế')
      return "1";
    else if (type == 'Tủ Kệ')
      return "2";
    else if (type == 'Sofa')
      return "3";
    else if (type == 'Giường')
      return "4";
    else if (type == 'Trang trí')
      return "5";
    else if (type == 'Khác') {
      return "6";
    }
    return '';
  }

  Future _pickImage() async {
    final _storage = FirebaseStorage.instance;
    final _picker = ImagePicker();
    PickedFile image;
    image = await _picker.getImage(source: ImageSource.gallery);
    if (image != null) {
      var file = File(image.path);
      String url = widget.product.name;
      if (image != null) {
        var snapshot =
            await _storage.ref().child('ProductPicture/$url').putFile(file);
        var downloadUrl = await snapshot.ref.getDownloadURL();
        setState(() {
          tempLink = downloadUrl;
        });
      } else {
        print('No Path Received');
      }
    }
  }
}
