import 'package:DecoHome/Views/models/cart.model.dart';
import 'package:DecoHome/Views/services/order.service.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/screen/CompleteStatusPage.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PaymentStatusPage extends StatefulWidget {
  final String userId;
  final String userName;
  final String userPhone;
  final String userAddress;
  final List<Cart> listCart;
  PaymentStatusPage({this.userId, this.userName,this.userPhone,this.userAddress,this.listCart});
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<PaymentStatusPage> {
  @override
  Widget build(BuildContext context) {
    //print(listCart);
    return Scaffold(
      body: SafeArea(
          child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 10, right: 10),
              child: Row(children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios_rounded,
                    size: 30,
                  ),
                ),
                Padding(padding: EdgeInsets.only(right: 5)),
                Text(
                  "Thông tin thanh toán",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                )
              ]),
            ),
            Padding(padding: EdgeInsets.only(top: 15)),
            Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Column(children: <Widget>[
                  Container(
                    width: 500,
                    height: 1.5,
                    child: Text(""),
                    decoration:
                        BoxDecoration(color: Theme.of(context).accentColor),
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Đặt hàng",
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      Padding(padding: EdgeInsets.only(right: 15)),
                      Text(
                        "Vận chuyển",
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      Padding(padding: EdgeInsets.only(right: 20)),
                      Text("Thanh toán",
                          style:
                              TextStyle(color: Theme.of(context).accentColor)),
                      Padding(padding: EdgeInsets.only(right: 20)),
                      Text("Hoàn thành",
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.5))),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 10, top: 5),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: 15,
                          width: 15,
                          child: Text(""),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Theme.of(context).accentColor),
                        ),
                        Container(
                          height: 3,
                          width: 75,
                          child: Text(""),
                          decoration: BoxDecoration(
                              color: Theme.of(context).accentColor),
                        ),
                        Container(
                          height: 15,
                          width: 15,
                          child: Text(""),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Theme.of(context).accentColor),
                        ),
                        Container(
                          height: 3,
                          width: 90,
                          child: Text(""),
                          decoration: BoxDecoration(
                              color: Theme.of(context).accentColor),
                        ),
                        Container(
                          height: 15,
                          width: 15,
                          child: Text(""),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Theme.of(context).accentColor),
                        ),
                        Container(
                          height: 3,
                          width: 90,
                          child: Text(""),
                          decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.5)),
                        ),
                        Container(
                          height: 15,
                          width: 15,
                          child: Text(""),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.black.withOpacity(0.5)),
                        ),
                      ],
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  Container(
                    width: 500,
                    height: 1.5,
                    child: Text(""),
                    decoration:
                        BoxDecoration(color: Theme.of(context).accentColor),
                  ),
                ])),
            OldCard(),
          ],
        ),
      )),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(left: 16,right: 16,bottom: 16),
        child: InkWell(
                    onTap: () {
                      OrderService().createOrder(widget.userId,widget.userName,widget.userPhone,widget.userAddress,1,widget.listCart);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CompleteStatusPage(userId: widget.userId,),
                        ));
                    },
                    child: Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Đặt hàng",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20)),
                        ],
                      ),
                      decoration: BoxDecoration(
                          //border: Border.all(color:Theme.of(context).accentColor),
                          borderRadius: BorderRadius.circular(30),
                          color: Theme.of(context).accentColor),
                    ),
                  ),
      ),
    );
  }
}

class OldCard extends StatefulWidget {
  OldCard({Key key}) : super(key: key);
  @override
  _OldCard createState() => _OldCard();
}

class _OldCard extends State<OldCard> {
  String deliveryType = '';
  Color theme = Color.fromRGBO(6, 130, 130, 50);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: Text(
              "Phương thức thanh toán",
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
            child: Row(
              children: <Widget>[
                Icon(Icons.money,size: 30,color: Theme.of(context)
                                                      .accentColor,),
                Padding(padding: EdgeInsets.only(left: 10)),
                Text("Tiền mặt",
                    style:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                Padding(padding: EdgeInsets.only(right: 165)),
                //SvgPicture.asset('assets/Doc.svg'),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Radio(
                          value: 'Google pay',
                          groupValue: deliveryType,
                          onChanged: (val) {
                            deliveryType = val;
                            //theme = val;
                            setState(() {});
                          }),
                    ],
                  ),
                )
                //Text("data"),
              ],
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                    color: Theme.of(context).accentColor, width: 1.5)),
          ),
        ],
      ),
    );
  }
}
