import 'package:DecoHome/Views/models/order.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/services/order.service.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:flutter/material.dart';

class OrderDetailPage extends StatefulWidget {
  final Order order;
  final Users user;
  final List<OrderInfo> list;
  final int total;
  final List<Product> listPro;
  final Function callbackFunction;
  OrderDetailPage(
      {this.order,
      this.list,
      this.user,
      this.total,
      this.listPro,
      this.callbackFunction});
  @override
  _OrderDetailState createState() => _OrderDetailState(
      order: order, list: list, user: user, total: total, listPro: listPro);
}

class _OrderDetailState extends State<OrderDetailPage> {
  Image noImage = Image.asset("assets/Ghe_1.png");
  Order order;
  final Users user;
  final List<OrderInfo> list;
  final int total;
  final List<Product> listPro;
  List<Order> listOrder;
  _OrderDetailState(
      {this.order, this.list, this.user, this.total, this.listPro});
  @override
  void initState() {
    super.initState();
    setState(() {});
    loadInitData();
  }

  loadInitData() async {
    this.listOrder=await OrderService().listOrder(4);
      this.order= await OrderService().getOrder(listOrder, this.order.orderId);
        if (this.mounted) {
      setState(() {
      });
    }
  }

  Widget build(BuildContext context) {
    //print(order.orderId);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.only(left: 10, top: 10, right: 10),
              child: Column(
                children: [
                  Row(children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        size: 30,
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(right: 5)),
                    Expanded(
                        child: Text(
                      "Thông tin đơn hàng",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    )),
                  ]),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.only(top: 10, left: 12, bottom: 10),
                    child: Column(
                      children: [
                        Row(
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Thông tin đơn hàng",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Theme.of(context).accentColor),
                                ),
                                Padding(padding: EdgeInsets.only(top: 8)),
                                Row(
                                  children: [
                                    Text(
                                      "Mã đơn hàng: ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                    Text(
                                      order.orderId,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8)),
                                Row(
                                  children: [
                                    Text(
                                      "Trạng thái đơn hàng: ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                    Text(
                                      status(order.deliveryState),
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                  ],
                                )
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                            color: Theme.of(context).accentColor, width: 1.5)),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.only(top: 10, left: 12, bottom: 10),
                    child: Column(
                      children: [
                        Row(
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Thông tin vận chuyển",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Theme.of(context).accentColor),
                                ),
                                Padding(padding: EdgeInsets.only(top: 8)),
                                Row(
                                  children: [
                                    Text(
                                      "Tên khách hàng: ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                    Text(
                                      order.userName,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8)),
                                Row(
                                  children: [
                                    Text(
                                      "Số điện thoại: ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                    Text(
                                      order.userPhone,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8)),
                                Row(
                                  children: [
                                    Text(
                                      "Địa chỉ vận chuyển: ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                    Text(
                                      order.userAddress,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                            color: Theme.of(context).accentColor, width: 1.5)),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.only(top: 10, left: 12, bottom: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Thông tin sản phẩm",
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: Theme.of(context).accentColor),
                        ),
                        Container(
                          height: 120 * list.length.toDouble(),
                          child: ListView.builder(
                            physics: ScrollPhysics(parent: null),
                            itemCount: list.length,
                            itemBuilder: (context, index) {
                              Product product = ProductService()
                                  .getPro(listPro, list[index].proId);
                              return Container(
                                child: Row(children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            height: 120,
                                            width: 120,
                                            child: Image.network(
                                              product.pic,
                                              loadingBuilder: (context, child,
                                                      loadingProgress) =>
                                                  (loadingProgress == null)
                                                      ? child
                                                      : CircularProgressIndicator(),
                                              errorBuilder: (context, error,
                                                      stackTrace) =>
                                                  noImage,
                                              //fit: BoxFit.fitWidth,
                                            ),
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    product.name,
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 16),
                                                  ),
                                                  Text(
                                                    " X ",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 16),
                                                  ),
                                                  Text(
                                                    list[index]
                                                        .quantityPro
                                                        .toString(),
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 16),
                                                  ),
                                                ],
                                              ),
                                              Padding(
                                                  padding:
                                                      EdgeInsets.only(top: 10)),
                                              Row(
                                                children: [
                                                  Text(
                                                    product.price.toString(),
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 16),
                                                  ),
                                                  Text(
                                                    "đ",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      //Text(product.price.toString(),)
                                    ],
                                  )
                                ]),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                            color: Theme.of(context).accentColor, width: 1.5)),
                  ),
                  Padding(padding: EdgeInsets.only(top: 8)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "Tổng tiền: ",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Theme.of(context).accentColor),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            total.toString(),
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600),
                          ),
                          Text(
                            "đ",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600),
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              )),
        ),
      ),
      bottomNavigationBar: (order.deliveryState == 2)
          ? SizedBox()
          : Container(
              padding: EdgeInsets.only(top: 10, bottom: 13),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  (order.deliveryState==3)?
                  SizedBox(
                    //height: 160,
                    width: 165,
                  ):InkWell(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (context) => Dialog(
                                child: Container(
                                    height: 160,
                                    width: 100,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(bottom: 16),
                                            child: Text(
                                              "Hủy đơn hàng!",
                                              style: TextStyle(fontSize: 24),
                                            )),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 16),
                                          child: Text(
                                            "Bạn có muốn hủy đơn hàng này?",
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 16),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              InkWell(
                                                onTap: () {
                                                  //exitDialog();
                                                  Navigator.pop(context);
                                                },
                                                child: Container(
                                                  width: 96,
                                                  height: 40,
                                                  //padding: EdgeInsets.only(top:0),
                                                  child: Center(
                                                    child: Text("Hủy",
                                                        style: TextStyle(
                                                            color: Colors.red,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                  ),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color:
                                                              Colors.red,
                                                          width: 1.5),
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                  OrderService().cancelOrder(
                                                      widget.order.orderId);

                                                  widget.callbackFunction();
                                                  showDialog(
                                                      context: context,
                                                      builder:
                                                          (context) => Dialog(
                                                                child:
                                                                    Container(
                                                                        height:
                                                                            160,
                                                                        width:
                                                                            100,
                                                                        child:
                                                                            Column(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.end,
                                                                          children: [
                                                                            Container(
                                                                                margin: EdgeInsets.only(bottom: 16),
                                                                                child: Text(
                                                                                  "Hủy đơn hàng!",
                                                                                  style: TextStyle(fontSize: 24),
                                                                                )),
                                                                            Container(
                                                                              margin: EdgeInsets.only(bottom: 16),
                                                                              child: Text(
                                                                                "Hủy đơn hàng thành công!",
                                                                                style: TextStyle(fontSize: 16),
                                                                              ),
                                                                            ),
                                                                            InkWell(
                                                                              onTap: () {
                                                                                loadInitData();
                                                                                Navigator.pop(context);
                                                                              },
                                                                              child: Container(
                                                                                margin: EdgeInsets.only(bottom: 16),
                                                                                width: 120,
                                                                                height: 40,
                                                                                //padding: EdgeInsets.only(top:0),
                                                                                child: Center(
                                                                                  child: Text("Hoàn thành", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),
                                                                                ),
                                                                                decoration: BoxDecoration(border: Border.all(color: Theme.of(context).accentColor, width: 1.5), color: Theme.of(context).accentColor, borderRadius: BorderRadius.circular(5)),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        )),
                                                              ));
                                                  //widget.callbackFunction();
                                                },
                                                child: Container(
                                                  margin:
                                                      EdgeInsets.only(left: 16),
                                                  width: 96,
                                                  height: 40,
                                                  //padding: EdgeInsets.only(top:0),
                                                  child: Center(
                                                    child: Text("Xác nhận",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                  ),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color:
                                                              Theme.of(context)
                                                                  .accentColor,
                                                          width: 1.5),
                                                      color: Theme.of(context)
                                                          .accentColor,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    )),
                              ));
                    },
                    child: Container(
                      width: 170,
                      height: 42,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Hủy",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red)),
                        ],
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.red, width: 2)),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(left: 16)),
                  InkWell(
                    onTap: (() {
                      showDialog(
                                context: context,
                                builder: (context) => Dialog(
                                      child: Container(
                                          height: 190,
                                          width: 100,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Container(
                                                  margin:
                                                      EdgeInsets.only(bottom: 16),
                                                  child: Text(
                                                    textStatus(widget.order.deliveryState)+" đơn hàng!",
                                                    style:
                                                        TextStyle(fontSize: 24),
                                                  )),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Column(
                                                  children: [
                                                    Text(
                                                      "Bạn có muốn "+textStatus(widget.order.deliveryState),
                                                      style: TextStyle(fontSize: 16),
                                                    ),
                                                    Text(
                                                      "đơn hàng này?",
                                                      style: TextStyle(fontSize: 16),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    InkWell(
                                                      onTap: () {
                                                        Navigator.pop(context);
                                                      },
                                                      child: Container(
                                                        width: 96,
                                                        height: 40,
                                                        child: Center(
                                                          child: Text("Hủy",
                                                              style: TextStyle(
                                                                  color: Colors.red,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                        ),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Colors.red,
                                                                width: 1.5),
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(5)),
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: (){
                                                        Navigator.pop(context);
                                                        OrderService().updateStatusOrder(widget.order.orderId,widget.order.deliveryState);
                                                        widget.callbackFunction();
                                                        showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) =>
                                                                      Dialog(
                                                                        child: Container(
                                                                            height: 160,
                                                                            width: 100,
                                                                            child: Column(
                                                                              mainAxisAlignment:
                                                                                  MainAxisAlignment.end,
                                                                              children: [
                                                                                Container(
                                                                                    margin: EdgeInsets.only(bottom: 16),
                                                                                    child: Text(
                                                                                      textStatus(widget.order.deliveryState)+" đơn hàng!",
                                                                                      style: TextStyle(fontSize: 24),
                                                                                    )),
                                                                                Container(
                                                                                  margin: EdgeInsets.only(bottom: 16),
                                                                                  child: Text(
                                                                                    textStatus(widget.order.deliveryState)+" thành công!",
                                                                                    style: TextStyle(fontSize: 16),
                                                                                  ),
                                                                                ),
                                                                                InkWell(
                                                                                  onTap: () {
                                                                                    Navigator.pop(context);
                                                                                    loadInitData();
                                                                                  },
                                                                                  child: Container(
                                                                                    margin: EdgeInsets.only(bottom: 16),
                                                                                    width: 120,
                                                                                    height: 40,
                                                                                    child: Center(
                                                                                      child: Text("Hoàn thành", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),
                                                                                    ),
                                                                                    decoration: BoxDecoration(border: Border.all(color: Theme.of(context).accentColor, width: 1.5), color: Theme.of(context).accentColor, borderRadius: BorderRadius.circular(5)),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            )),
                                                                      ));
                                                      },
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            left: 16),
                                                        width: 96,
                                                        height: 40,
                                                        //padding: EdgeInsets.only(top:0),
                                                        child: Center(
                                                          child: Text(buttonStatus(widget.order.deliveryState),
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500)),
                                                        ),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Theme.of(
                                                                        context)
                                                                    .accentColor,
                                                                width: 1.5),
                                                            color:
                                                                Theme.of(context)
                                                                    .accentColor,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(5)),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          )),
                                    ));
                    }),
                    child: Container(
                      width: 170,
                      height: 42,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(buttonStatus(order.deliveryState),
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ],
                      ),
                      decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              color: Theme.of(context).accentColor, width: 2)),
                    ),
                  ),
                ],
              )),
    );
  }

  String status(int a) {
    if (a == 0) {
      return "Chờ xác nhận";
    } else if (a == 1) {
      return "Đang vận chuyển";
    } else if (a == 2) {
      return "Giao hàng thành công";
    } else {
      return "Tạm hủy";
    }
  }
  String buttonStatus(int a) {
    if (a == 0) {
      return "Xác nhận";
    } else if (a == 1) {
      return "Thành công";
    } else if (a == 2) {
      return "";
    } else {
      return "Vận chuyển";
    }
  }
  String textStatus(int a){
    if(a==0){
      return "Xác nhận";
    }
    else if(a==1){
      return "Vận chuyển";
    }
    else if(a==2){
      return "";
    }
    else {
      return "Vận chuyển";
    }
  }
  String titleStatus(int a){
    if(a==0){
      return "xác nhận";
    }
    else if(a==1){
      return "vận chuyển";
    }
    else if(a==2){
      return "";
    }
    else {
      return "vận chuyển";
    }
  }
}
