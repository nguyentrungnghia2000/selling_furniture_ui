import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/widget/CateListPage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:DecoHome/Views/screen/ProductInfoPage.dart';
import 'package:DecoHome/Views/screen/CartPage.dart';
import 'package:DecoHome/Views/widget/CateItemCard.dart';

class CategoryPage extends StatefulWidget {
  final String userId;
  CategoryPage({this.userId});
  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 30),
        child: Column(
          children: <Widget>[
            Row(children: <Widget>[
              Expanded(
                  child: Text(
                "Danh mục",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              )),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CartPage(
                          userId: widget.userId,
                        ),
                      ));
                },
                child: Icon(
                  Icons.shopping_cart_rounded,
                  color: Colors.black,
                  size: 28,
                ),
              )
            ]),
            Container(
                child: CateList(
                  userId: widget.userId,
                ))
          ],
        ),
      ),
    )));
  }
}
