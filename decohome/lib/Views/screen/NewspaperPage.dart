import 'package:DecoHome/Views/models/favorite.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/widget/NewspaperList.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:DecoHome/Views/widget/ManagePurchaseList.dart';

class NewspaperPage extends StatefulWidget {
  //String userId;
  //ManagePurchasesPage({this.userId});
  @override
  _NewspaperState createState() => _NewspaperState();
}

class _NewspaperState extends State<NewspaperPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: Container(
      padding: EdgeInsets.only(left: 10, top: 10, right: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(children: <Widget>[
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back_ios_rounded,
                size: 30,
              ),
            ),
            Padding(padding: EdgeInsets.only(right: 5)),
            Text(
              "Kiến thức phong thủy",
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            )
          ]),
          SingleChildScrollView(
            child: Container(
              height: 775,
              child: NewspaperList(),
            ),
          ),
        ],
      ),
    )));
  }
}
