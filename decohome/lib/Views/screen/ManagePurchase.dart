import 'package:DecoHome/Views/models/favorite.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:DecoHome/Views/widget/ManagePurchaseList.dart';

class ManagePurchasesPage extends StatefulWidget {
  //String userId;
  //ManagePurchasesPage({this.userId});
  @override
  _ManagePurchasesState createState() => _ManagePurchasesState();
}

class _ManagePurchasesState extends State<ManagePurchasesPage> {
  List<Favorite> favoList = [];
  int currentIndex = 0;
  List<String> purchaseStateList = [
    'Chờ xác nhận',
    'Vận chuyển',
    'Thành công',
    'Đang hủy'
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      padding: EdgeInsets.only(left: 10, top: 10, right: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(children: <Widget>[
            Expanded(
                child: Text(
              "Quản lý đơn hàng",
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            )),
          ]),
          //Padding(padding: EdgeInsets.only(top: 5)),
          Container(
              padding: EdgeInsets.only(left: 5),
              child: Stack(children: <Widget>[
                Positioned(
                    top: 31,
                    //left: 20,
                    child: Container(
                      height: 3,
                      width: 380,
                      decoration:
                          BoxDecoration(color: Colors.black.withOpacity(0.3)),
                    )),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 30,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: purchaseStateList.length,
                      itemBuilder: (context, index) => GestureDetector(
                            child: Container(
                              alignment: Alignment.center,
                              //margin: EdgeInsets.only(right: 5),
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Column(
                                children: [
                                  Text(
                                    purchaseStateList[index],
                                    style: TextStyle(
                                        color: index == currentIndex
                                            ? Theme.of(context).accentColor
                                            : Colors.black.withOpacity(0.3),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Container(
                                    height: 3,
                                    width: 80,
                                    decoration: BoxDecoration(
                                        color: index == currentIndex
                                            ? Theme.of(context).accentColor
                                            : Colors.transparent),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              setState(() {
                                currentIndex = index;
                              });
                            },
                          )),
                ),
              ])),
          SingleChildScrollView(
            child: Container(
              height: 685,
              child: ManagePurchaseList(deliveryState: currentIndex),
            ),
          ),
        ],
      ),
    ));
  }
}
