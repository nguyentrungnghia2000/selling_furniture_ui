import 'package:DecoHome/Views/models/order.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/services/order.service.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:provider/provider.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:radar_chart/radar_chart.dart';

//import 'package:google_nav_bar/google_nav_bar.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<DashboardPage> {
  List<Order> listOrder = [];
  List<Order> listOrderSuccess = [];
  List<OrderInfo> listOrderInfoSuccess = [];
  List<Users> listUser = [];
  int countQuantity = 0;
  int countRevenue = 0;
  List<Product> listProduct = [];

  double pre_order = 5.0;
  double delevery = 1.0;
  double successs = 6.0;
  double cancel = 1.0;
  double choiceIndex = 1.0;

  double type1=0;
  double type2=0;
  double type3=0;
  double type4=0;
  double type5=0;
  double type6=0;

  int check = 0;
  //List<PieData>
  Map<String, double> dataMap = {
    "Chờ xác nhận": 0.0,
    "Vận chuyển": 0.0,
    "Thành công": 0.0,
    "Đang hủy": 0.0,
  };
  dataChart() {
    dataMap = <String, double>{
      "Chờ xác nhận": double.parse(pre_order.toStringAsFixed(0)),
      "Vận chuyển": double.parse(delevery.toStringAsFixed(0)),
      "Thành công": double.parse(successs.toStringAsFixed(0)),
      "Đang hủy": double.parse(cancel.toStringAsFixed(0)),
    };
  }
  List<double> values1 = [0.4, 0.4, 0.4, 0.4, 0.4, 0.4];
  loadDataType(){
    type1=OrderService().countTypeProduct(listOrderInfoSuccess, listProduct, "1")/10;
    type2=OrderService().countTypeProduct(listOrderInfoSuccess, listProduct, "2")/10;
    type3=OrderService().countTypeProduct(listOrderInfoSuccess, listProduct, "3")/10;
    type4=OrderService().countTypeProduct(listOrderInfoSuccess, listProduct, "4")/10;
    type5=OrderService().countTypeProduct(listOrderInfoSuccess, listProduct, "5")/10;
    type6=OrderService().countTypeProduct(listOrderInfoSuccess, listProduct, "6")/10;
    
    values1 = [
      type3,
      type4,
      type5,
      type6,
      type1,
      type2
    ];
  }
  //Sofa, Bed, Clock, Desk, Chair, Table
  callback() {
    loadInitData();
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    listOrder = await OrderService().listOrder(4);
    listOrderSuccess = await OrderService().listOrder(2);
    listOrderInfoSuccess =
        await OrderService().listOrderSuccess(listOrderSuccess);
    listUser = await UserService().countUser();
    countQuantity = OrderService().countQuantity(listOrderInfoSuccess);
    listProduct = await ProductService().prodList();
    countRevenue =
        OrderService().countRevenue(listOrderInfoSuccess, listProduct);

    pre_order = await OrderService().getQuantityOrder(0);
    delevery = await OrderService().getQuantityOrder(1);
    successs = await OrderService().getQuantityOrder(2);
    cancel = await OrderService().getQuantityOrder(3);

    check =
        OrderService().countTypeProduct(listOrderInfoSuccess, listProduct, "1");
    dataChart();
    loadDataType();
      if (this.mounted) { // check whether the state object is in tree
    setState(() {
      // make changes here
    });
  }
  }

  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<Product>>.value(
        value: ProductService().getAllProd,
        child: SafeArea(
            child: SingleChildScrollView(
          child: Container(
              height: 745,
              padding: EdgeInsets.only(left: 10, top: 10, right: 10),
              child: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(children: <Widget>[
                        Expanded(
                            child: Text(
                          "Bảng điều khiển",
                          style: TextStyle(
                              fontSize: 22, fontWeight: FontWeight.bold),
                        )),

                        //Text("Đây là trang quản lý sản phẩm"),
                      ]),
                      Container(
                        padding: EdgeInsets.only(top: 12),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(right: 6),
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        top: 12,
                                        bottom: 12,
                                      ),
                                      child: Column(
                                        children: [
                                          Text(
                                            "Tổng doanh thu",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Padding(
                                              padding: EdgeInsets.only(top: 4)),
                                          Text(
                                            countRevenue.toString()+"đ",
                                            style: TextStyle(
                                                fontSize: 24,
                                                fontWeight: FontWeight.bold,
                                                color: Theme.of(context)
                                                    .accentColor),
                                          ),
                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color:
                                                  Theme.of(context).accentColor,
                                              width: 2)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 6),
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        top: 12,
                                        bottom: 12,
                                      ),
                                      child: Column(
                                        children: [
                                          Text(
                                            "Số lượng đơn hàng",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Padding(
                                              padding: EdgeInsets.only(top: 4)),
                                          Text(
                                            listOrder.length.toString(),
                                            style: TextStyle(
                                                fontSize: 24,
                                                fontWeight: FontWeight.bold,
                                                color: Theme.of(context)
                                                    .accentColor),
                                          ),
                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color:
                                                  Theme.of(context).accentColor,
                                              width: 2)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                                padding: EdgeInsets.only(top: 6, bottom: 6)),
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(right: 6),
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        top: 12,
                                        bottom: 12,
                                      ),
                                      child: Column(
                                        children: [
                                          Text(
                                            "Số lượng sản phẩm",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Padding(
                                              padding: EdgeInsets.only(top: 4)),
                                          Text(
                                            countQuantity.toString(),
                                            style: TextStyle(
                                                fontSize: 24,
                                                fontWeight: FontWeight.bold,
                                                color: Theme.of(context)
                                                    .accentColor),
                                          ),
                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color:
                                                  Theme.of(context).accentColor,
                                              width: 2)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 6),
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        top: 12,
                                        bottom: 12,
                                      ),
                                      child: Column(
                                        children: [
                                          Text(
                                            "Số lượng người dùng",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Padding(
                                              padding: EdgeInsets.only(top: 4)),
                                          Text(
                                            listUser.length.toString(),
                                            style: TextStyle(
                                                fontSize: 24,
                                                fontWeight: FontWeight.bold,
                                                color: Theme.of(context)
                                                    .accentColor),
                                          ),
                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color:
                                                  Theme.of(context).accentColor,
                                              width: 2)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(padding: EdgeInsets.only(top: 12)),
                            Container(
                              //padding: EdgeInsets.only(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                      padding:
                                          EdgeInsets.only(left: 12, top: 8),
                                      child: Text(
                                        "Trang thái đơn hàng",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      )),
                                  PieChart(
                                    dataMap: dataMap,
                                    chartRadius:
                                        MediaQuery.of(context).size.width / 2,
                                    centerText: null,
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color: Theme.of(context).accentColor,
                                      width: 2)),
                            ),
                            Padding(padding: EdgeInsets.only(top: 12)),
                            Container(
                              padding: EdgeInsets.only(bottom: 12),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                      padding: EdgeInsets.only(
                                          left: 12, top: 8, bottom: 8),
                                      child: Text(
                                        "Loại sản phẩm",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      )),
                                  Stack(
                                    children: [
                                      Positioned(
                                          left: 105,
                                          child: Text(
                                            "Bàn ghế(" +
                                                OrderService()
                                                    .countTypeProduct(
                                                        listOrderInfoSuccess,
                                                        listProduct,
                                                        "1")
                                                    .toString() +
                                                ")",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Theme.of(context)
                                                    .accentColor),
                                          )),
                                      Positioned(
                                          left: 220,
                                          child: Text(
                                              "Tủ kệ(" +
                                                  OrderService()
                                                      .countTypeProduct(
                                                          listOrderInfoSuccess,
                                                          listProduct,
                                                          "2")
                                                      .toString() +
                                                  ")",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Theme.of(context)
                                                      .accentColor))),
                                      Positioned(
                                          left: 30,
                                          top: 105,
                                          child: Text(
                                              "Sofa(" +
                                                  OrderService()
                                                      .countTypeProduct(
                                                          listOrderInfoSuccess,
                                                          listProduct,
                                                          "3")
                                                      .toString() +
                                                  ")",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Theme.of(context)
                                                      .accentColor))),
                                      Positioned(
                                          left: 305,
                                          top: 105,
                                          child: Text(
                                              "Giường(" +
                                                  OrderService()
                                                      .countTypeProduct(
                                                          listOrderInfoSuccess,
                                                          listProduct,
                                                          "4")
                                                      .toString() +
                                                  ")",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Theme.of(context)
                                                      .accentColor))),
                                      Positioned(
                                          left: 100,
                                          top: 205,
                                          child: Text(
                                              "Trang trí(" +
                                                  OrderService()
                                                      .countTypeProduct(
                                                          listOrderInfoSuccess,
                                                          listProduct,
                                                          "5")
                                                      .toString() +
                                                  ")",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Theme.of(context)
                                                      .accentColor))),
                                      Positioned(
                                          left: 220,
                                          top: 205,
                                          child: Text(
                                              "Khác(" +
                                                  OrderService()
                                                      .countTypeProduct(
                                                          listOrderInfoSuccess,
                                                          listProduct,
                                                          "6")
                                                      .toString() +
                                                  ")",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Theme.of(context)
                                                      .accentColor))),
                                      Container(
                                        padding: EdgeInsets.all(12),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            RadarChart(
                                              radius: 100,
                                              length: 6,
                                              initialAngle: 0,
                                              backgroundColor: Colors.white,
                                              borderStroke: 2,
                                              borderColor: Color.fromRGBO(
                                                  255, 118, 116, 1),
                                              radars: [
                                                RadarTile(
                                                    values: values1,
                                                    borderStroke: 2,
                                                    //borderColor: Colors.blue,
                                                    backgroundColor:
                                                        Color.fromRGBO(
                                                            117, 184, 253, 1))
                                              ],
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color: Theme.of(context).accentColor,
                                      width: 2)),
                            )
                          ],
                        ),
                      )
                    ]),
              )),
        )));
  }
}

class chartData {
  String type;
  int quantity;
  chartData({this.type, this.quantity});
}
