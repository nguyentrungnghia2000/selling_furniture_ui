import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/screen/EditProductInfoPage.dart';
import 'package:DecoHome/Views/services/cart.service.dart';
import 'package:DecoHome/Views/services/order.service.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:DecoHome/camera/unity_camera.dart';

import '../../camera/unity_camera.dart';

class ManageProductInfoPage extends StatefulWidget {
  final Product prod;
  final String userId;
  final Function callbackFunction;
  ManageProductInfoPage({this.prod, this.userId, this.callbackFunction}) {
    //print(prod.name);
  }
  @override
  _ManageProductInfoState createState() =>
      _ManageProductInfoState(prod: this.prod);
}

class _ManageProductInfoState extends State<ManageProductInfoPage> {
  Product prod;
  String image;
  _ManageProductInfoState({this.prod});
  Image noImage = Image.asset("assets/Ghe_1.png");
  callback() {
    setState(() {
      widget.callbackFunction();
      loadInitData();
    });
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    this.prod = await ProductService().getProbyID(prod.prodid);
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    //print(prod.price);
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
          child: Container(
        decoration: BoxDecoration(color: Colors.black.withOpacity(0.1)),
        child: ListView(children: [
          Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Stack(children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10, right: 10),
                    child: Row(children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Icon(
                            Icons.arrow_back_ios_rounded,
                            size: 30,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              InkWell(
                                onTap: () async {
                                  bool checkCart = await CartService()
                                      .isExist(this.prod.prodid);
                                  //print(check);
                                  //print(widget.prod.prodid);
                                  showDialog(
                                      context: context,
                                      builder: (context) => Dialog(
                                            child: Container(
                                                height: 190,
                                                width: 160,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                            bottom: 16),
                                                        child: Text(
                                                          "Xóa sản phẩm!",
                                                          style: TextStyle(
                                                              fontSize: 24),
                                                        )),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          bottom: 16),
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            "Xóa sản phẩm sẽ ảnh hưởng đến",
                                                            style: TextStyle(
                                                                fontSize: 14),
                                                          ),
                                                          Text(
                                                            "khách hàng của bạn?",
                                                            style: TextStyle(
                                                                fontSize: 14),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        InkWell(
                                                          onTap: () {
                                                            Navigator.pop(
                                                                context);
                                                            // if (widget
                                                            //         .callbackFunction !=
                                                            //     null) {
                                                            //   widget
                                                            //       .callbackFunction();
                                                            // }
                                                            // Navigator.pop(
                                                            //     context);
                                                          },
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    bottom: 16),
                                                            width: 120,
                                                            height: 40,
                                                            //padding: EdgeInsets.only(top:0),
                                                            child: Center(
                                                              child: Text("Hủy",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .red,
                                                                      fontSize:
                                                                          16,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500)),
                                                            ),
                                                            decoration: BoxDecoration(
                                                                border: Border.all(
                                                                    color: Colors
                                                                        .red,
                                                                    width: 1.5),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5)),
                                                          ),
                                                        ),
                                                        Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 16)),
                                                        InkWell(
                                                          onTap: () async {
                                                            Navigator.pop(
                                                                context);
                                                             if (widget
                                                                     .callbackFunction !=
                                                                 null) {
                                                               widget
                                                                   .callbackFunction();
                                                             }
                                                            CartService()
                                                                .deletrProd(this
                                                                    .prod
                                                                    .prodid);
                                                            OrderService()
                                                                .deleteProd(this
                                                                    .prod
                                                                    .prodid);
                                                             ProductService()
                                                                 .deleteProdct(
                                                                     this
                                                                         .prod
                                                                         .prodid);
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    bottom: 16),
                                                            width: 120,
                                                            height: 40,
                                                            //padding: EdgeInsets.only(top:0),
                                                            child: Center(
                                                              child: Text(
                                                                  "Xác nhận",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .white,
                                                                      fontSize:
                                                                          16,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500)),
                                                            ),
                                                            decoration: BoxDecoration(
                                                                border: Border.all(
                                                                    color: Theme.of(
                                                                            context)
                                                                        .accentColor,
                                                                    width: 1.5),
                                                                color: Theme.of(
                                                                        context)
                                                                    .accentColor,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5)),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                )),
                                          ));
                                  //ProductService()
                                  //.deleteProdct(this.prod.prodid);
                                  //Xóa sản phẩm check lại
                                },
                                child: Container(
                                  margin: EdgeInsets.only(right: 16),
                                  child: Icon(
                                    Icons.delete,
                                    color: Colors.black,
                                    size: 28,
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            EditProductInfoPage(
                                              product: this.prod,
                                              callbackFunction: callback,
                                            )),
                                  );
                                },
                                child: Container(
                                  margin: EdgeInsets.only(right: 10),
                                  child: Icon(
                                    Icons.edit,
                                    color: Colors.black,
                                    size: 28,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ]),
                  ),
                  Container(
                    height: 440,
                    padding: EdgeInsets.only(top: 30),
                    child: Image.network(
                      prod.pic,
                      loadingBuilder: (context, child, loadingProgress) =>
                          (loadingProgress == null)
                              ? child
                              : CircularProgressIndicator(),
                      errorBuilder: (context, error, stackTrace) => noImage,
                      fit: BoxFit.fitWidth,
                    ),
                  )
                ]),
                Container(
                  child: Stack(overflow: Overflow.visible, children: <Widget>[
                    Container(
                      child: CustomPaint(
                        size: Size(screenWidth, 100),
                        painter: RPSCustomPainter(),
                      ),
                    ),
                    Positioned(
                        child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 5, left: 55),
                          child: Text(
                            "Chi tiết",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(top: 5, left: 155),
                            child: FlatButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => UnityCamera(),
                                      ));
                                },
                                child: Text("Xem AR",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500))))
                      ],
                    )),
                    Positioned(
                        top: 39,
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 15, left: 20, right: screenWidth - 50),
                          decoration: BoxDecoration(color: Colors.white),
                          child: Text(
                            prod.name,
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        )),
                    Positioned(
                        top: 76,
                        child: Container(
                            padding: EdgeInsets.only(
                                left: 20, right: screenWidth - 50, bottom: 1),
                            decoration: BoxDecoration(color: Colors.white),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.star_rate_rounded,
                                  color: Colors.yellow,
                                ),
                                Icon(
                                  Icons.star_rate_rounded,
                                  color: Colors.yellow,
                                ),
                                Icon(
                                  Icons.star_rate_rounded,
                                  color: Colors.yellow,
                                ),
                                Icon(
                                  Icons.star_rate_rounded,
                                  color: Colors.yellow,
                                ),
                                Icon(
                                  Icons.star_rate_rounded,
                                  color: Colors.yellow,
                                ),
                              ],
                            )))
                  ]),
                  //decoration: BoxDecoration(color: Colors.white),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10, left: 20),
                  child: Row(children: <Widget>[
                    Text(
                      "Mô tả sản phẩm",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                  ]),
                  decoration: BoxDecoration(color: Colors.white),
                ),
                Container(
                  height: 220,
                  width: 420,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          prod.prodInfo,
                          softWrap: true,
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(color: Colors.white),
                ),
              ]),
        ]),
      )),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(left: 20, top: 10, bottom: 13),
        child: Row(
          children: <Widget>[
            Text(
              prod.price.toString() + "đ",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
            ),
            Padding(padding: EdgeInsets.only(left: 70)),
          ],
        ),
        decoration: BoxDecoration(color: Colors.white),
      ),
    );
  }
}

class RPSCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint_0 = new Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;

    Path path_0 = Path();
    path_0.moveTo(0, size.height * 0.3967000);
    path_0.quadraticBezierTo(size.width * 0.0001000, size.height * 0.0035000,
        size.width * 0.1002750, 0);
    path_0.lineTo(size.width * 0.4000750, 0);
    path_0.quadraticBezierTo(size.width * 0.4160750, size.height * 0.3987000,
        size.width * 0.5006000, size.height * 0.3996000);
    path_0.quadraticBezierTo(size.width * 0.5861750, size.height * 0.3988000,
        size.width * 0.5999500, 0);
    path_0.quadraticBezierTo(
        size.width * 0.8318000, 0, size.width * 0.9007500, 0);
    path_0.quadraticBezierTo(size.width * 0.9998250, size.height * -0.0018000,
        size.width, size.height * 0.4019000);
    path_0.quadraticBezierTo(size.width * 0.7500000, size.height * 0.4006000, 0,
        size.height * 0.3967000);
    path_0.close();

    canvas.drawPath(path_0, paint_0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
