import 'package:DecoHome/Views/models/cart.model.dart';
import 'package:DecoHome/Views/services/cart.service.dart';
import 'package:DecoHome/Views/widget/CartItem.dart';
import 'package:DecoHome/Views/widget/CartListItem.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/screen/DeliveryStatusPage.dart';

class CartPage extends StatefulWidget {
  final String userId;
  Function callbackFunction;
  CartPage({this.userId,this.callbackFunction});
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  callback(){
    setState((){
      //print("Come here");
      // if(widget.callbackFunction()!=null){
      //         widget.callbackFunction();
      // }
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
        child: Column(children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 10, right: 10),
            child: Row(children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back_ios_rounded,
                  size: 30,
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 5)),
              Text(
                "Giỏ hàng",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              )
            ]),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          CartList(userId: widget.userId,callbackFunction: callback,),
        ]),
      )),
    );
  }
}
