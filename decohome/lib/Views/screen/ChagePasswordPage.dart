import 'package:DecoHome/Views/models/account.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChangePasswordPage extends StatefulWidget {
  String id;
  final Users users;
  final Function callbackFunction;
  ChangePasswordPage({this.users, this.callbackFunction});
  @override
  State<ChangePasswordPage> createState() {
    return _ChangePasswordSate();
  }
}

class _ChangePasswordSate extends State<ChangePasswordPage> {
  TextEditingController _oldController = TextEditingController();
  TextEditingController _newController = TextEditingController();
  TextEditingController _renewController = TextEditingController();
  bool _oldpassword = true;
  bool _newpassword = true;
  bool _renewpassword = true;
  @override
  Widget build(BuildContext context) {
    //print(users.id);
    return Scaffold(
        body: SafeArea(
            child: ListView(children: <Widget>[
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, right: 10),
            child: Row(children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back_ios_rounded,
                  size: 30,
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 5)),
              Text(
                "Thay đổi mật khẩu",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              )
            ]),
          ),
          Container(
            margin: EdgeInsets.only(top: 30),
            padding: EdgeInsets.only(left: 23, right: 23),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Mật khẩu cũ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 330,
                            child: TextField(
                              controller: _oldController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    icon: Icon(Icons.remove_red_eye),
                                    color: Theme.of(context).accentColor,
                                    onPressed: () {
                                      setState(() {
                                        if (_oldpassword == true) {
                                          _oldpassword = false;
                                        } else {
                                          _oldpassword = true;
                                        }
                                      });
                                    },
                                  ),
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                              obscureText: _oldpassword,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Mật khẩu mới",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 330,
                            //padding: EdgeInsets.only(left:16,right:16),
                            child: TextField(
                              controller: _newController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    icon: Icon(Icons.remove_red_eye),
                                    color: Theme.of(context).accentColor,
                                    onPressed: () {
                                      setState(() {
                                        if (_newpassword == true) {
                                          _newpassword = false;
                                        } else {
                                          _newpassword = true;
                                        }
                                      });
                                    },
                                  ),
                                  border: InputBorder.none,
                                  //hintText: widget.users.address,
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                              obscureText: _newpassword,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Nhập lại mật khẩu mới",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 330,
                            //padding: EdgeInsets.only(left:16,right:16),
                            child: TextField(
                              keyboardType: TextInputType.visiblePassword,
                              controller: _renewController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    icon: Icon(Icons.remove_red_eye),
                                    color: Theme.of(context).accentColor,
                                    onPressed: () {
                                      setState(() {
                                        if (_renewpassword == true) {
                                          _renewpassword = false;
                                        } else {
                                          _renewpassword = true;
                                        }
                                      });
                                    },
                                  ),
                                  border: InputBorder.none,
                                  //hintText: widget.users.email,
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                              obscureText: _renewpassword,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                InkWell(
                  onTap: () {
                    if (widget.users.password != _oldController.text) {
                      showDialog(
                          context: context,
                          builder: (context) => Dialog(
                                child: Container(
                                    height: 190,
                                    width: 100,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(bottom: 16),
                                            child: Text(
                                              "Sai mật khẩu!",
                                              style: TextStyle(fontSize: 24),
                                            )),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 16),
                                          child: Column(
                                            children: [
                                              Text(
                                                "Mật khẩu cũ không đúng",
                                                style: TextStyle(fontSize: 16),
                                              ),
                                              Text(
                                                "vui lòng nhập lại!",
                                                style: TextStyle(fontSize: 16),
                                              ),
                                            ],
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            Navigator.pop(context);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 16),
                                            width: 120,
                                            height: 40,
                                            padding: EdgeInsets.only(top: 0),
                                            child: Center(
                                              child: Text("Nhập lại",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w500)),
                                            ),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                    width: 1.5),
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                          ),
                                        ),
                                      ],
                                    )),
                              ));
                    }
                    else{
                      if(_newController.text!=_renewController.text){
                        showDialog(
                          context: context,
                          builder: (context) => Dialog(
                                child: Container(
                                    height: 190,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(bottom: 16),
                                            child: Text(
                                              "Sai mật khẩu!",
                                              style: TextStyle(fontSize: 24),
                                            )),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 16,left: 16,right: 16),
                                          child: Column(
                                            children: [
                                              Text(
                                                "Mật khẩu mới không trùng với nhập",
                                                style: TextStyle(fontSize: 16),
                                              ),
                                              Text(
                                                "lại mật khẩu mới vui lòng nhập lại!",
                                                style: TextStyle(fontSize: 16),
                                              ),
                                            ],
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            Navigator.pop(context);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 16),
                                            width: 120,
                                            height: 40,
                                            padding: EdgeInsets.only(top: 0),
                                            child: Center(
                                              child: Text("Nhập lại",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w500)),
                                            ),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                    width: 1.5),
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                          ),
                                        ),
                                      ],
                                    )),
                              ));
                      }
                      else{
                        //FirebaseAuth.instance.currentUser.updatePassword(_newController.text);
                        UserService().changePasswork(widget.users.id, _newController.text);
                        showDialog(
                          context: context,
                          builder: (context) => Dialog(
                                child: Container(
                                    height: 150,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(bottom: 16),
                                            child: Text(
                                              "Đổi mật khẩu!",
                                              style: TextStyle(fontSize: 24),
                                            )),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 16,left: 16,right: 16),
                                          child: Column(
                                            children: [
                                              Text(
                                                "Thay đổi mật khẩu thành công!",
                                                style: TextStyle(fontSize: 16),
                                              ),
                                            ],
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            Navigator.pop(context);
                                            if (widget.callbackFunction != null) {
                                            widget.callbackFunction();
                                          }
                                            Navigator.pop(context);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 16),
                                            width: 120,
                                            height: 40,
                                            padding: EdgeInsets.only(top: 0),
                                            child: Center(
                                              child: Text("Hoàn thành",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w500)),
                                            ),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                    width: 1.5),
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                          ),
                                        ),
                                      ],
                                    )),
                              ));
                      }
                    }
                    //print(_nationalityController.text);
                  },
                  child: Container(
                    padding: EdgeInsets.only(top: 16, bottom: 16),
                    margin: EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Đổi mật khẩu",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    ])));
  }
}
