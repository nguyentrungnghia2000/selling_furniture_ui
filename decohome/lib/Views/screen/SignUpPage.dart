import 'package:DecoHome/Views/services/sign.service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/screen/SignInPage.dart';
//import 'package:transparent_image/transparent_image.dart';
//import 'dart:async';
//import 'package:flutter/services.dart';

class SignUp extends StatefulWidget {
  @override
  State<SignUp> createState() {
    return _SignUp();
  }
}

class _SignUp extends State<SignUp> {
  String name = '';
  String email = '';
  String password = '';
  String error = '';
  final SignService _auth = SignService();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        body: Center(
          child: ListView(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    child: FlatButton(
                      padding: EdgeInsets.only(),
                      child: Icon(Icons.arrow_back_ios_rounded),
                      color: Colors.transparent,
                      minWidth: 50,
                      height: 50,
                      onPressed: () {
                        //print('back to login');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  SigninPage(), // back to login scene
                            ));
                      },
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 40),
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 20, left: 30),
                        child: FlatButton(
                          minWidth: 140,
                          height: 5,
                          color: Color.fromRGBO(6, 130, 130, 50),
                          child: Text(
                            '',
                            style: TextStyle(color: Colors.red),
                          ),
                          onPressed: () {},
                        ),
                      ),
                      Container(
                        decoration:
                            BoxDecoration(color: Color.fromARGB(0, 0, 0, 0)),
                        padding: EdgeInsets.only(),
                        child: Text(
                          'Đăng ký',
                          style: TextStyle(
                              fontSize: 42, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(bottom: 100, left: 40),
                child: Text(
                  'Tạo tài khoản mới',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
              ),
              Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 10, left: 40, right: 40),
                        child: TextFormField(
                          validator: (val) =>
                              val.isEmpty ? 'Hãy nhập tên của bạn' : null,
                          onChanged: (val) {
                            setState(() {
                              this.name = val;
                            });
                          },
                          decoration: InputDecoration(
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green)),
                              labelText: 'Tên',
                              hintText: 'Nhập tên của bạn',
                              hintStyle: TextStyle(fontSize: 13)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10, left: 40, right: 40),
                        child: TextFormField(
                          validator: (val) =>
                              val.isEmpty ? 'Hãy nhập email của bạn' : null,
                          onChanged: (val) {
                            setState(() {
                              this.email = val;
                            });
                          },
                          decoration: InputDecoration(
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green)),
                              labelText: 'Email',
                              hintText: 'Nhập email của bạn',
                              hintStyle: TextStyle(fontSize: 13)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: 10, bottom: 80, left: 40, right: 40),
                        child: TextFormField(
                          validator: (val) => val.length < 6
                              ? 'Mật khẩu cần ít nhất 6 ký tự'
                              : null,
                          obscureText: true,
                          onChanged: (val) {
                            setState(() {
                              this.password = val;
                            });
                          },
                          decoration: InputDecoration(
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green)),
                              labelText: 'Mật khẩu',
                              hintText: 'Nhập mật khẩu của bạn',
                              hintStyle: TextStyle(fontSize: 13)),
                        ),
                      ),
                    ],
                  )),
              Container(
                  child: Align(
                child: SizedBox(
                  width: 240,
                  child: FlatButton(
                    height: 60,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    child: Text(
                      'Đăng ký',
                      style: TextStyle(fontSize: 16),
                    ),
                    color: Color.fromRGBO(6, 130, 130, 1),
                    textColor: Colors.white,
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        //print(this.email);
                        //print(this.password);
                        dynamic result =
                            await _auth.SignUp(name, this.email, this.password);
                        if (result == null) {
                          setState(() {
                            showDialog(
                                context: context,
                                builder: (context) => Dialog(
                                      child: Container(
                                        height: 170,
                                        width: 150,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Text(
                                                  "Đăng ký!",
                                                  style:
                                                      TextStyle(fontSize: 24),
                                                )),
                                            Container(
                                              margin:
                                                  EdgeInsets.only(bottom: 16),
                                              child: Column(
                                                children: [
                                                  Text(
                                                    "Tài khoản đã tồn tại!",
                                                    style:
                                                        TextStyle(fontSize: 16),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin:
                                                  EdgeInsets.only(bottom: 16),
                                              child: InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                  // Navigator.push(
                                                  //       context,
                                                  //       MaterialPageRoute(
                                                  //           builder: (context) =>
                                                  //               SigninPage()));
                                                },
                                                child: Container(
                                                  //margin: EdgeInsets.only(left: 16),
                                                  width: 120,
                                                  height: 40,
                                                  //padding: EdgeInsets.only(top:0),
                                                  child: Center(
                                                    child: Text("Đăng ký lại",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                  ),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color:
                                                              Theme.of(context)
                                                                  .accentColor,
                                                          width: 1.5),
                                                      color: Theme.of(context)
                                                          .accentColor,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ));
                          });
                        } else {
                          name = '';
                          email = '';
                          password = '';
                          setState(() {
                            showDialog(
                                context: context,
                                builder: (context) => Dialog(
                                      child: Container(
                                        height: 170,
                                        width: 150,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Text(
                                                  "Đăng ký!",
                                                  style:
                                                      TextStyle(fontSize: 24),
                                                )),
                                            Container(
                                              margin:
                                                  EdgeInsets.only(bottom: 16),
                                              child: Column(
                                                children: [
                                                  Text(
                                                    "Đăng ký tài khoản thành công!",
                                                    style:
                                                        TextStyle(fontSize: 16),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin:
                                                  EdgeInsets.only(bottom: 16),
                                              child: InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                  Navigator.push(
                                                         context,
                                                         MaterialPageRoute(
                                                             builder: (context) =>
                                                                 SigninPage()));
                                                },
                                                child: Container(
                                                  width: 96,
                                                  height: 40,
                                                  child: Center(
                                                    child: Text("Đăng nhập",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                  ),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color:
                                                              Theme.of(context)
                                                                  .accentColor,
                                                          width: 1.5),
                                                      color: Theme.of(context)
                                                          .accentColor,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ));
                          });
                        }
                      }
                    },
                  ),
                ),
              )),
              Container(
                padding: EdgeInsets.only(left: 65, top: 10),
                child: Text(
                  'Bằng cách tạo một tài khoản, bạn đồng ý với',
                  style: TextStyle(fontSize: 13),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 10),
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 90),
                      child: Text(
                        'Điều khoảng',
                        style: TextStyle(
                            fontSize: 13,
                            decoration: TextDecoration.underline,
                            color: Color.fromRGBO(6, 130, 130, 1)),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'và',
                        style: TextStyle(
                          fontSize: 13,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'Chính sách',
                        style: TextStyle(
                            fontSize: 13,
                            decoration: TextDecoration.underline,
                            color: Color.fromRGBO(6, 130, 130, 1)),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
