//import 'dart:html';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:DecoHome/Views/screen/ManagePurchase.dart';
import 'package:DecoHome/Views/screen/ManageProductPage.dart';
import 'package:DecoHome/Views/screen/UserPage.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:DecoHome/Views/screen/HomePage.dart';
import 'package:DecoHome/Views/screen/DashboardPage.dart';

class AdminMainPage extends StatefulWidget {
  final String userInfo;
  AdminMainPage({this.userInfo}){
    //print(userInfo);
  }
  @override
  _AdminMainPage createState()=> _AdminMainPage(id: userInfo);
}

class _AdminMainPage extends State<AdminMainPage> {
  int _selectedIndex = 0;
  final String id;
  Users users;
  _AdminMainPage({this.id})
  {
    _pages =[DashboardPage(),ManageProductPage(userId: id,), ManagePurchasesPage(), UserPage(users: id,)];
  }
  List<Widget> _pages;
  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    users = await UserService().userInfo(id);
      if (this.mounted) { // check whether the state object is in tree
    setState(() {
      // make changes here
    });
  }
  }
  @override
  Widget build(BuildContext context) {
    //print(id);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: _pages[_selectedIndex],
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              blurRadius: 20,
              color: Colors.black.withOpacity(.1),
            )
          ],
        ),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 8),
            child: GNav(
              gap: 8,
              activeColor: Colors.white,
              iconSize: 24,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
              duration: Duration(milliseconds: 400),
              tabBackgroundColor: Colors.grey[100],
              //color: Colors.black.withOpacity(0.5),
              color: Theme.of(context).hoverColor,
              tabs: [
                GButton(
                  icon: Icons.dashboard_rounded,
                  text: 'Điều kiển',
                  backgroundColor: Theme.of(context).accentColor,
                ),
                GButton(
                  icon: Icons.chair,
                  text: 'Sản phẩm',
                  backgroundColor: Theme.of(context).accentColor,
                ),
                GButton(
                  icon: Icons.shopping_cart_rounded,
                  text: 'Đơn hàng',
                  backgroundColor: Theme.of(context).accentColor,
                ),
                GButton(
                  icon: Icons.people_alt_rounded,
                  text: 'Cá nhân',
                  backgroundColor: Theme.of(context).accentColor,
                ),
              ],
              selectedIndex: _selectedIndex,
              onTabChange: (index) {
                setState(() {
                  _selectedIndex = index;
                });
              },
            ),
          ),
        ),
      ),
    );
  }
}
