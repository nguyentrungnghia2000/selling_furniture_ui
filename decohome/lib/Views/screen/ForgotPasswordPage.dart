import 'package:DecoHome/Views/services/user.service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/screen/SignInPage.dart';
import 'package:DecoHome/Views/screen/SignUpPage.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:math';

class ForgotPassword extends StatefulWidget {
  @override
  State<ForgotPassword> createState() {
    return _Forgot();
  }
}

class _Forgot extends State<ForgotPassword> {
  TextEditingController _emailController = TextEditingController();
  List<Users> listUser = [];
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    listUser = await UserService().getUserList();
  }

  Future sendEmail(String randomPassword) async {
    final url = Uri.parse("https://api.emailjs.com/api/v1.0/email/send");
    const serviceId = "service_6vvytvv";
    const templateId = "template_dzvysql";
    const userId = "WE0BOfl9KJ7IVmi_p";
    final response = await http.post(url,
        headers: {
          'origin': 'http://localhost',
          'Content-Type': 'application/json'
        },
        body: json.encode({
          "service_id": "service_6vvytvv",
          "template_id": "template_dzvysql",
          "user_id": "WE0BOfl9KJ7IVmi_p",
          "template_params": {
            "name": _emailController.text,
            "message": randomPassword,
            "user_email": _emailController.text,
          },
        }));
    return response.statusCode;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        body: Center(
            child: SafeArea(
          child: ListView(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    child: FlatButton(
                      padding: EdgeInsets.only(),
                      child: Icon(Icons.arrow_back_ios_rounded),
                      color: Colors.transparent,
                      minWidth: 50,
                      height: 50,
                      onPressed: () {
                        //print('back to login');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SigninPage(),
                            ));
                        //Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 40),
                  ),
                  Stack(
                    children: <Widget>[
                      Positioned(
                        top: 25,
                        left: 40,
                        child: Container(
                          width: 320,
                          height: 45,
                          decoration: BoxDecoration(
                              color: Theme.of(context).accentColor),
                        ),
                      ),
                      Container(
                        width: 300,
                        decoration:
                            BoxDecoration(color: Color.fromARGB(0, 0, 0, 0)),
                        padding: EdgeInsets.only(),
                        child: Text(
                          'Quên mật khẩu',
                          style: TextStyle(
                              fontSize: 32, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(top: 10, left: 40),
                child: Text(
                  'Nhập email của bạn tại đây',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 80, left: 40),
                child: Text(
                  'tôi sẽ gửi bạn mật khẩu mới',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
              ),
              Container(
                padding:
                    EdgeInsets.only(top: 50, bottom: 100, left: 40, right: 40),
                child: TextField(
                  controller: _emailController,
                  decoration: InputDecoration(
                      border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green)),
                      labelText: 'Email',
                      hintText: 'Enter your email',
                      hintStyle: TextStyle(fontSize: 13)),
                ),
              ),
              Container(
                  child: Align(
                child: SizedBox(
                  width: 240,
                  child: FlatButton(
                    height: 60,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    child: Text(
                      'Quên mật khẩu',
                      style: TextStyle(fontSize: 16),
                    ),
                    color: Color.fromRGBO(6, 130, 130, 1),
                    textColor: Colors.white,
                    onPressed: () async {
                      String randomPassword = generateRandomString(6);
                      sendEmail(randomPassword);
                      String id = UserService()
                          .getUserByEmail(listUser, _emailController.text);
                      UserService().changePasswork(id, randomPassword);
                      showDialog(
                          context: context,
                          builder: (context) => Dialog(
                                child: Container(
                                  height: 170,
                                  width: 150,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(bottom: 16),
                                          child: Text(
                                            "Quên mật khẩu!",
                                            style: TextStyle(fontSize: 24),
                                          )),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 16),
                                        child: Column(
                                          children: [
                                            Text(
                                              "Hệ thống đã gửi mật khẩu đến",
                                              style: TextStyle(fontSize: 16),
                                            ),
                                            Text(
                                              _emailController.text,
                                              style: TextStyle(fontSize: 16,color: Theme.of(context)
                                                        .accentColor),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 16),
                                        child: InkWell(
                                          onTap: () {
                                            Navigator.pop(context);
                                            Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          SigninPage()));
                                          },
                                          child: Container(
                                            //margin: EdgeInsets.only(left: 16),
                                            width: 96,
                                            height: 40,
                                            //padding: EdgeInsets.only(top:0),
                                            child: Center(
                                              child: Text("Xác nhận",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w500)),
                                            ),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                    width: 1.5),
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ));
                    },
                  ),
                ),
              )),
              Container(
                  child: Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 100),
                    child: Text(
                      'Bạn chưa có tài khoản?',
                      style: TextStyle(fontSize: 13),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 5),
                      child: FlatButton(
                        padding: EdgeInsets.only(right: 45),
                        child: Text(
                          'Đăng ký',
                          style: TextStyle(
                              fontSize: 13,
                              decoration: TextDecoration.underline,
                              color: Color.fromRGBO(6, 130, 130, 1)),
                        ),
                        onPressed: () {
                          //print('sign up');
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SignUp(),
                              ));
                        },
                      ))
                ],
              ))
            ],
          ),
        )),
      ),
    );
  }

  String generateRandomString(int len) {
    var r = Random();
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    return List.generate(len, (index) => _chars[r.nextInt(_chars.length)])
        .join();
  }
}
