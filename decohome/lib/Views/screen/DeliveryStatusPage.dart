import 'package:DecoHome/Views/models/cart.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/screen/PaymentStatusPage.dart';

class DeliveryStatusPage extends StatefulWidget {
  final Users users;
  final List<Cart> listCart;
  DeliveryStatusPage({this.users, this.listCart});
  @override
  _DeliveryState createState() => _DeliveryState();
}

class _DeliveryState extends State<DeliveryStatusPage> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 10, right: 10),
                  child: Row(children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        size: 30,
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(right: 5)),
                    Text(
                      "Thông tin vận chuyển",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    )
                  ]),
                ),
                Padding(padding: EdgeInsets.only(top: 15)),
                Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Column(children: <Widget>[
                      Container(
                        width: 500,
                        height: 1.5,
                        child: Text(""),
                        decoration:
                            BoxDecoration(color: Theme.of(context).accentColor),
                      ),
                      Padding(padding: EdgeInsets.only(top: 10)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Đặt hàng",
                            style:
                                TextStyle(color: Theme.of(context).accentColor),
                          ),
                          Padding(padding: EdgeInsets.only(right: 15)),
                          Text(
                            "Vận chuyển",
                            style:
                                TextStyle(color: Theme.of(context).accentColor),
                          ),
                          Padding(padding: EdgeInsets.only(right: 20)),
                          Text("Thanh toán",
                              style: TextStyle(
                                  color: Colors.black.withOpacity(0.5))),
                          Padding(padding: EdgeInsets.only(right: 20)),
                          Text("Hoàn thành",
                              style: TextStyle(
                                  color: Colors.black.withOpacity(0.5))),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 20, right: 10, top: 5),
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: 15,
                              width: 15,
                              child: Text(""),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Theme.of(context).accentColor),
                            ),
                            Container(
                              height: 3,
                              width: 75,
                              child: Text(""),
                              decoration: BoxDecoration(
                                  color: Theme.of(context).accentColor),
                            ),
                            Container(
                              height: 15,
                              width: 15,
                              child: Text(""),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Theme.of(context).accentColor),
                            ),
                            Container(
                              height: 3,
                              width: 90,
                              child: Text(""),
                              decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.5)),
                            ),
                            Container(
                              height: 15,
                              width: 15,
                              child: Text(""),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.black.withOpacity(0.5)),
                            ),
                            Container(
                              height: 3,
                              width: 90,
                              child: Text(""),
                              decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.5)),
                            ),
                            Container(
                              height: 15,
                              width: 15,
                              child: Text(""),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.black.withOpacity(0.5)),
                            ),
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 10)),
                      Container(
                        width: 500,
                        height: 1.5,
                        child: Text(""),
                        decoration:
                            BoxDecoration(color: Theme.of(context).accentColor),
                      ),
                    ])),
                Padding(
                  padding: EdgeInsets.only(top: 20, left: 20),
                  child: Text(
                    "Thông tin",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(
                        top: 10, bottom: 10, left: 30, right: 30),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Text(
                                "Tên",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                              //Padding(padding: EdgeInsets.only(bottom:5)),
                              Container(
                                  margin: EdgeInsets.only(top: 15),
                                  child: TextField(
                                    controller: _nameController,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: widget.users.name,
                                        hintStyle: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),
                                  )),
                              Positioned(
                                  top: 50,
                                  child: Container(
                                    height: 1,
                                    width: 500,
                                    child: Text(""),
                                    decoration: BoxDecoration(
                                        color: Theme.of(context).accentColor),
                                  ))
                            ],
                          ),
                          Stack(
                            children: <Widget>[
                              Text(
                                "Số điện thoại",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 15),
                                  child: TextField(
                                    controller: _phoneController,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: widget.users.phoneNum,
                                        hintStyle: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),
                                  )),
                              Positioned(
                                  top: 50,
                                  child: Container(
                                    height: 1,
                                    width: 500,
                                    child: Text(""),
                                    decoration: BoxDecoration(
                                        color: Theme.of(context).accentColor),
                                  ))
                            ],
                          ),
                          Stack(
                            children: <Widget>[
                              Text(
                                "Địa chỉ",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                              //Padding(padding: EdgeInsets.only(bottom:5)),
                              Container(
                                margin: EdgeInsets.only(top: 15),
                                child: TextField(
                                  controller: _addressController,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: widget.users.address,
                                      hintStyle: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16)),
                                ),
                              ),
                              Positioned(
                                  top: 50,
                                  child: Container(
                                    height: 1,
                                    width: 500,
                                    child: Text(""),
                                    decoration: BoxDecoration(
                                        color: Theme.of(context).accentColor),
                                  ))
                            ],
                          ),
                        ])),
                Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text(
                    "Vận chuyển",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                  ),
                ),
                DeliveryType(),
                Padding(
                    padding: EdgeInsets.only(top: 200, left: 20, right: 20),
                    child: InkWell(
                      onTap: () {
                        if (_nameController.text != "" ||
                            _phoneController.text != "" ||
                            _addressController.text != "") {
                          showDialog(
                              context: context,
                              builder: (context) => Dialog(
                                    child: Container(
                                        height: 190,
                                        width: 100,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 16),
                                                child: Text(
                                                  "Cập nhật thông tin!",
                                                  style:
                                                      TextStyle(fontSize: 24),
                                                )),
                                            Container(
                                              margin:
                                                  EdgeInsets.only(bottom: 16),
                                              child: Column(
                                                children: [
                                                  Text(
                                                    "Bạn có muốn cập nhật thông tin",
                                                    style:
                                                        TextStyle(fontSize: 16),
                                                  ),
                                                  Text(
                                                    "mới không!",
                                                    style:
                                                        TextStyle(fontSize: 16),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                    if (_nameController.text ==
                                                        "") {
                                                      _nameController.text =
                                                          widget.users.name;
                                                      print(
                                                          _nameController.text);
                                                    }
                                                    if (_phoneController.text ==
                                                        "") {
                                                      _phoneController.text =
                                                          widget.users.phoneNum;
                                                    }
                                                    if (_addressController
                                                            .text ==
                                                        "") {
                                                      _addressController.text =
                                                          widget.users.address;
                                                    }
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              PaymentStatusPage(
                                                            userId:
                                                                widget.users.id,
                                                            userName:
                                                                _nameController
                                                                    .text,
                                                            userPhone:
                                                                _phoneController
                                                                    .text,
                                                            userAddress:
                                                                _addressController
                                                                    .text,
                                                            listCart:
                                                                widget.listCart,
                                                          ),
                                                        ));
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 16),
                                                    width: 120,
                                                    height: 40,
                                                    //padding: EdgeInsets.only(top:0),
                                                    child: Center(
                                                      child: Text("Hủy",
                                                          style: TextStyle(
                                                              color: Colors.red,
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                    ),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: Colors.red,
                                                            width: 1.5),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5)),
                                                  ),
                                                ),
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 16)),
                                                InkWell(
                                                  onTap: () {
                                                    UserService()
                                                        .updateUserInfo(
                                                            widget.users.id,
                                                            _nameController
                                                                .text,
                                                            "",
                                                            _phoneController
                                                                .text,
                                                            _addressController
                                                                .text,
                                                            "",
                                                            "",
                                                            "");
                                                    Navigator.pop(context);
                                                    if (_nameController.text ==
                                                        "") {
                                                      _nameController.text =
                                                          widget.users.name;
                                                      print(
                                                          _nameController.text);
                                                    }
                                                    if (_phoneController.text ==
                                                        "") {
                                                      _phoneController.text =
                                                          widget.users.phoneNum;
                                                    }
                                                    if (_addressController
                                                            .text ==
                                                        "") {
                                                      _addressController.text =
                                                          widget.users.address;
                                                    }
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              PaymentStatusPage(
                                                            userId:
                                                                widget.users.id,
                                                            userName:
                                                                _nameController
                                                                    .text,
                                                            userPhone:
                                                                _phoneController
                                                                    .text,
                                                            userAddress:
                                                                _addressController
                                                                    .text,
                                                            listCart:
                                                                widget.listCart,
                                                          ),
                                                        ));
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 16),
                                                    width: 120,
                                                    height: 40,
                                                    //padding: EdgeInsets.only(top:0),
                                                    child: Center(
                                                      child: Text("Đồng ý",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                    ),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .accentColor,
                                                            width: 1.5),
                                                        color: Theme.of(context)
                                                            .accentColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5)),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        )),
                                  ));
                        } else {
                          if (_nameController.text == "") {
                            _nameController.text = widget.users.name;
                            print(_nameController.text);
                          }
                          if (_phoneController.text == "") {
                            _phoneController.text = widget.users.phoneNum;
                          }
                          if (_addressController.text == "") {
                            _addressController.text = widget.users.address;
                          }
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => PaymentStatusPage(
                                  userId: widget.users.id,
                                  userName: _nameController.text,
                                  userPhone: _phoneController.text,
                                  userAddress: _addressController.text,
                                  listCart: widget.listCart,
                                ),
                              ));
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Tiếp theo",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20)),
                          ],
                        ),
                        decoration: BoxDecoration(
                            //border: Border.all(color:Theme.of(context).accentColor),
                            borderRadius: BorderRadius.circular(30),
                            color: Theme.of(context).accentColor),
                      ),
                    ))
              ],
            ),
          ],
        ),
      )),
    );
  }
}

class DeliveryType extends StatefulWidget {
  DeliveryType({Key key}) : super(key: key);
  @override
  _DeliveryType createState() => _DeliveryType();
}

class _DeliveryType extends State<DeliveryType> {
  String deliveryType = '';
  Color theme = Color.fromRGBO(6, 130, 130, 50);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
            child: Row(
              children: <Widget>[
                Text("Vận chuyển nhanh",
                    style:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                Padding(padding: EdgeInsets.only(right: 150)),
                Radio(
                    value: '1',
                    groupValue: deliveryType,
                    onChanged: (val) {
                      deliveryType = val;
                      setState(() {});
                    }),
              ],
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                    color: Theme.of(context).accentColor, width: 1.5)),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            padding: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
            child: Row(
              children: <Widget>[
                Text("Vận chuyển thường",
                    style:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                Padding(padding: EdgeInsets.only(right: 145)),
                Radio(
                    value: '0',
                    groupValue: deliveryType,
                    onChanged: (val) {
                      deliveryType = val;
                      setState(() {});
                    }),
              ],
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                    color: Theme.of(context).accentColor, width: 1.5)),
          )
        ],
      ),
    );
  }
}
