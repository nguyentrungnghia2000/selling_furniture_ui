import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/favorite.service.dart';
import 'package:DecoHome/Views/services/cart.service.dart';
import 'package:DecoHome/Views/screen/CartPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:DecoHome/camera/unity_camera.dart';

import '../../camera/unity_camera.dart';

class ProductInfoPage extends StatefulWidget {
  final Product prod;
  final String userId;
  bool isInCart;
  Function callbackFunction;
  ProductInfoPage({this.prod, this.userId, this.callbackFunction});
  @override
  _ProductInfoState createState() => _ProductInfoState();
}

class _ProductInfoState extends State<ProductInfoPage> {
  Image noImage = Image.asset("assets/Ghe_1.png");
  bool isFavo;
  bool isInCart;
  callback() {
    setState(() {
      loadInitData();
    });
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    isFavo = await FavoriteService().isFavo(widget.userId, widget.prod.prodid);
    isInCart = await CartService().isInCart(widget.userId, widget.prod.prodid);
    if (this.mounted) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
          child: Container(
        decoration: BoxDecoration(color: Colors.black.withOpacity(0.1)),
        child: ListView(children: [
          Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Stack(children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10, right: 10),
                    child: Row(children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          size: 30,
                        ),
                      )
                    ]),
                  ),
                  Container(
                    height: 440,
                    padding: EdgeInsets.only(top: 30),
                    child: Image.network(
                      widget.prod.pic,
                      loadingBuilder: (context, child, loadingProgress) =>
                          (loadingProgress == null)
                              ? child
                              : CircularProgressIndicator(),
                      errorBuilder: (context, error, stackTrace) => noImage,
                      fit: BoxFit.fitWidth,
                    ),
                  )
                ]),
                Container(
                  child: Stack(overflow: Overflow.visible, children: <Widget>[
                    Container(
                      child: CustomPaint(
                        size: Size(screenWidth, 100),
                        painter: RPSCustomPainter(),
                      ),
                    ),
                    Positioned(
                        child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 5, left: 55),
                          child: Text(
                            "Chi tiết",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(top: 5, left: 155),
                            child: FlatButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => UnityCamera(),
                                      ));
                                },
                                child: Text("Xem AR",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500))))
                      ],
                    )),
                    Positioned(
                      left: 175,
                      top: -30,
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            if (isFavo == true) {
                              isFavo = false;
                              FavoriteService().deleteFavoItem(
                                  widget.userId, widget.prod.prodid);
                              showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (context) => Dialog(
                                        child: Container(
                                            height: 190,
                                            width: 100,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 16),
                                                    child: Text(
                                                      "Hủy yêu thích!",
                                                      style: TextStyle(
                                                          fontSize: 24),
                                                    )),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      bottom: 16),
                                                  child: Column(
                                                    children: [
                                                      Text(
                                                        "Xóa sản phẩn khỏi danh sách",
                                                        style: TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                      Text(
                                                        "yêu thích thành công!",
                                                        style: TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                    if (widget
                                                            .callbackFunction !=
                                                        null) {
                                                      widget.callbackFunction();
                                                    }
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 16),
                                                    width: 120,
                                                    height: 40,
                                                    //padding: EdgeInsets.only(top:0),
                                                    child: Center(
                                                      child: Text("Hoàn thành",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)),
                                                    ),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: Theme.of(
                                                                    context)
                                                                .accentColor,
                                                            width: 1.5),
                                                        color: Theme.of(context)
                                                            .accentColor,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5)),
                                                  ),
                                                ),
                                              ],
                                            )),
                                      ));
                            } else {
                              isFavo = true;
                              FavoriteService().createFavoItem(
                                  widget.userId, widget.prod.prodid);
                              if (widget.callbackFunction != null) {
                                widget.callbackFunction();
                              }
                            }
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Icon(
                            isFavo == true
                                ? Icons.favorite
                                : Icons.favorite_border,
                            size: 40,
                            color: Theme.of(context).accentColor,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40),
                              color: Colors.white),
                        ),
                      ),
                    ),
                    Positioned(
                        top: 39,
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 15, left: 20, right: screenWidth - 50),
                          decoration: BoxDecoration(color: Colors.white),
                          child: Text(
                            widget.prod.name,
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                        )),
                    Positioned(
                        top: 76,
                        child: Container(
                            padding: EdgeInsets.only(
                                left: 20, right: screenWidth - 50, bottom: 1),
                            decoration: BoxDecoration(color: Colors.white),
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.star_rate_rounded,
                                  color: Colors.yellow,
                                ),
                                Icon(
                                  Icons.star_rate_rounded,
                                  color: Colors.yellow,
                                ),
                                Icon(
                                  Icons.star_rate_rounded,
                                  color: Colors.yellow,
                                ),
                                Icon(
                                  Icons.star_rate_rounded,
                                  color: Colors.yellow,
                                ),
                                Icon(
                                  Icons.star_rate_rounded,
                                  color: Colors.yellow,
                                ),
                              ],
                            )))
                  ]),
                  //decoration: BoxDecoration(color: Colors.white),
                ),
                Container(
                  //height: 400,
                  padding: EdgeInsets.only(top: 10, left: 20),
                  child: Row(children: <Widget>[
                    Text(
                      "Mô tả sản phẩm",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                  ]),
                  decoration: BoxDecoration(color: Colors.white),
                ),
                Container(
                  height: 338,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          widget.prod.prodInfo,
                          textAlign: TextAlign.justify,

                          //softWrap: true,
                        ),
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(color: Colors.white),
                ),
              ]),
        ]),
      )),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(left: 20, top: 10, bottom: 13),
        child: Container(
          child: Row(
            children: <Widget>[
              Container(
                width: 130,
                child: Row(
                  children: [
                    Text(
                      widget.prod.price.toString(),
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                    ),
                    Text(
                      "đ",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              ),
              //Padding(padding: EdgeInsets.only(left: 70)),
              Container(
                padding: EdgeInsets.all(8),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CartPage(
                            userId: widget.userId,
                            callbackFunction: callback,
                          ),
                        ));
                  },
                  child: Icon(
                    Icons.shopping_cart_rounded,
                    color: isInCart == true
                        ? Colors.white
                        : Colors.black.withOpacity(0.3),
                  ),
                ),
                decoration: BoxDecoration(
                    color: isInCart == true
                        ? Theme.of(context).accentColor
                        : Colors.black.withOpacity(0.3),
                    borderRadius: BorderRadius.circular(10)),
              ),
              Padding(padding: EdgeInsets.only(left: 40)),
              InkWell(
                onTap: () {
                  setState(() {
                    if (isInCart == false) {
                      isInCart = true;
                      CartService().createCartItem(
                          widget.userId, widget.prod.prodid, widget.prod.price);
                    } else {
                      isInCart = false;
                      CartService()
                          .deleteCartItem(widget.userId, widget.prod.prodid);
                    }
                  });
                },
                child: Container(
                    width: 170,
                    padding:
                        EdgeInsets.only(top: 9, bottom: 9, left: 10, right: 10),
                    decoration: BoxDecoration(
                        color: isInCart == true
                            ? Colors.black.withOpacity(0.3)
                            : Theme.of(context).accentColor,
                        borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          isInCart == true ? "Xóa giỏ hàng" : "Thêm giỏ hàng",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: isInCart == true
                                  ? Colors.black.withOpacity(0.3)
                                  : Colors.white),
                        ),
                      ],
                    )),
              )
            ],
          ),
        ),
        decoration: BoxDecoration(color: Colors.white),
      ),
    );
  }
}

class RPSCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint_0 = new Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;

    Path path_0 = Path();
    path_0.moveTo(0, size.height * 0.3967000);
    path_0.quadraticBezierTo(size.width * 0.0001000, size.height * 0.0035000,
        size.width * 0.1002750, 0);
    path_0.lineTo(size.width * 0.4000750, 0);
    path_0.quadraticBezierTo(size.width * 0.4160750, size.height * 0.3987000,
        size.width * 0.5006000, size.height * 0.3996000);
    path_0.quadraticBezierTo(size.width * 0.5861750, size.height * 0.3988000,
        size.width * 0.5999500, 0);
    path_0.quadraticBezierTo(
        size.width * 0.8318000, 0, size.width * 0.9007500, 0);
    path_0.quadraticBezierTo(size.width * 0.9998250, size.height * -0.0018000,
        size.width, size.height * 0.4019000);
    path_0.quadraticBezierTo(size.width * 0.7500000, size.height * 0.4006000, 0,
        size.height * 0.3967000);
    path_0.close();

    canvas.drawPath(path_0, paint_0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
