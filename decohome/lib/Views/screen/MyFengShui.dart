import 'package:DecoHome/Views/screen/MainPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FengShui extends StatefulWidget {
  final String menh;
  final String userid;
  FengShui({this.menh, this.userid});
  @override
  State<FengShui> createState() {
    return _FengShuiPage();
  }
}

class _FengShuiPage extends State<FengShui> {
  @override
  Widget build(BuildContext context) {
    //print(listUser);
    // TODO: implement build
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: Column(
            children: [
              Container(
                      padding: EdgeInsets.only(top: 10, right: 10),
                      child: Row(children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back_ios_rounded,
                            size: 30,
                          ),
                        )
                      ]),
                    ),
              Column(        
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    iconMenh(widget.menh),
                    height: 100,
                    width: 100,
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Mệnh ",
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 24),
                      ),
                      Text(widget.menh,
                          style:
                              TextStyle(fontWeight: FontWeight.w500, fontSize: 24,
                              color: (widget.menh=="Mộc")?Color(0xff009343):
                              (widget.menh=="Hỏa")?Color(0xffe43309):
                              (widget.menh=="Kim")?Color(0xff939498):
                              (widget.menh=="Thủy")?Color(0xff24629f):
                              Color(0xffe09d32))),
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Màu sắc phù hợp: ",
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                      ),
                      Text(
                        mauSac(widget.menh),
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 4)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Gợi ý phong thủy: ",
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(goiY(widget.menh),
                            textAlign: TextAlign.justify,
                            style: TextStyle(fontSize: 16)),
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 4)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Lưu ý: ",
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(luuY(widget.menh),
                            textAlign: TextAlign.justify,
                            style: TextStyle(fontSize: 16)),
                      )
                    ],
                  ),
                                    Padding(padding: EdgeInsets.only(bottom: 16)),

                ],
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(16),
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MainPage(
                          userInfo: widget.userid,
                        )));
          },
          child: Container(
            padding: EdgeInsets.only(top: 16, bottom: 16),
            margin: EdgeInsets.only(top: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Mua sản phẩm",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
              ],
            ),
            decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.circular(10)),
          ),
        ),
      ),
    ));
  }

  String iconMenh(String menh) {
    if (menh == "Hỏa") {
      return 'assets/Hoa.svg';
    } else if (menh == "Kim") {
      return 'assets/Kim.svg';
    } else if (menh == "Mộc") {
      return 'assets/Moc.svg';
    } else if (menh == "Thổ") {
      return 'assets/Tho.svg';
    } else {
      return 'assets/Thuy.svg';
    }
  }

  String mauSac(String menh) {
    if (menh == "Hỏa") {
      return "Đỏ, Cam, Hồng, Tím";
    } else if (menh == "Kim") {
      return "Trắng, Vàng";
    } else if (menh == "Mộc") {
      return "Xanh lá cây, Màu Gỗ";
    } else if (menh == "Thổ") {
      return "Vàng, Nâu Vàng, Nâu Đất";
    } else {
      return "Xanh nước biển, Đen";
    }
  }

  String goiY(String menh) {
    if (menh == "Hỏa") {
      return "Đối với người mệnh Hỏa, bạn nên sử dụng những nội thất có màu sắc như Đỏ, Cam, Hồng, Tím. Ngoài ra, theo quy tắc cây cối sinh lửa những mà sắc thuộc hành Mộc như Xanh lá cây, màu Gỗ cũng rất phù hợp với những người mệnh Hỏa như bạn.\nTuy nhiên, khi sử dụng quá nhiều màu nóng sẽ làm cho căn phòng của bạn trở nên bức bối, nóng nực. Vì vậy, bạn chỉ nên sử dụng những nội thất có màu sắc rực rỡ chỉ để là điểm nhấn, ngoài ra nên sử dụng thêm nhiều cây xanh để trang trí và sử dụng đồ nội thất màu gỗ để trung hòa lại không gian.";
    } else if (menh == "Kim") {
      return "Khi lựa chọn đồ nội thất cho những người mệnh Kim nên lấy màu trắng làm màu chủ đạo để phù hợp với bản mệnh của bản thân. Ngoài ra, màu Vàng cũng là màu sắc được coi là màu sắc mang lại nhiều may mắn và tài lộc cho người mệnh Kim vì thế hay khéo léo những màu sắc trên với nhau để tạo cho căn phòng của bản thân có nhiều điểm nhấn hơn.\nĐặc biệt, căn phòng của người có mệnh Kim nên sử dụng những đồ nội thất được làm bằng kim loại như Vàng, Đồng, Inox,... hoặc những đồ bằng Thủy tinh, Gốm sứ";
    } else if (menh == "Mộc") {
      return "Đối với những người mệnh Mộc, nên lựa chọn những loại đồ dùng nội thất từ những chất liệu tự nhiên như gỗ, tre cho bàn ghế, kệ tủ. Những đồ nội thất được làm bằng vậy liệu tự nhiên sẽ làm cho căn nhà trở nên sáng sủa, tốt cho sự phát triển và cuộc sống của người mệnh mộc\nTheo quan hệ tương sinh của ngũ hành thì Thủy duy trì sự sống của Mộc. Vì thế khi sử dụng trang trí, người chủ nhà nên chú ý đến việc tạo thủy khí trong không gian. Ngoài những vật trang trí, đồ nội thất màu Xanh lá cây, màu Gỗ hoặc những cây cảnh, hoa chủ nhà có thể suy nghĩ đến việc làm lỗi đi uốn lượn như nước hoặc trang trí nên bể cá, hồ thủy sinh.";
    } else if (menh == "Thổ") {
      return "Đa số những người có mệnh Thổ thường có tính cách giản dị, ôn hòa nên việc thiết kế nhà cửa cần toát lên vẻ đẹp thanh thoát, bình dị nhưng vẫn không thiếu đi phần thanh lịch cho căn nhà. Màu sắc phù hợp với những người mệnh Thổ là màu Vàng, Nâu Vàng, Nâu Đất.\nNgoài ra, chủ nhà cũng có thể sử dụng những đồ trang trí có màu trắng màu của những người mệnh Kim theo quy tắc tương sinh Thổ sinh Kim";
    } else {
      return "Hai màu sắc cơ bản mà bạn cần phải nhớ khi trang trí nhà co người mang mệnh Thủy là màu Xanh nước biển và màu Đen. Trong khi màu Xanh nước biển mang trong mình tính cách hiền hòa, tinh tế thì màu Đen mang đến cho người nhin sự huyền bí và đây là kết hợp hoàn hảo khi trang trí căn nhà của bạn.\nNgoài ra, người mệnh Thủy cũng có thể lựa chọn những đồ nội thất làm từ gỗ, kim loại hoặc kính, thủy tinh. Nên phối hợp nhiều màu sắc nhiều chất liệu lại với nhau để trung hòa màu sắc giúp căn nhà trở nên đa dạng và mới mẻ hơn.";
    }
  }

  String luuY(String menh) {
    if (menh == "Hỏa") {
      return "Khi sử dụng màu sắc khác, bạn nên tránh những đồ nội thất sử dụng màu chủ đạo là Xanh nước biển hoặc màu Đen, đây là những màu tượng trưng của mệnh Thủy, là màu tương khắc với với những người mệnh Hỏa như bạn.";
    } else if (menh == "Kim") {
      return "Khác với những người có mệnh Hỏa, những người có mệnh Kim nên sử dụng những chậu hoa để trang trí thay vì sử dụng cây cảnh. Một số loại hoa mà bạn có thể cân nhắc như hoa Ngọc Lan, hoa Kim Ngân,...";
    } else if (menh == "Mộc") {
      return "Ngược lại với những yếu tố trên, nếu sử dụng quá ít những đồ nội thất hợp mệnh sẽ làm cho bạn cảm thấy mệt mỏi, trì trệ trong công việc và giảm đi sự phát triển của bản thân.";
    } else if (menh == "Thổ") {
      return "Mặc dù Mộc khắc Thổ, nhưng khi trang trí nhà bằng những đồ nội thất được làm bằng vật liệu thiên nhiên như tre, gỗ,... cho căn nhà của người hệ Thổ sẽ làm cho căn nhà cảm giác mộc mạc, có sức sống hơn so với chỉ sử dụng những màu sắc phù hợp với mệnh Thổ và Kim";
    } else {
      return "Tuyệt đối nên tránh những màu của mệnh Thổ như Vàng, Nâu hoặc những đồ được làm từ đất như đồ gốm sứ khi sử dụng lâu đài sẽ không tốt cho sự phát triển của mệnh Thủy. Và nên sử dụng đồ nội thất có hình tròn, trụ tránh dùng những đồ có cạnh sắc nhọt sẽ không phù hợp với phong thủy.";
    }
  }
}
