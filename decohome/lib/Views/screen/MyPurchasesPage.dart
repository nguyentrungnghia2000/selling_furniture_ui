import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:DecoHome/Views/widget/PurchasesListItem.dart';

class MyPurchesePage extends StatefulWidget {
  final String userId;
  MyPurchesePage({this.userId});
  @override
  _MyPurchaseState createState() => _MyPurchaseState();
}

class _MyPurchaseState extends State<MyPurchesePage> {
  int currentIndex = 0;
  List<String> purchaseStateList = ['Chờ xác nhận','Đang giao hàng','Lịch sử','Bị hủy'];
  PurchaseList purchaseList;
  @override
  Widget build(BuildContext buildContext) {
    //print(purchaseStateList.length);
    return Scaffold(
        body: SafeArea(
            child: Container(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 10, right: 10),
              child: Row(children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios_rounded,
                    size: 30,
                  ),
                ),
                Padding(padding: EdgeInsets.only(right: 5)),
                Text(
                  "Đơn hàng của tôi",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                )
              ]),
            ),
            Padding(padding: EdgeInsets.only(top: 5)),
            Container(
                padding: EdgeInsets.only(left: 5, right: 10),
                child: Stack(children: <Widget>[
                  Positioned(
                      top: 31,
                      left: 20,
                      child: Container(
                        height: 3,
                        width: 360,
                        decoration:
                            BoxDecoration(color: Colors.black.withOpacity(0.3)),
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    height: 30,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: purchaseStateList.length,
                        itemBuilder: (context, index) => GestureDetector(
                              child: Container(
                                alignment: Alignment.center,
                                //margin: EdgeInsets.only(right: 5),
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: Column(
                                  children: [
                                    Text(
                                      purchaseStateList[index],
                                      style: TextStyle(
                                          color: index == currentIndex
                                              ? Theme.of(context).accentColor
                                              : Colors.black.withOpacity(0.3),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Container(
                                      height: 3,
                                      width: 80,
                                      decoration: BoxDecoration(
                                          color: index == currentIndex
                                              ? Theme.of(context).accentColor
                                              : Colors.transparent),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  currentIndex = index;
                                });
                              },
                            )),
                  ),
                ])),
                Container(
                  height: 700,
                  child: purchaseList= PurchaseList(deliveryState:currentIndex,userId: widget.userId,),)
          ]),
    )));
  }
}

class EmptyPurchase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 160),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            children: [
              SvgPicture.asset('assets/EmtyCard.svg'),
              Padding(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Text("Your purchase is empty!",
                    style: TextStyle(
                        fontSize: 24, color: Theme.of(context).accentColor)),
              ),
              Text(
                "Go a head, order some items",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.black.withOpacity(0.5)),
              ),
              Text(
                "from the menu.",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.black.withOpacity(0.5)),
              ),
              Padding(
                padding: EdgeInsets.only(top: 50),
                child: Container(
                  padding:
                      EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.add_outlined,
                        size: 30,
                        color: Colors.white,
                      ),
                      Text(
                        "Add item",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: Colors.white),
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Theme.of(context).accentColor),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
