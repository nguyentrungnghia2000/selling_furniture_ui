import 'package:DecoHome/Views/screen/SignInPage.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:flutter/material.dart';

class OnBoardPage extends StatefulWidget {
  @override
  _OnBoardPageState createState() => _OnBoardPageState();
}

class _OnBoardPageState extends State<OnBoardPage> {
  List<String> title = ["Hiện đại", "Thoải mái", "Đa dạng"];
  List<String> body1 = [
    "Chúng tôi cung cấp cho bạn một phong cách",
    "Giá cả cực kỳ phải chăng cho những món đồ",
    "Chúng tôi có nhiều sản phẩm chất lượng"
  ];
  List<String> body2 = [
    "cách tối giản và vật liệu hiện đại.",
    "nội thất bạn có thể mua ở chúng tôi.",
    "cao nhiều mẫu mã cho bạn lựa chọn."
  ];
  List<String> image = ["Onboard_1.jpg", "Onboard_2.jpg", "Onboard_3.jpg"];
  int currentPage=0;
  @override
  Widget build(BuildContext context) {
    //print(string.length);
    return Scaffold(
        body: Container(
          //scrollDirection: Axis.vertical,
          child: Column(
              children: [
          Expanded(
            flex: 4,
            child: PageView.builder(
              onPageChanged: (value){
                setState(() {
                  currentPage=value;
                });
              },
              itemCount: title.length,
              itemBuilder: (context, index) => Container(
                child: SplashContent(
                  title: title[index],
                  body1: body1[index],
                  body2: body2[index],
                  image: image[index],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: List.generate(title.length, (index) => buildDot(index:index)),),
                    Container(
            padding: EdgeInsets.only(top: 100, left: 15, right: 15),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    InkWell(onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SigninPage(),
                        ));
                    },
                                        child: Container(
                          padding: EdgeInsets.all(3),
                          decoration: BoxDecoration(
                              color: Theme.of(context).hoverColor,
                              borderRadius: BorderRadius.circular(5)),
                          child: Icon(
                            Icons.arrow_forward_ios_rounded,
                            size: 30,
                            color: Theme.of(context).accentColor,
                          )),
                    )
                  ],
                ))
              ],
            ),
          )
              ],
            )
          ),
              ],
            ),
        ));
  }

  Container buildDot({int index}) {
    return Container(
      margin: EdgeInsets.only(right:5),
      padding: EdgeInsets.all(2),
      height: 20,
      width: 20,
      child: Container(
        child: Text(""),
        decoration: BoxDecoration(
            color: currentPage==index? Theme.of(context).accentColor:Theme.of(context).accentColor.withOpacity(0.5),
            borderRadius: BorderRadius.circular(10)),
      ),
      decoration: BoxDecoration(
          border: Border.all(color: currentPage==index? Theme.of(context).accentColor:Colors.white, width: 1.5),
          borderRadius: BorderRadius.circular(10)),
    );
  }
}

class SplashContent extends StatelessWidget {
  const SplashContent({
    Key key,
    this.title,
    this.body1,
    this.body2,
    this.image,
  }) : super(key: key);
  final String title;
  final String body1;
  final String body2;
  final String image;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Image(
          image: AssetImage(image),
        ),
        Padding(
            child: Text(title,
                style: TextStyle(fontSize: 26, fontWeight: FontWeight.w600)),
            padding: EdgeInsets.only(top: 30)),
        Padding(
            child: Text(body1,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
            padding: EdgeInsets.only(top: 15)),
        Text(body2,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
        Padding(padding: EdgeInsets.only(top: 20)),
      ],
    );
  }
}
