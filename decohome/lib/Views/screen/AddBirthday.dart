import 'package:DecoHome/Views/screen/MainPage.dart';
import 'package:DecoHome/Views/screen/MyFengShui.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class AddBirthday extends StatefulWidget {
  final String userid;
  AddBirthday({this.userid});
  @override
  State<AddBirthday> createState() {
    return _AddBirthdayPage();
  }
}

class _AddBirthdayPage extends State<AddBirthday> {
  TextEditingController _yearController = TextEditingController();
  DateTime date = DateTime(2022, 01, 01);
  @override
  Widget build(BuildContext context) {
    //print(listUser);
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        body: Center(
            child: Container(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  margin: EdgeInsets.only(bottom: 16),
                  child: Text(
                    "Nhập năm sinh",
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).accentColor),
                    textAlign: TextAlign.justify,
                  )),
              Container(
                  padding: EdgeInsets.only(left: 4, right: 4),
                  margin: EdgeInsets.only(bottom: 16),
                  child: Text(
                    "Hãy nhập năm sinh (Âm lịch) của bạn để chúng tôi gợi ý sản phẩm phù hợp nhất với bạn nhé!",
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.justify,
                  )),
              Container(
                child: Container(
                  padding: EdgeInsets.all(16),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    controller: _yearController,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Năm sinh của bạn",
                        hintStyle: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16)),
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
              ),
            ],
          ),
        )),
        bottomNavigationBar: Container(
          padding: EdgeInsets.only(bottom: 16),
          child: Container(
            padding: EdgeInsets.only(top: 16, bottom: 16),
            margin: EdgeInsets.only(top: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainPage(
                                  userInfo: widget.userid,
                                )));
                  },
                  child: Container(
                    width: 185,
                    padding: EdgeInsets.only(top: 16, bottom: 16),
                    margin: EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Hủy",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.red),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        //color: Colors.red,
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          color: Colors.red,
                          width: 2,
                        )),
                  ),
                ),
                InkWell(
                  onTap: () {
                    String element = UserService().menhNguHanh(
                        UserService()
                            .getDiachi(int.parse(_yearController.text)),
                        UserService()
                            .getThienCan(int.parse(_yearController.text)));
                    UserService().updateUserInfo(widget.userid, "", "", "", "",
                        "", _yearController.text, element);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FengShui(
                                  menh: element,
                                  userid: widget.userid,
                                )));
                  },
                  child: Container(
                    width: 185,
                    margin: EdgeInsets.only(left: 16, top: 16),
                    padding: EdgeInsets.only(top: 16, bottom: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Xác nhận",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
