//import 'dart:html';
import 'dart:io';
import 'dart:async';

import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/screen/ChagePasswordPage.dart';
import 'package:DecoHome/Views/screen/FullFengShui.dart';
import 'package:DecoHome/Views/screen/MyFengShui.dart';
import 'package:DecoHome/Views/services/sign.service.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/screen/CartPage.dart';
import 'package:DecoHome/Views/screen/MyPurchasesPage.dart';
import 'package:DecoHome/Views/screen/SignInPage.dart';
import 'package:DecoHome/Views/screen/MyInformation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';

class UserPage extends StatefulWidget {
  final String users;
  UserPage({this.users}) {}
  @override
  _UserState createState() => _UserState();
}

class _UserState extends State<UserPage> {
  final SignService _auth = new SignService();
  Image noImage = Image.asset("assets/Ghe_1.png");
  String defaulAVT = "";
  Users users;

  final ImagePicker _picker = ImagePicker();
  callback() {
    setState(() {
      loadInitData();
    });
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    users = await UserService().userInfo(widget.users);

        if (this.mounted) {
      setState(() {
        if (users.photoUrl != "") {
        defaulAVT = users.photoUrl;
      }
      });
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 10, top: 10, right: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(children: <Widget>[
                  Expanded(
                      child: Text(
                    "Cá nhân",
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  )),
                  (widget.users == '2LKx0QrDJIO5tziT8OIvQFaAFep2')
                      ? Container()
                      : InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => CartPage(
                                    userId: users.id,
                                  ),
                                ));
                          },
                          child: Icon(
                            Icons.shopping_cart_rounded,
                            color: Colors.black,
                            size: 28,
                          ),
                        )
                ]),
                Padding(padding: EdgeInsets.only(top: 25, bottom: 25)),
                Container(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                      InkWell(
                        onTap: () {
                          _pickImage(1);
                        },
                        child: Container(
                          padding: EdgeInsets.all(7.5),
                          child: Icon(
                            Icons.camera_alt_rounded,
                            size: 30,
                            color: Theme.of(context).accentColor,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: Theme.of(context).hintColor),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(left: 5)),
                      Container(
                        width: 150,
                        height: 150,
                        child: CircleAvatar(
                          radius: 150,
                          child: defaulAVT == ""
                              ? Icon(
                                  Icons.add_a_photo,
                                  size: 30,
                                  color: Colors.white,
                                )
                              : null,
                          backgroundImage:
                              defaulAVT != "" ? NetworkImage(defaulAVT) : null,
                          backgroundColor: Theme.of(context).accentColor,
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Theme.of(context).accentColor,
                            width: 5,
                          ),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(75),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(left: 5)),
                      InkWell(
                        onTap: () {
                          _pickImage(2);
                        },
                        child: Container(
                          padding: EdgeInsets.all(7.5),
                          child: Icon(
                            Icons.image,
                            size: 30,
                            color: Theme.of(context).accentColor,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: Theme.of(context).hintColor),
                        ),
                      )
                    ])),
                Padding(padding: EdgeInsets.only(top: 15, bottom: 15)),
                (widget.users == '2LKx0QrDJIO5tziT8OIvQFaAFep2')
                    ? Container(
                      padding: EdgeInsets.only(bottom: 250),
                      child: Container(
                          //height: 200,
                          child: Padding(
                              padding: EdgeInsets.only(right: 10, left: 10),
                              child: InkWell(
                                onTap: (){
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ChangePasswordPage(
                                          users: users,
                                          callbackFunction: callback,
                                        ),
                                      ));
                                },
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 10, bottom: 10),
                                  child: Row(children: <Widget>[
                                    //Padding(padding: EdgeInsets.only(left: 5, right: 5)),
                                    Icon(
                                      Icons.lock,
                                      size: 30,
                                      color: Theme.of(context).accentColor,
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(left: 5, right: 5)),
                                    Text(
                                      "Đổi mật khẩu",
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Theme.of(context).accentColor,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ]),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      border: Border.all(
                                        color: Theme.of(context).accentColor,
                                        width: 2,
                                      )),
                                ),
                              ),
                            ),
                        ),
                    )
                    : Column(
                        children: [
                          Padding(
                              padding: EdgeInsets.only(right: 10, left: 10),
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => MyPurchesePage(
                                          userId: users.id,
                                        ),
                                      ));
                                },
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 10, bottom: 10),
                                  child: Row(children: <Widget>[
                                    Icon(
                                      Icons.shopping_cart_rounded,
                                      size: 30,
                                      color: Theme.of(context).accentColor,
                                    ),
                                    Padding(
                                        padding:
                                            EdgeInsets.only(left: 5, right: 5)),
                                    Text(
                                      "Đơn hàng của tôi",
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Theme.of(context).accentColor,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ]),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      border: Border.all(
                                        color: Theme.of(context).accentColor,
                                        width: 2,
                                      )),
                                ),
                              )),
                          Padding(padding: EdgeInsets.only(top: 15, bottom: 15)),
                          Padding(
                            padding: EdgeInsets.only(right: 10, left: 10),
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => MyInformation(
                                        users: users,
                                        callbackFunction: callback,
                                      ),
                                    ));
                              },
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, top: 10, bottom: 10),
                                child: Row(children: <Widget>[
                                  //Padding(padding: EdgeInsets.only(left: 5, right: 5)),
                                  Icon(
                                    Icons.info_rounded,
                                    size: 30,
                                    color: Theme.of(context).accentColor,
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(left: 5, right: 5)),
                                  Text(
                                    "Thông tin cá nhân",
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Theme.of(context).accentColor,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ]),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(
                                      color: Theme.of(context).accentColor,
                                      width: 2,
                                    )),
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 15, bottom: 15)),
                          Padding(
                              padding: EdgeInsets.only(right: 10, left: 10),
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => FullFengShui()));
                                },
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 10, bottom: 10),
                                  child: Row(children: <Widget>[
                                    Icon(
                                      Icons.brightness_4_rounded,
                                      size: 30,
                                      color: Theme.of(context).accentColor,
                                    ),
                                    Padding(
                                        padding:
                                            EdgeInsets.only(left: 5, right: 5)),
                                    Text(
                                      "Phong thủy",
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Theme.of(context).accentColor,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ]),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      border: Border.all(
                                        color: Theme.of(context).accentColor,
                                        width: 2,
                                      )),
                                ),
                              )),
                          Padding(padding: EdgeInsets.only(top: 15, bottom: 15)),
                          Padding(
                            padding: EdgeInsets.only(right: 10, left: 10),
                            child: InkWell(
                              onTap: (){
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ChangePasswordPage(
                                        users: users,
                                        callbackFunction: callback,
                                      ),
                                    ));
                              },
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, top: 10, bottom: 10),
                                child: Row(children: <Widget>[
                                  //Padding(padding: EdgeInsets.only(left: 5, right: 5)),
                                  Icon(
                                    Icons.lock,
                                    size: 30,
                                    color: Theme.of(context).accentColor,
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(left: 5, right: 5)),
                                  Text(
                                    "Đổi mật khẩu",
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Theme.of(context).accentColor,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ]),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(
                                      color: Theme.of(context).accentColor,
                                      width: 2,
                                    )),
                              ),
                            ),
                          ),
                        ],
                      ),
                Padding(padding: EdgeInsets.only(top: 40, bottom: 40)),
                InkWell(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (context) => Dialog(
                                child: Container(
                                    height: 190,
                                    width: 100,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.only(bottom: 16),
                                            child: Text(
                                              "Đăng xuất!",
                                              style: TextStyle(fontSize: 24),
                                            )),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 16),
                                          child: Column(
                                            children: [
                                              Text(
                                                "Bạn có muốn đăng xuất",
                                                style: TextStyle(fontSize: 16),
                                              ),
                                              Text(
                                                "khỏi hệ thống?",
                                                style: TextStyle(fontSize: 16),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 16),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              InkWell(
                                                onTap: () {
                                                  //exitDialog();
                                                  Navigator.pop(context);
                                                },
                                                child: Container(
                                                  width: 96,
                                                  height: 40,
                                                  //padding: EdgeInsets.only(top:0),
                                                  child: Center(
                                                    child: Text("Hủy",
                                                        style: TextStyle(
                                                            color: Colors.red,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight.w500)),
                                                  ),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color: Colors.red,
                                                          width: 1.5),
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(5)),
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () async{
                                                  dynamic result = await _auth.signOut();
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SigninPage(),
                                                      ));
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(left: 16),
                                                  width: 96,
                                                  height: 40,
                                                  //padding: EdgeInsets.only(top:0),
                                                  child: Center(
                                                    child: Text("Xác nhận",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight.w500)),
                                                  ),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color: Theme.of(context)
                                                              .accentColor,
                                                          width: 1.5),
                                                      color: Theme.of(context)
                                                          .accentColor,
                                                      borderRadius:
                                                          BorderRadius.circular(5)),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    )),
                              ));
                    },
                    child: Padding(
                      padding: EdgeInsets.only(left: 10,bottom: 16),
                      child: Row(children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(5),
                          child: Icon(
                            Icons.logout,
                            size: 30,
                            color: Colors.white,
                          ),
                          decoration: BoxDecoration(
                              color: Theme.of(context).accentColor,
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        Padding(padding: EdgeInsets.only(left: 10)),
                        Text(
                          "Đăng xuất",
                          style: TextStyle(
                              fontSize: 18,
                              color: Theme.of(context).accentColor,
                              fontWeight: FontWeight.w500),
                        )
                      ]),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future _pickImage(int a) async {
    final _storage = FirebaseStorage.instance;
    final _picker = ImagePicker();
    PickedFile image;
    if (a == 1) {
      image = await _picker.getImage(source: ImageSource.camera);
      if (image != null) {
        var file = File(image.path);
        String url = widget.users;
        if (image != null) {
          //Upload to Firebase
          var snapshot = await _storage.ref().child('AVT/$url').putFile(file);

          var downloadUrl = await snapshot.ref.getDownloadURL();
          setState(() {
            defaulAVT = downloadUrl;
            UserService().updateAVT(widget.users, defaulAVT, users.photoUrl);
          });
          //print(_tempAVT);
        } else {
          print('No Path Received');
        }
      }
    } else {
      image = await _picker.getImage(source: ImageSource.gallery);
      if (image != null) {
        var file = File(image.path);
        String url = widget.users;
        if (image != null) {
          //Upload to Firebase
          var snapshot = await _storage.ref().child('AVT/$url').putFile(file);

          var downloadUrl = await snapshot.ref.getDownloadURL();
          setState(() {
            defaulAVT = downloadUrl;
            UserService().updateAVT(widget.users, defaulAVT, users.photoUrl);
          });
          //print(_tempAVT);
        } else {
          print('No Path Received');
        }
      }
    }
  }
}
