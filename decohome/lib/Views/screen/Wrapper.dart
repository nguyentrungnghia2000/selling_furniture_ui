import 'package:DecoHome/Views/models/account.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/screen/AddBirthday.dart';
import 'package:DecoHome/Views/screen/MainPage.dart';
import 'package:DecoHome/Views/screen/OnboardPage.dart';
import 'package:DecoHome/Views/screen/AdminMainPage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  Users users = null;
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Account>(context);
    if (user != null&&user.uid!='2LKx0QrDJIO5tziT8OIvQFaAFep2') {
      //print("User");
       return MainPage(
         userInfo: user.uid,
       );
    }
    else if(user!=null && user.uid=='2LKx0QrDJIO5tziT8OIvQFaAFep2'){
      //print("Admin");
      return AdminMainPage(
        userInfo:user.uid,
      );
    } else {
      return OnBoardPage();
    }
  }
}
