import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/screen/AddBirthday.dart';
import 'package:DecoHome/Views/screen/MyFengShui.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:DecoHome/Views/widget/ProductListPage.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/screen/CartPage.dart';
import 'package:provider/provider.dart';
//import 'package:google_nav_bar/google_nav_bar.dart';

class HomePage extends StatefulWidget {
  final String userId;
  HomePage({this.userId});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Users users;

  callback() {
    setState(() {
      //print("Come here");
    });
  }

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    users = await UserService().userInfo(widget.userId);
    if (this.mounted) {
      setState(() {
        print(users.element);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return StreamProvider<List<Product>>.value(
      value: ProductService().getAllProd,
      child: Scaffold(
          body: SafeArea(
              child: Container(
                  child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 470,
                  width: screenWidth,
                  child: Image(
                    image: AssetImage('Poster_1.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  height: 50,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Trang chủ",
                          style: TextStyle(
                              fontSize: 22, fontWeight: FontWeight.bold),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => CartPage(
                                    userId: widget.userId,
                                  ),
                                ));
                          },
                          child: Icon(
                            Icons.shopping_cart_rounded,
                            color: Colors.black,
                            size: 28,
                          ),
                        )
                      ]),
                ),
              ],
            ),
            Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        "Đề xuất phong thủy",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 18),
                      ),
                    ),
                    InkWell(
                        onTap: () {
                          if (users.element == "") {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddBirthday(
                                          userid: users.id,
                                        )));
                          } else {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FengShui(
                                          menh: users.element,
                                          userid: users.id,
                                        )));
                          }
                        },
                        child: Text("Gợi ý",
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Theme.of(context).accentColor)))
                  ],
                )),
            Container(
                height: 250,
                padding: EdgeInsets.only(left: 10),
                child: ProductList(
                  userId: widget.userId,
                  callbackFunction: callback,
                )),
          ],
        ),
      )))),
    );
  }
}
