import 'package:DecoHome/Views/models/account.model.dart';
import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyInformation extends StatefulWidget {
  String id;
  final Users users;
  final Function callbackFunction;
  MyInformation({this.users, this.callbackFunction});
  @override
  State<MyInformation> createState() {
    return _MyInformation();
  }
}

class _MyInformation extends State<MyInformation> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _yearController = TextEditingController();
  TextEditingController _nationalityController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    //print(users.id);
    return Scaffold(
        body: SafeArea(
            child: ListView(children: <Widget>[
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, right: 10),
            child: Row(children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back_ios_rounded,
                  size: 30,
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 5)),
              Text(
                "Thông tin cá nhân",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              )
            ]),
          ),
          Container(
            margin: EdgeInsets.only(top: 30),
            padding: EdgeInsets.only(left: 23, right: 23),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Tên",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 300,
                            child: TextField(
                              controller: _nameController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: widget.users.name,
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Địa chỉ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 300,
                            //padding: EdgeInsets.only(left:16,right:16),
                            child: TextField(
                              controller: _addressController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: widget.users.address,
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Email",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 300,
                            padding: EdgeInsets.only(bottom: 16, top: 16),
                            child: Text(widget.users.email,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16)),
                          )
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Số điện thoại",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 300,
                            //padding: EdgeInsets.only(left:16,right:16),
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: _phoneController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: widget.users.phoneNum,
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Năm sinh",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 300,
                            //padding: EdgeInsets.only(left:16,right:16),
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: _yearController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: widget.users.birthday,
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Quốc tịch",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 300,
                            child: TextField(
                              controller: _nationalityController,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: widget.users.nationality,
                                  hintStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      )),
                ),
                InkWell(
                  onTap: () {
                    String element = "";
                    if (_yearController.text != "") {
                      element = UserService().menhNguHanh(
                          UserService()
                              .getDiachi(int.parse(_yearController.text)),
                          UserService()
                              .getThienCan(int.parse(_yearController.text)));
                    }
                    UserService().updateUserInfo(
                        widget.users.id,
                        _nameController.text,
                        _emailController.text,
                        _phoneController.text,
                        _addressController.text,
                        _nationalityController.text,
                        _yearController.text,
                        element);
                    showDialog(
                        context: context,
                        builder: (context) => Dialog(
                              child: Container(
                                  height: 190,
                                  width: 100,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(bottom: 16),
                                          child: Text(
                                            "Cập nhật thông tin!",
                                            style: TextStyle(fontSize: 24),
                                          )),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 16),
                                        child: Column(
                                          children: [
                                            Text(
                                              "Cập nhật thông tin cá nhân",
                                              style: TextStyle(fontSize: 16),
                                            ),
                                            Text(
                                              "thành công!",
                                              style: TextStyle(fontSize: 16),
                                            ),
                                          ],
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          Navigator.pop(context);
                                          if (widget.callbackFunction != null) {
                                            widget.callbackFunction();
                                          }
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(bottom: 16),
                                          width: 120,
                                          height: 40,
                                          //padding: EdgeInsets.only(top:0),
                                          child: Center(
                                            child: Text("Hoàn thành",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w500)),
                                          ),
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                  width: 1.5),
                                              color:
                                                  Theme.of(context).accentColor,
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                        ),
                                      ),
                                    ],
                                  )),
                            ));
                    //print(_nationalityController.text);
                  },
                  child: Container(
                    padding: EdgeInsets.only(top: 16, bottom: 16),
                    margin: EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Cập nhật thông tin",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    ])));
  }
}
