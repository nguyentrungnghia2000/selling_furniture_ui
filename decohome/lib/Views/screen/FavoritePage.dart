import 'package:DecoHome/Views/models/favorite.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/screen/CartPage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:DecoHome/Views/widget/FavoListPage.dart';
//import 'package:google_nav_bar/google_nav_bar.dart';

class FavoritePage extends StatefulWidget {
  String userId;
  FavoritePage({this.userId});
  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<FavoritePage> {
  List<Favorite> favoList=[];
  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<Product>>.value(
      value: ProductService().getAllProd,
      child: SafeArea(
          child: SingleChildScrollView(
            child: Container(
                  padding: EdgeInsets.only(left: 10, top: 10, right: 10),
                  child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(children: <Widget>[
                Expanded(
                    child: Text(
                  "Yêu thích",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                )),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CartPage(userId: widget.userId,),
                        ));
                  },
                  child: Icon(
                    Icons.shopping_cart_rounded,
                    color: Colors.black,
                    size: 28,
                  ),
                )
              ]),
              Container(
                margin: EdgeInsets.only(top:30,bottom: 16),
                height: 690,
                //padding: EdgeInsets.only(left: 10), 
                child: FavotListPro(userId: widget.userId,)),
            ],
                  ),
                ),
          )),
    );
  }
}
