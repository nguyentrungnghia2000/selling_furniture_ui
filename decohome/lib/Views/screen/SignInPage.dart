import 'package:DecoHome/Views/models/user.model.dart';
import 'package:DecoHome/Views/screen/AddBirthday.dart';
import 'package:DecoHome/Views/screen/AdminMainPage.dart';
import 'package:DecoHome/Views/services/sign.service.dart';
import 'package:DecoHome/Views/services/user.service.dart';
import 'package:flutter/material.dart';
import 'package:DecoHome/Views/screen/ForgotPasswordPage.dart';
import 'package:DecoHome/Views/screen/SignUpPage.dart';
import 'package:DecoHome/Views/screen/MainPage.dart';

class SigninPage extends StatefulWidget {
  @override
  State<SigninPage> createState() {
    return _SignInPage();
  }
}

class _SignInPage extends State<SigninPage> {
  String email = '';
  String password = '';
  String error = '';
  final SignService _auth = SignService();
  final _formKey = GlobalKey<FormState>();
  List<Users> listUser;
  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    listUser = await UserService().getUserList();
  }

  Widget build(BuildContext context) {
    //print(listUser);
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        body: Center(
            child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 50),
            ),
            Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 40),
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 10, left: 30),
                      child: FlatButton(
                        minWidth: 110,
                        height: 2,
                        color: Color.fromRGBO(6, 130, 130, 50),
                        child: Text(
                          '',
                          style: TextStyle(color: Colors.red),
                        ),
                        onPressed: () {},
                      ),
                    ),
                    Container(
                      decoration:
                          BoxDecoration(color: Color.fromARGB(0, 0, 0, 0)),
                      padding: EdgeInsets.only(),
                      child: Text(
                        'Xin chào',
                        style: TextStyle(
                            fontSize: 32, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.only(bottom: 100, left: 40),
              child: Text(
                'Đăng nhập vào tài khoản của bạn',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
            ),
            Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 10, bottom: 10, left: 40, right: 40),
                      child: TextFormField(
                        validator: (val) =>
                            val.isEmpty ? 'Hãy nhập email của bạn' : null,
                        onChanged: (val) {
                          setState(() {
                            this.email = val;
                          });
                        },
                        decoration: InputDecoration(
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.green)),
                            labelText: 'Email',
                            hintText: 'Nhập email của bạn',
                            hintStyle: TextStyle(fontSize: 13)),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 10, bottom: 10, left: 40, right: 40),
                      child: TextFormField(
                        validator: (val) => val.length < 6
                            ? 'Mật khẩu cần ít nhất 6 ký tự'
                            : null,
                        obscureText: true,
                        onChanged: (val) {
                          setState(() {
                            this.password = val;
                          });
                        },
                        decoration: InputDecoration(
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.green)),
                            labelText: 'Mật khẩu',
                            hintText: 'Nhập mật khẩu của bạn',
                            hintStyle: TextStyle(fontSize: 13)),
                      ),
                    ),
                  ],
                )),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ForgotPassword(),
                    ));
              },
              child: Container(
                  padding: EdgeInsets.only(bottom: 70, left: 210),
                  child: FlatButton(
                    child: Text(
                      'Quên mật khẩu',
                      style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.bold,
                          color: Color.fromRGBO(6, 130, 130, 50),
                          decoration: TextDecoration.underline),
                    ),
                    onPressed: () {
                      //print('forgot');
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ForgotPassword(),
                          ));
                    },
                  )),
            ),
            Container(
                child: Align(
              child: SizedBox(
                width: 240,
                child: FlatButton(
                  height: 60,
                  minWidth: 240,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  child: Text(
                    'Đăng nhập',
                    style: TextStyle(fontSize: 16),
                  ),
                  color: Color.fromRGBO(6, 130, 130, 50),
                  textColor: Colors.white,
                  onPressed: () async {
                    String id = UserService().getUserByEmail(listUser, email);
                    if (SignService().checkSign(listUser, email, password) ==
                            0 ||
                        SignService().checkSign(listUser, email, password) ==
                            1) {
                      showDialog(
                          context: context,
                          builder: (context) => Dialog(
                                child: Container(
                                  height: 170,
                                  width: 100,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(bottom: 16),
                                          child: Text(
                                            "Đăng nhập!",
                                            style: TextStyle(fontSize: 24),
                                          )),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 16),
                                        child: Column(
                                          children: [
                                            Text(
                                              "Tài khoản hoặc mật khẩu",
                                              style: TextStyle(fontSize: 16),
                                            ),
                                            Text(
                                              "không chính xác!",
                                              style: TextStyle(fontSize: 16),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 16),
                                        child: InkWell(
                                          onTap: () {
                                            Navigator.pop(context);
                                          },
                                          child: Container(
                                            //margin: EdgeInsets.only(left: 16),
                                            width: 140,
                                            height: 40,
                                            //padding: EdgeInsets.only(top:0),
                                            child: Center(
                                              child: Text("Đăng nhập lại",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w500)),
                                            ),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                    width: 1.5),
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ));
                    } else {
                      dynamic result = await _auth.SignIn(email, password);
                      showDialog(
                          context: context,
                          builder: (context) => Dialog(
                                child: Container(
                                  height: 150,
                                  width: 100,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(bottom: 16),
                                          child: Text(
                                            "Đăng nhập!",
                                            style: TextStyle(fontSize: 24),
                                          )),
                                      Container(
                                        //margin: EdgeInsets.only(bottom: 16),
                                        child: Column(
                                          children: [
                                            Text(
                                              "Đăng nhập thành công!",
                                              style: TextStyle(fontSize: 16),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            bottom: 16, top: 16),
                                        child: InkWell(
                                          onTap: () async{
                                            if (id !=
                                                '2LKx0QrDJIO5tziT8OIvQFaAFep2') {
                                              String element = await UserService()
                                                  .getElement(id);
                                              if (element == '') {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            AddBirthday(userid: id,)));
                                              } else {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                        MainPage(userInfo: id,)));
                                              }
                                            } else {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          AdminMainPage(
                                                            userInfo: id,
                                                          )));
                                            }
                                          },
                                          child: Container(
                                            //margin: EdgeInsets.only(left: 16),
                                            width: 96,
                                            height: 40,
                                            //padding: EdgeInsets.only(top:0),
                                            child: Center(
                                              child: Text("Xác nhận",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w500)),
                                            ),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                    width: 1.5),
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ));
                    }
                  },
                ),
              ),
            )),
            Container(
                child: Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 100),
                  child: Text(
                    'Chưa có tài khoản?',
                    style: TextStyle(fontSize: 13),
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(left: 5),
                    child: FlatButton(
                      padding: EdgeInsets.only(right: 45),
                      child: Text(
                        'Đăng ký',
                        style: TextStyle(
                            fontSize: 13,
                            decoration: TextDecoration.underline,
                            color: Color.fromRGBO(6, 130, 130, 1)),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SignUp(),
                            ));
                      },
                    ))
              ],
            )),
          ],
        )),
      ),
    );
  }
}
