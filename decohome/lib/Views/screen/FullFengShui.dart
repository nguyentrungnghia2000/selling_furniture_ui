import 'package:DecoHome/Views/screen/NewspaperPage.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';

class FullFengShui extends StatefulWidget {
  @override
  _FullFengShuiState createState() => _FullFengShuiState();
}

class _FullFengShuiState extends State<FullFengShui> {
  Image noImage = Image.asset("assets/Ghe_1.png");

  @override
  Widget build(BuildContext context) {
    //print(string.length);
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
          child: Container(
        padding: EdgeInsets.only(left: 16, right: 16, top: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(top: 10, right: 10),
              child: Row(children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios_rounded,
                    size: 30,
                  ),
                )
              ]),
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Những kiến thức cơ bản khi mua đồ nội thất theo mệnh:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).accentColor),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Đối với nhiều gia đình Việt Nam phong thủy trong việc lựa chọn đồ nội thất cho chính căn nhà của mình là một vấn đề được nhiều người quan tâm đến bởi đối với họ điều này quyết định tới vận mệnh của cả gia đình. Tại đây chúng tôi sẽ cung cấp cho bạn một vài kiến thức cơ bản khi lựa chọn đồ nội thất theo mệnh.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Mua đồ nội thất theo mệnh Ngũ Hành:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).accentColor),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Khi mua đồ nội thất theo mệnh Ngũ Hành đầu tiên chúng ta cần có kiến thức về Ngũ Hành gồm những mệnh gì, đặc biệt là mối quan hệ tương sinh, tương khắc giữa các mệnh với nhau để có thể lựa chọn những đồ nội thất có màu sắc, chất liệu bổ trợ nhau phù hợp với mệnh của bản thân và gia đình, tránh sử dụng những màu sắc vật liệu tương khắc gây hại cho việc bố trí cũng như không phù hợp với phong thủy của gia đình.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Container(
              child: SvgPicture.asset(
                'assets/NguHanh.svg',
                height: 400,
                width: 400,
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 16)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Tương sinh",
                    style: TextStyle(fontWeight: FontWeight.w500,fontSize: 16)),
                Padding(padding: EdgeInsets.only(right: 8)),
                Container(
                  child: SvgPicture.asset(
                    'assets/TuongSinh.svg',
                    height: 20,
                    width: 20,
                  ),
                ),
                Padding(padding: EdgeInsets.only(left: 8, right: 8)),
                Text(
                  "Tương khắc",
                  style: TextStyle(fontWeight: FontWeight.w500,fontSize: 16),
                ),
                Padding(padding: EdgeInsets.only(right: 8)),
                Container(
                  child: SvgPicture.asset(
                    'assets/TuongKhac.svg',
                    height: 20,
                    width: 20,
                  ),
                ),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 16)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Mua đồ nội thất cho người mệnh Hỏa:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).accentColor),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Đối với người mệnh Hỏa, bạn nên sử dụng những nội thất có màu sắc như Đỏ, Cam, Hồng, Tím. Ngoài ra, theo quy tắc cây cối sinh lửa những mà sắc thuộc hành Mộc như Xanh lá cây, màu Gỗ cũng rất phù hợp với những người mệnh Hỏa như bạn.\nTuy nhiên, khi sử dụng quá nhiều màu nóng sẽ làm cho căn phòng của bạn trở nên bức bối, nóng nực. Vì vậy, bạn chỉ nên sử dụng những nội thất có màu sắc rực rỡ chỉ để là điểm nhấn, ngoài ra nên sử dụng thêm nhiều cây xanh để trang trí và sử dụng đồ nội thất màu gỗ để trung hòa lại không gian.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Image.network(
              "https://firebasestorage.googleapis.com/v0/b/app-demo-e7cf0.appspot.com/o/huy-nguyen-KlZ33F_Y3aI-unsplash.jpg?alt=media&token=89b5e8ab-1924-4501-8d63-2e5b6437b9a2",
              loadingBuilder: (context, child, loadingProgress) =>
                  (loadingProgress == null)
                      ? child
                      : CircularProgressIndicator(),
              errorBuilder: (context, error, stackTrace) => noImage,
              fit: BoxFit.fitWidth,
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Khi sử dụng màu sắc khác, bạn nên tránh những đồ nội thất sử dụng màu chủ đạo là Xanh nước biển hoặc màu Đen, đây là những màu tượng trưng của mệnh Thủy, là màu tương khắc với với những người mệnh Hỏa như bạn.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 16)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Mua đồ nội thất cho người mệnh Kim:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).accentColor),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Khi lựa chọn đồ nội thất cho những người mệnh Kim nên lấy màu trắng làm màu chủ đạo để phù hợp với bản mệnh của bản thân. Ngoài ra, màu Vàng cũng là màu sắc được coi là màu sắc mang lại nhiều may mắn và tài lộc cho người mệnh Kim vì thế hay khéo léo những màu sắc trên với nhau để tạo cho căn phòng của bản thân có nhiều điểm nhấn hơn.\nĐặc biệt, căn phòng của người có mệnh Kim nên sử dụng những đồ nội thất được làm bằng kim loại như Vàng, Đồng, Inox,... hoặc những đồ bằng Thủy tinh, Gốm sứ.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Image.network(
              "https://firebasestorage.googleapis.com/v0/b/app-demo-e7cf0.appspot.com/o/jonathan-borba-JXGBlAv0kEQ-unsplash.jpg?alt=media&token=be6ea2e9-0d53-479e-9055-cdeaa0bcfb20",
              loadingBuilder: (context, child, loadingProgress) =>
                  (loadingProgress == null)
                      ? child
                      : CircularProgressIndicator(),
              errorBuilder: (context, error, stackTrace) => noImage,
              fit: BoxFit.fitWidth,
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Khác với những người có mệnh Hỏa, những người có mệnh Kim nên sử dụng những chậu hoa để trang trí thay vì sử dụng cây cảnh. Một số loại hoa mà bạn có thể cân nhắc như hoa Ngọc Lan, hoa Kim Ngân,...",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 16)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Mua đồ nội thất cho người mệnh Mộc:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).accentColor),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Đối với những người mệnh Mộc, nên lựa chọn những loại đồ dùng nội thất từ những chất liệu tự nhiên như gỗ, tre cho bàn ghế, kệ tủ. Những đồ nội thất được làm bằng vậy liệu tự nhiên sẽ làm cho căn nhà trở nên sáng sủa, tốt cho sự phát triển và cuộc sống của người mệnh mộc\nTheo quan hệ tương sinh của ngũ hành thì Thủy duy trì sự sống của Mộc. Vì thế khi sử dụng trang trí, người chủ nhà nên chú ý đến việc tạo thủy khí trong không gian. Ngoài những vật trang trí, đồ nội thất màu Xanh lá cây, màu Gỗ hoặc những cây cảnh, hoa chủ nhà có thể suy nghĩ đến việc làm lỗi đi uốn lượn như nước hoặc trang trí nên bể cá, hồ thủy sinh.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Image.network(
              "https://firebasestorage.googleapis.com/v0/b/app-demo-e7cf0.appspot.com/o/mathias-adam-JKHUw0Xujf8-unsplash.jpg?alt=media&token=3bb4195b-e838-48c7-bd31-54791c5b4819",
              loadingBuilder: (context, child, loadingProgress) =>
                  (loadingProgress == null)
                      ? child
                      : CircularProgressIndicator(),
              errorBuilder: (context, error, stackTrace) => noImage,
              fit: BoxFit.fitWidth,
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Ngược lại với những yếu tố trên, nếu sử dụng quá ít những đồ nội thất hợp mệnh sẽ làm cho bạn cảm thấy mệt mỏi, trì trệ trong công việc và giảm đi sự phát triển của bản thân.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 16)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Mua đồ nội thất cho người mệnh Thổ:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).accentColor),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Đa số những người có mệnh Thổ thường có tính cách giản dị, ôn hòa nên việc thiết kế nhà cửa cần toát lên vẻ đẹp thanh thoát, bình dị nhưng vẫn không thiếu đi phần thanh lịch cho căn nhà. Màu sắc phù hợp với những người mệnh Thổ là màu Vàng, Nâu Vàng, Nâu Đất.\nNgoài ra, chủ nhà cũng có thể sử dụng những đồ trang trí có màu trắng màu của những người mệnh Kim theo quy tắc tương sinh Thổ sinh Kim",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Image.network(
              "https://firebasestorage.googleapis.com/v0/b/app-demo-e7cf0.appspot.com/o/francesca-tosolini-ykHFS4Ul64U-unsplash.jpg?alt=media&token=3a3a75d3-b6ed-4975-8d98-0507171a8b87",
              loadingBuilder: (context, child, loadingProgress) =>
                  (loadingProgress == null)
                      ? child
                      : CircularProgressIndicator(),
              errorBuilder: (context, error, stackTrace) => noImage,
              fit: BoxFit.fitWidth,
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Mặc dù Mộc khắc Thổ, nhưng khi trang trí nhà bằng những đồ nội thất được làm bằng vật liệu thiên nhiên như tre, gỗ,... cho căn nhà của người hệ Thổ sẽ làm cho căn nhà cảm giác mộc mạc, có sức sống hơn so với chỉ sử dụng những màu sắc phù hợp với mệnh Thổ và Kim",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 16)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Mua đồ nội thất cho người mệnh Thủy:",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).accentColor),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Hai màu sắc cơ bản mà bạn cần phải nhớ khi trang trí nhà co người mang mệnh Thủy là màu Xanh nước biển và màu Đen. Trong khi màu Xanh nước biển mang trong mình tính cách hiền hòa, tinh tế thì màu Đen mang đến cho người nhin sự huyền bí và đây là kết hợp hoàn hảo khi trang trí căn nhà của bạn.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Image.network(
              "https://firebasestorage.googleapis.com/v0/b/app-demo-e7cf0.appspot.com/o/adam-winger-t4oVP2xFMJ8-unsplash.jpg?alt=media&token=2e0ec22f-6dcd-4ea9-9a5a-b5e9004e2ef3",
              loadingBuilder: (context, child, loadingProgress) =>
                  (loadingProgress == null)
                      ? child
                      : CircularProgressIndicator(),
              errorBuilder: (context, error, stackTrace) => noImage,
              fit: BoxFit.fitWidth,
            ),
            Padding(padding: EdgeInsets.only(bottom: 8)),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Tuyệt đối nên tránh những màu của mệnh Thổ như Vàng, Nâu hoặc những đồ được làm từ đất như đồ gốm sứ khi sử dụng lâu đài sẽ không tốt cho sự phát triển của mệnh Thủy. Và nên sử dụng đồ nội thất có hình tròn, trụ tránh dùng những đồ có cạnh sắc nhọt sẽ không phù hợp với phong thủy.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 16)),
          ],
        ),
      )),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(16),
        child: InkWell(
          onTap: () {
             Navigator.push(
                 context,
                 MaterialPageRoute(
                 builder: (context) => NewspaperPage()));
          },
          child: Container(
            padding: EdgeInsets.only(top: 16, bottom: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Tìm hiểu thêm",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
              ],
            ),
            decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.circular(10)),
          ),
        ),
      ),
    ));
  }

}
