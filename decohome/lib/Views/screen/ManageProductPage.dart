import 'package:DecoHome/Views/models/favorite.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:DecoHome/Views/widget/ManageProductList.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:DecoHome/Views/screen/AddNewProductPage.dart';

//import 'package:google_nav_bar/google_nav_bar.dart';

class ManageProductPage extends StatefulWidget {
  String userId;

  ManageProductPage({
    this.userId,
  });
  @override
  _ManageProductState createState() => _ManageProductState();
}

class _ManageProductState extends State<ManageProductPage> {
  List<Favorite> favoList = [];

  @override
  void initState() {
    super.initState();
    loadInitData();
  }

  loadInitData() async {
    //widget.userId=widget.userId;
        if (this.mounted) {
      setState(() {
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<Product>>.value(
      value: ProductService().getAllProd,
      child: SingleChildScrollView(
        child: SafeArea(
            child: Container(
          padding: EdgeInsets.only(left: 10, top: 10, right: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(children: <Widget>[
                Expanded(
                    child: Text(
                  "Quản lý sản phẩm",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                )),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddNewProductPage(userid: widget.userId,)),
                    );
                  },
                  child: Icon(
                    Icons.add_circle_outline,
                    color: Colors.black,
                    size: 28,
                  ),
                )
              ]),
              Container(
                  //padding: EdgeInsets.only(left: 10),
                  child: ManageProductList(
                    userId: widget.userId,
                  )),
              //Text("Đây là trang quản lý sản phẩm"),
            ],
          ),
        )),
      ),
    );
  }
}
