import 'package:DecoHome/Views/models/user.model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserService {
  final String uid;
  UserService({this.uid});
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('Users');
  bool _isExist = false;

  Future updateUser(
      String id,
      String name,
      String email,
      String password,
      String date,
      String phoneNum,
      String address,
      String nationality,
      String photoUrl,
      String birthday,
      String element,
      int typeAccount) async {
    return await userCollection.doc(uid).set({
      'id': id,
      'name': name,
      'email': email,
      'password': password,
      'date': date,
      'phoneNum': phoneNum,
      'address': address,
      'nationality': nationality,
      'photoUrl': photoUrl,
      'birthday':birthday,
      'element':element,
      'typeAccount': typeAccount
    });
  }

  Stream<QuerySnapshot> get getUsers {
    return userCollection.snapshots();
  }

  bool CheckIdExist(String uid) {
    userCollection.doc(uid).get().then((data) => {
          if (data == null) {setIsExist(false)} else setIsExist(true)
        });
    return _isExist;
  }

  Future updateAVT(String id, String pic,String check) async {
      print(pic);
      print(check);
      print(id);
    if (pic != '' && pic!= check) {
      await userCollection.doc(id).update({'photoUrl': pic});
    }
  }

  Future <List<Users>> countUser() async{
    List<Users> list=[];
    await userCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Users.formSnapShot(element));
      });
    });
    return list;
  }

  Future updateUserInfo(String id, String name, String email, String phoneNum,
      String address, String nationality, String birthday, String element) async {
    // update name
    if (name != '') {
      await userCollection.doc(id).update({'name': name});
    }
    // update email
    if (email != '') {
      await userCollection.doc(id).update({'email': email});
    }
    // update phone number
    if (phoneNum != '') {
      await userCollection.doc(id).update({'phoneNum': phoneNum});
    }
    // update address
    if (address != '') {
      await userCollection.doc(id).update({'address': address});
    }
    //update nationality
    if (nationality != '') {
      await userCollection.doc(id).update({'nationality': nationality});
    }
    if (birthday != '') {
      await userCollection.doc(id).update({'birthday': birthday});
    }
    if (element != '') {
      await userCollection.doc(id).update({'element': element});
    }
  }
  Future changePasswork(String id, String newPassword)async{   
    await userCollection.doc(id).update({'password': newPassword});
  }

  Future<Users> userInfo(String id) async {
    Users users = null;
    List<Users> list = [];
    await userCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Users.formSnapShot(element));
      });
    });
    for (int i = 0; i < list.length; i++) {
      if (list[i].id == id) {
        users = list[i];
        return users;
      }
    }
    return users;
  }

  Future<List<Users>> getUserList() async {
    List<Users> list = [];
    await userCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Users.formSnapShot(element));
      });
    });
    return list;
  }
  Future <String> getElement(String id)async{
    List<Users> list = [];
    await userCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Users.formSnapShot(element));
      });
    });
    for (int i=0;i<list.length;i++){
      if(list[i].id==id){
        return list[i].element;
      }
    }
  }

  String getUserByEmail(List<Users> listUsers, String email) {
    for (int i = 0; i < listUsers.length; i++) {
      if (listUsers[i].email == email) {
        return listUsers[i].id;
      }
    }
  }

  setIsExist(bool val) {
    this._isExist = val;
  }
  int getDiachi(int year) {
    int year2 = 0;
    int count = 0;
    while (year > 100) {
      if (count == 0) {
        year2 = year % 10;
        count++;
      } else {
        year2 = year2 + (year % 10) * 10;
      }
      year = year ~/ 10;
    }
    if (year2 % 12 == 0 ||
        year2 % 12 == 1 ||
        year2 % 12 == 6 ||
        year2 % 12 == 7) {
      return 0;
    } else if (year2 % 12 == 2 ||
        year2 % 12 == 3 ||
        year2 % 12 == 8 ||
        year2 % 12 == 9) {
      return 1;
    } else {
      return 2;
    }
  }

  int getThienCan(int year) {
    if (year % 10 == 4 || year % 10 == 5) {
      return 1;
    } else if (year % 10 == 6 || year % 10 == 7) {
      return 2;
    } else if (year % 10 == 8 || year % 10 == 9) {
      return 3;
    } else if (year % 10 == 0 || year % 10 == 1) {
      return 4;
    } else {
      return 5;
    }
  }

  String menhNguHanh(int Diachi, int Thiencan) {
    int menhNguHanh = Diachi + Thiencan;
    if (menhNguHanh > 5) {
      menhNguHanh = menhNguHanh - 5;
    }
    if (menhNguHanh == 1) {
      return "Kim";
    } else if (menhNguHanh == 2) {
      return "Thủy";
    } else if (menhNguHanh == 3) {
      return "Hỏa";
    } else if (menhNguHanh == 4) {
      return "Thổ";
    } else {
      return "Mộc";
    }
  }

}
