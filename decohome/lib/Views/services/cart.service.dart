import 'package:DecoHome/Views/models/cart.model.dart';
import 'package:DecoHome/Views/models/favorite.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CartService {
  final CollectionReference cartCollection =
      FirebaseFirestore.instance.collection('Cart');
  Future updateCart(
    String cartId,
    String userId,
    String proId,
    int quantityPro,
    int proPrice,
  ) async {
    return await cartCollection.doc(cartId).set({
      'cartId': cartId,
      'userId': userId,
      'proId': proId,
      'quantityPro': quantityPro,
      'proPrice': proPrice,
    });
  }

  Future<List<Cart>> cartList(String userid) async {
    List<Cart> list = [];
    List<Cart> list_temp = [];
    //print(userid);
    await cartCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Cart.fromSanpShot(element));
      });
    });
    for (int i = 0; i < list.length; i++) {
      //print(list[i].userId);
      //print(userid);
      if (list[i].userId == userid) {
        list_temp.add(list[i]);
      }
    }
    return list_temp;
  }

  Future<bool> isInCart(String userId, String proId) async {
    List<Cart> list = await cartList(userId);
    //print(proId);
    //print(list);
    for (int i = 0; i < list.length; i++) {
      //print(list[i].proId);
      if (list[i].proId == proId) {
        return true;
      }
    }
    return false;
  }

  Future<void> createCartItem(String userId, String proId, int proPrice) {
    String id = cartCollection.doc().id;
    cartCollection.doc(id).set({
      'cartId': id,
      'userId': userId,
      'proId': proId,
      'quantityPro': 1,
      'proPrice': proPrice
    });
    print(userId);
  }

  Future<void> deleteCartItem(String userId, String proId) async {
    //print(proId);
    List<Cart> list = [];
    await cartCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Cart.fromSanpShot(element));
      });
    });
    for (int i = 0; i < list.length; i++) {
      if (list[i].proId == proId) {
        //print(list[i].favoId);
        cartCollection.doc(list[i].cartId).delete();
      }
    }
  }

  Future<int> totalPrice(String userId) async {
    int totalPrice = 0;
    List<Cart> list = [];
    List<Cart> list_temp=[];
    await cartCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Cart.fromSanpShot(element));
      });
    });
    for(int i=0;i<list.length;i++){
      if(list[i].userId==userId){
        list_temp.add(list[i]);
      }
    }
    for(int j=0;j<list_temp.length;j++){
      totalPrice=totalPrice+list_temp[j].proPrice*list_temp[j].quantityPro;
    }
    return totalPrice;
  }
  Future subtractQuantityPro(String cartId,int a)async{
    //print(cartId);
    await cartCollection.doc(cartId).update({"quantityPro":a});
  }
  Future addQuantityPro(String cartId,int a)async{
    //print(cartId);
    await cartCollection.doc(cartId).update({"quantityPro":a});
  }
  Future<bool> isExist (String idProd)async{
    List<Cart> list = [];
    await cartCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Cart.fromSanpShot(element));
      });
    });
    for(int i=0;i<list.length;i++){
      if(list[i].proId==idProd){
        return true;
      }
    }
    return false;
  }
  Future<void> deletrProd(String idProd)async{
    List<Cart> list = [];
    await cartCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Cart.fromSanpShot(element));
      });
    });
    for(int i=0;i<list.length;i++){
      if(list[i].proId==idProd){
        cartCollection.doc(list[i].cartId).delete();
      }
    }
  }
}
