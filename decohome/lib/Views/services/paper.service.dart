import 'package:DecoHome/Views/models/paper.model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class PaperService {
  final CollectionReference paperCollection =
      FirebaseFirestore.instance.collection('Paper');
  Future updatePaper(
  String id,
  String Header,
  String Pic,
  String Title,
  String Link,
  ) async {
    return await paperCollection.doc(id).set({
      'id': id,
      'header': Header,
      'pic': Pic,
      'title': Title,
      'link': Link,
    });
  }
  Future<List<Paper>> paperList() async {
    List<Paper> list = [];
    //print(userid);
    await paperCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Paper.fromSanpShot(element));
      });
    });
    return list;
  }
}