import 'package:DecoHome/Views/models/favorite.model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FavoriteService {
  final CollectionReference favoCollection =
      FirebaseFirestore.instance.collection('Favorite');

  Future updateFavorite(
    String favoId,
    String userId,
    String proId,
  ) async {
    return await favoCollection.doc(favoId).set({
      'favoId': favoId,
      'userId': userId,
      'proId': proId,
    });
  }
  Future<List<Favorite>> favoList(String userid) async {
    List<Favorite> list = [];
    List<Favorite> list_temp=[];
    await favoCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Favorite.fromSanpShot(element));
      });
    });
    for (int i=0;i<list.length;i++)
    {
      if(list[i].userId==userid){
        list_temp.add(list[i]);
      }
    }
    return list_temp;
  }
  Future<bool> isFavo(String userId,String proId) async{
    List<Favorite> list=await favoList(userId);
    //print(list);
    for(int i=0;i<list.length;i++)
    {
      //print(list[i].proId);
      if(list[i].proId==proId){
        return true;
      }
    }
    return false;
  }
  Future<void> createFavoItem(String userId, String proId){
    String id= favoCollection.doc().id;
    favoCollection.doc(id).set({'favoId': id,'userId':userId,'proId':proId});
  }
  Future<void> deleteFavoItem(String userId,String proId)async{
    //print(proId);
    List<Favorite> list = [];
    await favoCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Favorite.fromSanpShot(element));
      });
    });
    for(int i=0;i<list.length;i++)
    {
      if(list[i].proId==proId){
        //print(list[i].favoId);
        favoCollection.doc(list[i].favoId).delete();
      }
    }
  }
}
