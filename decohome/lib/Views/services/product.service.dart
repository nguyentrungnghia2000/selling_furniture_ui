import 'dart:ffi';
import 'package:DecoHome/Views/models/cart.model.dart';
import 'package:DecoHome/Views/models/cart.model.dart';
import 'package:DecoHome/Views/models/favorite.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/favorite.service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ProductService {
  final CollectionReference prodCollection =
      FirebaseFirestore.instance.collection('Product');

  Future<List<Product>> prodList() async {
    List<Product> list = [];
    await prodCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Product.formSnapShot(element));
      });
    });
    return list;
  }

  Future<List<Product>> favoListProd(String userid) async {
    List<Product> list = [];
    List<Favorite> listFavo = await FavoriteService().favoList(userid);
    List<Product> list_temp = [];
    //print(listFavo);
    //var snap =  prodCollection.snapshots();
    await prodCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Product.formSnapShot(element));
      });
    });
    //print(list[0].prodid);
    for (int i = 0; i < list.length; i++) {
      for (int j = 0; j < listFavo.length; j++) {
        if (list[i].prodid == listFavo[j].proId) {
          list_temp.add(list[i]);
        }
      }
    }
    //print(list_temp);
    return list_temp;
  }

  Future<List<Product>> prodCateList(int a) async {
    List<Product> list = [];
    List<Product> list_temp = [];
    await prodCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Product.formSnapShot(element));
      });
    });
    list_temp = typeProd(list, a);
    return list_temp;
  }

  Stream<List<Product>> get getAllProd {
    //return prodCollection.snapshots().map(prodList);
    return null;
  }

  Product getPro(List<Product> list, String proId) {
    Product prod = null;
    for (int i = 0; i < list.length; i++) {
      if (list[i].prodid == proId) {
        prod = list[i];
      }
    }
    return prod;
  }

  Future<Product> getProbyID(String id) async {
    List<Product> list = [];
    await prodCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Product.formSnapShot(element));
      });
    });
    for (int i = 0; i < list.length; i++) {
      if (id == list[i].prodid) {
        return list[i];
      }
    }
  }

  Future updateProductInfo(
      String id,
      String name,
      String type,
      String price,
      String description,
      String amount,
      String picture,
      String color,
      String material) async {
    if (name != '') {
      await prodCollection.doc(id).update({'name': name});
    }
    if (type != '') {
      await prodCollection.doc(id).update({'typeid': type});
    }
    if (price != '') {
      await prodCollection.doc(id).update({'price': int.tryParse(price)});
    }
    if (description != '') {
      await prodCollection.doc(id).update({'prodInfo': description});
    }
    if (amount != '') {
      await prodCollection.doc(id).update({'amount': int.tryParse(amount)});
    }
    if (picture != '') {
      await prodCollection.doc(id).update({'pic': picture});
    }
    if (color != '') {
      await prodCollection.doc(id).update({'color': color});
    }
    if (material != '') {
      await prodCollection.doc(id).update({'material': material});
    }
  }

  Future<void> createNewProduct(
      String name,
      String type,
      String price,
      String description,
      String amount,
      String pic,
      String color,
      String material) {
    String id = prodCollection.doc().id;
    prodCollection.doc(id).set({
      'prodid': id,
      'name': name,
      'typeid': type,
      'price': int.tryParse(price),
      'prodInfo': description,
      'amount': int.tryParse(amount),
      'pic': pic,
      'color': color,
      'material': material
    });
  }

  Future<void> deleteProdct(String id) async {
    //print(id);
    await prodCollection.doc(id).delete();
  }

  List<Product> searchProd(String searchName, List<Product> prodList) {
    List<Product> temp = [];
    for (int i = 0; i < prodList.length; i++) {
      if (prodList[i].name.contains(searchName)) {
        print(prodList[i].name);
        temp.add(prodList[i]);
      }
    }
    return temp;
  }

  Future<List<Product>> prodCateElementList(String element, int a) async {
    List<Product> list = [];
    List<Product> listElement = [];
    //var snap =  prodCollection.snapshots();
    await prodCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Product.formSnapShot(element));
      });
    });
    if (element == "Hỏa") {
      for (int i = 0; i < list.length; i++) {
        if (list[i].color == "Đỏ" ||
            list[i].color == "Cam" ||
            list[i].color == "Tím" ||
            list[i].color == "Hồng" ||
            list[i].color == "Xanh lá cây" ||
            list[i].material == "Tự nhiên" ||
            list[i].material == "Gỗ") {
          listElement.add(list[i]);
        }
      }
    } else if (element == "Kim") {
      for (int i = 0; i < list.length; i++) {
        if (list[i].color == "Trắng" ||
            list[i].color == "Vàng" ||
            list[i].color == "Xám" ||
            list[i].color == "Nâu" ||
            list[i].material == "Gốm sứ" ||
            list[i].material == "Kim loại") {
          listElement.add(list[i]);
        }
      }
    } else if (element == "Mộc") {
      for (int i = 0; i < list.length; i++) {
        if (list[i].color == "Xanh lá cây" ||
            list[i].color == "Xanh nước biển" ||
            list[i].color == "Đen" ||
            list[i].material == "Gỗ" ||
            list[i].material == "Tự nhiên" ||
            list[i].material == "Thủy tinh") {
          listElement.add(list[i]);
        }
      }
    } else if (element == "Thổ") {
      for (int i = 0; i < list.length; i++) {
        if (list[i].color == "Vàng" ||
            list[i].color == "Nâu" ||
            list[i].color == "Đỏ" ||
            list[i].color == "Hồng" ||
            list[i].color == "Tím" ||
            list[i].color == "Cam" ||
            list[i].material == "Gốm sứ") {
          listElement.add(list[i]);
        }
      }
    } else {
      for (int i = 0; i < list.length; i++) {
        if (list[i].color == "Xanh nước biển" ||
            list[i].color == "Đen" ||
            list[i].color == "Trắng" ||
            list[i].color == "Xám" ||
            list[i].material == "Thủy tinh") {
          listElement.add(list[i]);
        }
      }
    }
    //print(list[2].price);
    listElement = typeProd(listElement, a);
    return listElement;
  }

  List<Product> typeProd(List<Product> list, int type) {
    List<Product> list_temp = [];
    switch (type) {
      case 0:
        for (int i = 0; i < list.length; i++) {
          list_temp.add(list[i]);
        }
        break;
      case 1:
        for (int i = 0; i < list.length; i++) {
          if (list[i].prodCategory == '1') {
            list_temp.add(list[i]);
          }
        }
        break;
      case 2:
        for (int i = 0; i < list.length; i++) {
          if (list[i].prodCategory == '2') {
            list_temp.add(list[i]);
          }
        }
        break;
      case 3:
        for (int i = 0; i < list.length; i++) {
          if (list[i].prodCategory == '3') {
            list_temp.add(list[i]);
          }
        }
        break;
      case 4:
        for (int i = 0; i < list.length; i++) {
          if (list[i].prodCategory == '4') {
            list_temp.add(list[i]);
          }
        }
        break;
      case 5:
        for (int i = 0; i < list.length; i++) {
          if (list[i].prodCategory == '5') {
            list_temp.add(list[i]);
          }
        }
        break;
      case 6:
        for (int i = 0; i < list.length; i++) {
          if (list[i].prodCategory == '6') {
            list_temp.add(list[i]);
          }
        }
        break;
      case 7:
        for (int i = 0; i < list.length; i++) {
          if (list[i].prodCategory == '7') {
            list_temp.add(list[i]);
          }
        }
        break;
    }
    return list_temp;
  }

  String getElementByColor(String color) {
    if (color == "Đỏ" || color == "Cam" || color == "Tím" || color == "Hồng") {
      return "Hỏa";
    } else if (color == "Trắng" || color == "Xám") {
      return "Kim";
    } else if (color == "Xanh lá cây") {
      return "Mộc";
    } else if (color == "Vàng" || color == "Nâu") {
      return "Thổ";
    } else if (color == "Xanh nước biển" || color == "Đen") {
      return "Thủy";
    } else {
      return "";
    }
  }

  String getElementByMaterail(String material) {
    // List _listMaterial = ["Nhựa","Da","Khác"];
    if (material == "Gỗ" || material == "Tự nhiên") {
      return "Mộc";
    } else if (material == "Kim loại") {
      return "Kim";
    } else if (material == "Gốm sứ") {
      return "Thổ";
    } else if (material == "Thủy tinh") {
      return "Thủy";
    } else {
      return "";
    }
  }

  Future<List<Product>> prodListElement(String element) async {
    List<Product> list = [];
    List<Product> listElement = [];
    //var snap =  prodCollection.snapshots();
    await prodCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Product.formSnapShot(element));
      });
    });
    if (element == "Hỏa") {
      for (int i = 0; i < list.length; i++) {
        if (list[i].color == "Đỏ" ||
            list[i].color == "Cam" ||
            list[i].color == "Tím" ||
            list[i].color == "Hồng" ||
            list[i].color == "Xanh lá cây") {
          listElement.add(list[i]);
        }
      }
    } else if (element == "Kim") {
      for (int i = 0; i < list.length; i++) {
        if (list[i].color == "Trắng" ||
            list[i].color == "Vàng" ||
            list[i].color == "Xám" ||
            list[i].color == "Nâu") {
          listElement.add(list[i]);
        }
      }
    } else if (element == "Mộc") {
      for (int i = 0; i < list.length; i++) {
        if (list[i].color == "Xanh lá cây" ||
            list[i].color == "Xanh nước biển" ||
            list[i].color == "Đen") {
          listElement.add(list[i]);
        }
      }
    } else if (element == "Thổ") {
      for (int i = 0; i < list.length; i++) {
        if (list[i].color == "Vàng" ||
            list[i].color == "Nâu" ||
            list[i].color == "Đỏ" ||
            list[i].color == "Hồng" ||
            list[i].color == "Tím" ||
            list[i].color == "Cam") {
          listElement.add(list[i]);
        }
      }
    } else {
      for (int i = 0; i < list.length; i++) {
        if (list[i].color == "Xanh nước biển" ||
            list[i].color == "Đen" ||
            list[i].color == "Trắng" ||
            list[i].color == "Xám") {
          listElement.add(list[i]);
        }
      }
    }
    //print(list[2].price);
    return listElement;
  }
}
