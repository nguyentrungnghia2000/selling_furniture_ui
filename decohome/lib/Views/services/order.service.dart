import 'dart:ffi';

import 'package:DecoHome/Views/models/cart.model.dart';
import 'package:DecoHome/Views/models/order.model.dart';
import 'package:DecoHome/Views/models/product.model.dart';
import 'package:DecoHome/Views/services/cart.service.dart';
import 'package:DecoHome/Views/services/product.service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class OrderService {
  final CollectionReference orderCollection =
      FirebaseFirestore.instance.collection('Order');
  final CollectionReference orderInfoCollection =
      FirebaseFirestore.instance.collection('OrderInfo');
  Future<List<OrderInfo>> listOrderInfoUser(String userId) async {
    //int deliveryState=1;
    List<Order> list = [];
    List<Order> list_temp = [];
    List<OrderInfo> listInfo = [];
    List<OrderInfo> listInfo_temp = [];
    await orderCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Order.fromSanpShot(element));
      });
    });
    for (int i = 0; i < list.length; i++) {
      if (list[i].userId == userId) {
        list_temp.add(list[i]);
      }
    }
    await orderInfoCollection.get().then((value) {
      value.docs.forEach((element) {
        listInfo.add(OrderInfo.fromSanpShot(element));
      });
    });
    for (int i = 0; i < listInfo.length; i++) {
      for (int j = 0; j < list_temp.length; j++) {
        if (list_temp[j].orderId == listInfo[i].orderId) {
          listInfo_temp.add(listInfo[i]);
        }
      }
    }
    //print(listInfo_temp);
    return listInfo_temp;
  }

  Future<List<OrderInfo>> listOrderInfo(String orderID) async {
    //print(orderID);
    List<OrderInfo> listInfo = [];
    List<OrderInfo> listInfo_temp = [];
    await orderInfoCollection.get().then((value) {
      value.docs.forEach((element) {
        listInfo.add(OrderInfo.fromSanpShot(element));
      });
    });
    for (int i = 0; i < listInfo.length; i++) {
      if (listInfo[i].orderId == orderID) {
        listInfo_temp.add(listInfo[i]);
      }
    }
    return listInfo_temp;
  }

  Future<List<OrderInfo>> listOrderSuccess(List<Order> orderList) async {
    //print(orderID);
    List<OrderInfo> listInfo = [];
    List<OrderInfo> listInfo_temp = [];
    await orderInfoCollection.get().then((value) {
      value.docs.forEach((element) {
        listInfo.add(OrderInfo.fromSanpShot(element));
      });
    });
    for (int i = 0; i < orderList.length; i++) {
      for (int j = 0; j < listInfo.length; j++) {
        if (orderList[i].orderId == listInfo[j].orderId) {
          listInfo_temp.add(listInfo[j]);
        }
      }
    }
    return listInfo_temp;
  }

  int countQuantity(List<OrderInfo> list) {
    int count = 0;
    for (int i = 0; i < list.length; i++) {
      count = count + list[i].quantityPro;
    }
    return count;
  }

  int countRevenue(List<OrderInfo> listOrderInfo, List<Product> listProduct) {
    int revenue = 0;
    for (int i = 0; i < listOrderInfo.length; i++) {
      Product product =
          ProductService().getPro(listProduct, listOrderInfo[i].proId);
      revenue = revenue + listOrderInfo[i].quantityPro * product.price;
    }
    return revenue;
  }

  int countTypeProduct(
      List<OrderInfo> listOrderInfo, List<Product> listProduct, String type) {
    int count = 0;
    for (int i = 0; i < listOrderInfo.length; i++) {
      Product product =
          ProductService().getPro(listProduct, listOrderInfo[i].proId);
      if (product.prodCategory == type) {
        count = count + listOrderInfo[i].quantityPro;
      }
    }
    return count;
  }

  Future<List<Order>> listOrderUser(String userId) async {
    List<Order> list = [];
    List<Order> list_temp = [];
    await orderCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Order.fromSanpShot(element));
      });
    });
    for (int i = 0; i < list.length; i++) {
      if (list[i].userId == userId) {
        list_temp.add(list[i]);
      }
    }
    return list_temp;
  }

  Future<List<Order>> listOrder(int dashboard) async {
    List<Order> list = [];
    List<Order> list_temp = [];
    //List<Order> list_temp=[];
    await orderCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Order.fromSanpShot(element));
      });
    });
    if (dashboard != 4) {
      for (int i = 0; i < list.length; i++) {
        if (list[i].deliveryState == dashboard) {
          list_temp.add(list[i]);
        }
      }
      return list_temp;
    } else {
      return list;
    }
  }

  Future<double> getQuantityOrder(int dashboard) async {
    List<Order> list = [];
    List<Order> list_temp = [];
    //List<Order> list_temp=[];
    await orderCollection.get().then((value) {
      value.docs.forEach((element) {
        list.add(Order.fromSanpShot(element));
      });
    });
    if (dashboard == 2) {
      for (int i = 0; i < list.length; i++) {
        if (list[i].deliveryState == 2) {
          list_temp.add(list[i]);
        }
      }
    } else if (dashboard == 0) {
      for (int i = 0; i < list.length; i++) {
        if (list[i].deliveryState == 0) {
          list_temp.add(list[i]);
        }
      }
    } else if (dashboard == 1) {
      for (int i = 0; i < list.length; i++) {
        if (list[i].deliveryState == 1) {
          list_temp.add(list[i]);
        }
      }
    } else {
      for (int i = 0; i < list.length; i++) {
        if (list[i].deliveryState == 3) {
          list_temp.add(list[i]);
        }
      }
    }
    return double.parse(list_temp.length.toString());
  }

  Order getOrder(List<Order> listOrder, String orderId) {
    for (int i = 0; i < listOrder.length; i++) {
      if (listOrder[i].orderId == orderId) {
        return listOrder[i];
      }
    }
  }

  Future<void> createOrder(String userId, String userName, String userPhone,
      String userAddress, int deliveryType, List<Cart> listCart) {
    //print(listCart);
    DateTime now = new DateTime.now();
    String id = orderCollection.doc().id;
    orderCollection.doc(id).set({
      'orderId': id,
      'userId': userId,
      'deliveryType': deliveryType,
      'deliveryState': 0,
      'countPro': listCart.length,
      'timeOrder':
          "${now.year}-${now.month}-${now.day} ${now.hour}:${now.minute}",
      'userName': userName,
      'userPhone': userPhone,
      'userAddress': userAddress,
    });
    for (int i = 0; i < listCart.length; i++) {
      String idInfo = orderInfoCollection.doc().id;
      orderInfoCollection.doc(idInfo).set({
        'orderInfoId': idInfo,
        'orderId': id,
        'proId': listCart[i].proId,
        'quantityPro': listCart[i].quantityPro,
        'status': "",
      });
      CartService().deleteCartItem(userId, listCart[i].proId);
    }
  }

  Future<void> deleteOrder(Order order, OrderInfo orderInfo) async {
    print("here");
    await orderInfoCollection.doc(orderInfo.orderInfoId).delete();
    if (order.countPro == 1) {
      await orderCollection.doc(order.orderId).delete();
    } else {
      await orderCollection
          .doc(order.orderId)
          .update({'countPro': order.countPro - 1});
    }
  }

  Future updateStatusOrder(String idOrder, int status) async {
    if (status != 3) {
      await orderCollection.doc(idOrder).update({'deliveryState': status + 1});
    } else {
      await orderCollection.doc(idOrder).update({'deliveryState': 1});
    }
  }

  Future cancelOrder(String idOrder) async {
    print(idOrder);
    await orderCollection.doc(idOrder).update({'deliveryState': 3});
  }

  Future<void> deleteProd(String idProd) async {
    List<Order> listOrder = [];
    List<OrderInfo> listOrderInfo = [];
    await orderCollection.get().then((value) {
      value.docs.forEach((element) {
        listOrder.add(Order.fromSanpShot(element));
      });
    });
    await orderInfoCollection.get().then((value) {
      value.docs.forEach((element) {
        listOrderInfo.add(OrderInfo.fromSanpShot(element));
      });
    });
    for (int i = 0; i < listOrder.length; i++) {
      for (int j = 0; j < listOrderInfo.length; j++) {
        if ((listOrderInfo[j].orderId==listOrder[i].orderId)&&(listOrderInfo[j].proId==idProd)) {
          print("Đây nè"+listOrderInfo[j].orderId);
           await orderInfoCollection.doc(listOrderInfo[j].orderInfoId).delete();
           if (listOrder[i].countPro == 1) {
             await orderCollection.doc(listOrder[i].orderId).delete();
           } else {
             await orderCollection
                 .doc(listOrder[i].orderId)
                 .update({'countPro': listOrder[i].countPro - 1});
           }
        }
      }
    }
  }
}
