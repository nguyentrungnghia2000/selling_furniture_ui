import 'package:cloud_firestore/cloud_firestore.dart';

class Cart{
  String cartId;
  String proId;
  String userId;
  int quantityPro;
  int proPrice;
  
  Cart.fromSanpShot(DocumentSnapshot snapshot){
    Map data=snapshot.data();
    this.cartId=data["cartId"];
    this.userId=data["userId"];
    this.proId=data["proId"];
    this.quantityPro=data["quantityPro"];
    this.proPrice=data["proPrice"];
  }
}