import 'package:cloud_firestore/cloud_firestore.dart';

class Favorite{
  String favoId;
  String proId;
  String userId;

  Favorite.fromSanpShot(DocumentSnapshot snapshot){
    Map data=snapshot.data();
    this.favoId=data["favoId"];
    this.userId=data["userId"];
    this.proId=data["proId"];
  }
}