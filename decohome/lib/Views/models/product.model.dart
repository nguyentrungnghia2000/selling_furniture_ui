import 'package:cloud_firestore/cloud_firestore.dart';

class Product {
  String prodid;
  String name;
  //String manufacture;
  String prodInfo;
  num amount;
  num price;
  //num rate;
  String pic;
  String color;
  String material;
  String prodCategory;

  Product.formSnapShot(DocumentSnapshot snapshot) {
    Map data = snapshot.data();
    this.prodid = data["prodid"] ?? '';
    this.name = data["name"] ?? '';
    //this.manufacture = data["manufacture"] ?? '';
    this.prodInfo = data["prodInfo"] ?? '';
    this.amount = data["amount"] ?? 0;
    this.price = data["price"] ?? 0;
    //this.rate = data["rate"] ?? 0;
    this.pic = data["pic"] ?? '';
    this.color = data["color"] ?? '';
    this.material = data["material"] ?? '';
    this.prodCategory = data["typeid"] ?? '';
  }
}
