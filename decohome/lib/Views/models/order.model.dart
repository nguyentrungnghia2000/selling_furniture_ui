import 'package:cloud_firestore/cloud_firestore.dart';

class Order {
  String orderId;
  String userId;
  int deliveryState;
  int deliveryType;
  int countPro;
  String userName;
  String userPhone;
  String timeOrder;
  String userAddress;

  Order.fromSanpShot(DocumentSnapshot snapshot) {
    Map data = snapshot.data();
    this.orderId = data["orderId"];
    this.userId = data["userId"];
    this.deliveryState = data["deliveryState"];
    this.deliveryType = data["deliveryType"];
    this.timeOrder=data["timeOrder"];
    this.countPro=data["countPro"];
    this.userName=data["userName"];
    this.userPhone=data["userPhone"];
    this.userAddress=data["userAddress"];
  }
}

class OrderInfo {
  String orderInfoId;
  String orderId;
  String proId;
  int quantityPro;
  String timeOrder;
  String status;
  OrderInfo.fromSanpShot(DocumentSnapshot snapshot) {
    Map data = snapshot.data();
    this.orderInfoId = data["orderInfoId"];
    this.orderId = data["orderId"];
    this.proId = data["proId"];
    this.quantityPro=data["quantityPro"];
    this.status=data["status"];
  }
}
