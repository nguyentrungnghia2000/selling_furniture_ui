import 'package:cloud_firestore/cloud_firestore.dart';

class Paper{
  String id;
  String Header;
  String Pic;
  String Title;
  String Link;
  
  Paper.fromSanpShot(DocumentSnapshot snapshot){
    Map data=snapshot.data();
    this.id=data["id"];
    this.Header=data["header"];
    this.Pic=data["pic"];
    this.Title=data["title"];
    this.Link=data["link"];
  }
}